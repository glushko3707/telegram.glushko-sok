<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\Telegram\InviteLinkController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/register/telegram', [\App\Http\Controllers\Auth\RegisteredUserController::class, 'telegram',])->name('RegisterTelegram');


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/projects', [ProjectController::class, 'projectIndex',])->name('project.index');
    Route::post('/projects/{id}', [ProjectController::class, 'projectUpdate'])->name('project.update');

    Route::post('/api/project/create/store', [ProjectController::class, 'createProjectStore',])->name('createProject.store');
    Route::delete('/project/destroy/store/{id}', [ProjectController::class, 'destroyProjectStore',])->name('project.destroy');

    Route::get('/linkInvite', [InviteLinkController::class, 'linkInviteIndex',])->name('linkInvite.index');
    Route::get('/project/permission/get', [ProjectController::class, 'getPermissionProject',])->name('getPermissionProject.index');
});

require __DIR__.'/auth.php';

// Тильда входящие лиды
Route::post('/api/tilda/new_leads/{id}', [\App\Http\Controllers\Tilda\TildaController::class, 'newLead']);
Route::post('/api/facebook/lead/store/{id}', [\App\Http\Controllers\Facebook\ApiConvertionWebhook::class, 'getWebhookTilda',])->name('getWebhookTilda.store');
