<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Smartsender\PicturController;
use App\Http\Controllers\Telegram\InviteLinkController;
use App\Http\Controllers\Telegram\MessageController;
use App\Http\Controllers\Telegram\Pipeline\PipelineController;
use App\Http\Controllers\Telegram\TelegramController;
use App\Http\Controllers\Telegram\TelegramSettingController;
use App\Http\Controllers\Telegram\UserBotController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/telegram/setting/{id}', [TelegramSettingController::class,'telegramSettingIndex',])->name('telegramSetting.index');
Route::post('/telegram/setting/store/{id}', [TelegramSettingController::class,'telegramSettingStore',])->name('telegramSetting.store');

Route::post('/api/service/setting/platform_name/{id}', [\App\Http\Controllers\Smartsender\SettingController::class,'setServiceBot',])->name('platform_name.store');

Route::match(['get', 'post'],'/telegram/post/{id}', [\App\Http\Controllers\PossevService\PossevPostController::class,'possevPostIndex',])->name('possevPost.index');
Route::match(['get', 'post'],'/telegram/post/delete/{post_id}', [\App\Http\Controllers\PossevService\PossevPostController::class,'deletePost',])->name('possevPost.delete');
Route::match(['get', 'post'],'/telegram/post/send/{post_id}/{project_id}', [\App\Http\Controllers\PossevService\PossevServiceController::class,'sendNewPossevLink',])->name('sendNewPossevLink.store');

Route::match(['get'],'/telegram/possev/stat/{id}', [\App\Http\Controllers\PossevService\PossevServiceController::class,'possevStatIndex',])->middleware('auth')->name('possevStat.index');



Route::get('/telegram/invite/link/{id}', [InviteLinkController::class,'telegramLinkIndex',])->name('telegramLink.index');
Route::get('/telegram/invite/link/create/store/{id}', [InviteLinkController::class,'createLinktelegram',])->name('createLinktelegram.store');

// Воронка телеграм
Route::get('telegram/pipeline/{id}', [PipelineController::class,'viewPipline',])->name('telegramPipeline.index');;
Route::get('api/telegram/pipeline/updatePost/{step_id}', [PipelineController::class,'viewPiplineUpdate',])->name('telegramPipeline.update');

Route::get('telegram/createPipeline/{id}', [PipelineController::class,'createPipeline',])->name('createPipeline.index');;
Route::post('api/telegram/createPipeline/store/{id}', [PipelineController::class,'createPipelineStore',])->name('createPipeline.store');;
Route::post('telegram/updatePipelineStep/store/{step_id}', [PipelineController::class,'updatePipelineStep',])->name('updatePipelineStep.store');;
Route::post('/api/telegram/deletePipelineStep/store/{id}', [PipelineController::class,'deletePipelineStep',])->name('deletePipelineStep.store');;

Route::get('telegram/createMessage/{id}', [MessageController::class,'createMessage',])->name('createMessage.index');;
Route::post('api/telegram/createMessage/store/{id}', [MessageController::class,'createMessageStore',])->name('createMessage.store');;

Route::middleware('auth')->group(function () {
    Route::get('telegram/user/{id}', [UserBotController::class, 'UserBotIndex',])->name('telegramBotUser.index');;
});


Route::match(['get', 'post'],'/api/getTelegramHook/{id}', [TelegramController::class,'getWebhook',])->name('getWebhook.store');

Route::post('/api/salebot/left_chat_member/{id}', [\App\Http\Controllers\SaleBot\TelegramMember::class,'leftMember',])->name('leftMember.store');
Route::match(['get', 'post'],'/api/salebot/join_chat_member/{id}', [\App\Http\Controllers\SaleBot\TelegramMember::class,'joinMember',])->name('joinMember.store');

Route::get('telegram/setWebhook', [\App\Http\Controllers\Telegram\TextHtmlBotController::class,'setWebhook',]);;
Route::post('/api/telegram/TextHtml/getTelegramHook', [\App\Http\Controllers\Telegram\TextHtmlBotController::class,'getTelegramHook',])->name('getTelegramHook');
Route::post('/api/telegram/TextHtml/getTelegramChatIdWebhook', [TelegramController::class,'getTelegramChatIdWebhook',])->name('getTelegramChatIdWebhook');

Route::middleware('auth')->group(function () {
    Route::get('api/post/picture/{id}', [PicturController::class, 'viewPicture',])->name('SmartsenderViewPicture.index');;
    Route::get('api/post/picture/create/{id}', [PicturController::class, 'createPicture',])->name('smartsenderAddPicture.index');;
});

Route::get('telegram/getUserChanal/{id}', [\App\Http\Controllers\SaleBot\TelegramMember::class, 'getUserChanal',]);
