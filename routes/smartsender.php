<?php

use App\Http\Controllers\Smartsender\ApiRequestController;
use App\Http\Controllers\Smartsender\PhoneController;
use App\Http\Controllers\Smartsender\PicturController;
use App\Http\Controllers\Telegram\TelegramController;
use App\Http\Controllers\Telegram\UserBotController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// настройки
Route::get('api/smartsender/setting/{id}', [\App\Http\Controllers\Smartsender\SettingController::class,'SettingIndex'])->name('Setting.index');
Route::post('api/smartsender/setting/store/{id}', [\App\Http\Controllers\Smartsender\SettingController::class,'SettingStore'])->name('Setting.store');


//phoneUpdate
Route::post('api/smartsender/phoneUser/get', [PhoneController::class,'phoneUser']);


Route::post('api/smartsender/sendPicture/{id}/{banner_name}', [PicturController::class,'sendAvatarPicturSmartsender',])->name('sendAvatarPicturSmartsender.store');;
Route::match(['get', 'post'],'api/smartsender/new/sendPicture/{id}', [PicturController::class,'sendAvatarPicturSmartsenderNew',])->name('sendAvatarPicturSmartsenderNew.store');



Route::post('api/post/picture/create/store/{id}', [PicturController::class,'createPictureStore',])->name('smartsenderAddPicture.store');;

Route::get('api/post/picture/update/{id}/{picture_name}', [PicturController::class,'updatePicture',])->name('smartsenderUpdatePicture.index');;
Route::delete('api/post/picture/delete/store/{id}', [PicturController::class,'deletePictureStore',])->name('smartsenderPicture.delete');;
Route::post('api/post/picture/get/store/{picture_id}', [PicturController::class,'getPictureInfo',])->name('getPictureInfo.store');;


Route::post('/api/smartsender/fire/{telegram_project_id}', [ApiRequestController::class,'smartsenderFireRequest',]);
Route::post('/api/smartsender/setVaruable/{telegram_project_id}', [ApiRequestController::class,'smartsenderSetVaruable',]);



Route::match(['get', 'post'],'api/salebot/sendPicture/{id}', [PicturController::class,'sendAvatarPictureSaleBot',])->name('sendAvatarPictureSaleBot.store');;



