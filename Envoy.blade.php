@servers(['glushko' => ['glushko_sok@194.67.112.130']])
{{--@servers(['glushko' => 'Nateve3707! -p ssh glushko_sok@194.67.112.130'])--}}

@task('deploy', ['on' => 'glushko'])

cd /var/www/glushko_sok/data/www/telegram.glushko-sok.ru
set -e
git pull
/opt/php82/bin/php artisan migrate
/opt/php82/bin/php artisan config:cache
/opt/php82/bin/php artisan event:cache
/opt/php82/bin/php artisan route:cache
/opt/php82/bin/php artisan view:cache
/opt/php82/bin/php artisan queue:restart
@endtask

@task('deployAll', ['on' => 'glushko'])

cd /var/www/glushko_sok/data/www/telegram.glushko-sok.ru
set -e
git pull
/opt/php82/bin/php artisan down
/opt/php82/bin/php /usr/local/bin/composer install
/usr/bin/npm run build
/opt/php82/bin/php artisan migrate
/opt/php82/bin/php artisan config:cache
/opt/php82/bin/php artisan event:cache
/opt/php82/bin/php artisan route:cache
/opt/php82/bin/php artisan view:cache
/opt/php82/bin/php artisan queue:restart
/opt/php82/bin/php artisan up

@endtask