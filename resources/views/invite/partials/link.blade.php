<section>
    <div class="d-flex">
        <div class="max-w-xl w-100">
        <header>
                {{ __($project->name) }}
        </header>



        <form method="post" action="{{ route('project.update',$project->id) }}" class="mt-6 space-y-6">
            @csrf

    {{--        <div>--}}
    {{--            <x-input-label for="name" :value="__('Твоя ссылка на приглашение в канал')" />--}}
    {{--            <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $project->link ?? '')" />--}}
    {{--        </div>--}}

            <div class="row mt-5 mb-3">
                <label for="inviteLink" class=""> Твоя личная ссылка на приглашения </label>
                <div class="mt-1 w-full">
                    <div class="input-group">
                        <p id="inviteLink" class="form-control" style="font-size: 14px">
                            {{$user->link}} <p>
                                        <span>
                                          <button type="button" id="copyButton" style="background-color: #0d6efd; color: #ffffff" class="btn btn-outline-primary">Копировать</button>
                                        </span>
                    </div>
                </div>
            </div>

        </form>
        </div>
        <div class="w-100">
            <div class="d-flex justify-content-between">
                <div>
                </div>
                @if(auth()->user()->telegram_id == null)
                <div class="mr-3">
                    <a href="https://t.me/morskoy_invite_bot?start={{$user->code ?? ''}}" target="_blank">Подключить телеграм </a>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>

<script>
    var copyButton = document.getElementById('copyButton');
    var paragraph = document.getElementById('inviteLink');


    copyButton.addEventListener('click', function() {

        // Используем библиотеку clipboard.js для копирования текста в буфер обмена
        var temp = document.createElement("textarea");
        document.body.appendChild(temp);
        temp.value = paragraph.innerText;
        temp.select();
        document.execCommand("copy");
        document.body.removeChild(temp);


        // Визуальное подтверждение копирования
        copyButton.innerText = 'Скопировано!';
        setTimeout(function(){
            copyButton.innerText = 'Скопировать';
        }, 2000);
    });
</script>
