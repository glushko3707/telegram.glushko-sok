<section>
    <div class="d-flex">
        <div class="w-100">
            <div class="d-flex mb-3 justify-content-between">
                <div class="mr-3">
                    <h2 style="font-size: 18px;font-weight: 500">
                        Кого пригласил я
                    </h2>
                </div>
                <div>
                </div>
            </div>
            <div class="mb-3">
                Я пригласил {{count($invites['my'] ?? '0')}} пользователя <br>
                и заработал {{(count($invites['my'])  * 100) ?? '0' }} рублей

            </div>
            @if( isset($invites['my']) )

            <div>
                Пользователи:
               @foreach($invites['my'] as $invit)
                 id - {{$invit['telegram_id']}},
               @endforeach
            </div>
            @endif
        </div>
    </div>
</section>

<script>
    var copyButton = document.getElementById('copyButton');
    var paragraph = document.getElementById('inviteLink');


    copyButton.addEventListener('click', function() {

        // Используем библиотеку clipboard.js для копирования текста в буфер обмена
        var temp = document.createElement("textarea");
        document.body.appendChild(temp);
        temp.value = paragraph.innerText;
        temp.select();
        document.execCommand("copy");
        document.body.removeChild(temp);


        // Визуальное подтверждение копирования
        copyButton.innerText = 'Скопировано!';
        setTimeout(function(){
            copyButton.innerText = 'Скопировать';
        }, 2000);
    });
</script>
