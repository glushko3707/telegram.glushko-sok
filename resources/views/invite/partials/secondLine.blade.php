<section>
    <div class="d-flex">
        <div class="w-100">
            <div class="d-flex mb-3 justify-content-between">
                <div class="mr-3">
                    <h2 style="font-size: 18px;font-weight: 500">
                        Моя банда
                    </h2>
                </div>
                <div>
                </div>
            </div>
            @if( isset($invites['first']) )

            <div class="mb-3">
               Мои друзья пригласили {{count($invites['first'] ?? '0')}}
                <br> и я заработал {{(count($invites['first']) * 40) ?? '0'}} рублей
            </div>

            <div>
               @foreach($invites['first'] as $key => $invit)
                 {{$key}} пригласил - {{$invit['count']}} чел. <br>
               @endforeach
            </div>
            @else
                <div class="mb-3">
                    Мои друзья пригласили 0
                    <br> и я заработал 0 рублей
                </div>
            @endif
        </div>
    </div>
</section>

<script>
    var copyButton = document.getElementById('copyButton');
    var paragraph = document.getElementById('inviteLink');


    copyButton.addEventListener('click', function() {

        // Используем библиотеку clipboard.js для копирования текста в буфер обмена
        var temp = document.createElement("textarea");
        document.body.appendChild(temp);
        temp.value = paragraph.innerText;
        temp.select();
        document.execCommand("copy");
        document.body.removeChild(temp);


        // Визуальное подтверждение копирования
        copyButton.innerText = 'Скопировано!';
        setTimeout(function(){
            copyButton.innerText = 'Скопировать';
        }, 2000);
    });
</script>
