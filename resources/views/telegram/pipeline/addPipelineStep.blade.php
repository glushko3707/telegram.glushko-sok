@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <main class="flex flex-grow-1 py-3">
            <div class="container" >

                <div class="card" >
                    <div class="card-header"> Добавить событие воронки телеграм </div>


                    <div class="card-body">
                        <form method="POST" id="pipeline_step" enctype="multipart/form-data" action="{{ route('createPipeline.store',$id) }}">
                            @csrf


                            <div class="row mb-3">
                                <label for="pipelineId" class="col-md-4 col-form-label text-md-end">ID Воронки</label>
                                <div class="col-md-3 col-form-label text-md-end" style="display: flex;">
                                    <input id="pipeline_id" type="text" class="form-control" name="pipeline_id">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="content_type" class="col-md-4 col-form-label text-md-end">Тип события</label>
                                <div class="col-md-3">
                                    <select class="form-select-inline" onchange="enterType(this.value)" id="content_type" name="content_type" style="width:100%;margin-left: 0">
                                        <option value="post">Пост</option>
                                        <option value="condition">Условие</option>
                                        <option value="action">Действие</option>
                                        <option value="delay">Задержка</option>
                                    </select>
                                </div>
                            </div>

                            <div id="time" class="delay enterType" style="display: none">
                                <div class="row mb-3">
                                    <label for="delay_type" class="col-md-4 col-form-label text-md-end">Тип задержки</label>
                                    <div class="col-md-3">
                                        <select class="form-select-inline" onchange="delayType(this.value)" id="content_type" name="delay_type" style="width:100%;margin-left: 0">
                                            <option value="minute">В минутах</option>
                                            <option value="time">Время</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mb-3 minute">
                                    <label for="delay" class="col-md-4 col-form-label text-md-end">Задержка</label>
                                    <div class="col-md-3">
                                        <input id="delay" type="number" class="form-control" name="delay">
                                    </div>
                                </div>

                                <div class="row mb-6 mb-3 time" style="display: none">
                                    <label for="timeSend" class="col-md-4 col-form-label text-md-end">Указать время</label>
                                    <div class="col-md-7" style="display: flex;">

                                        <div class="col-md-3">
                                            <input id="timeSend" type="time" class="form-control" name="timeSend">
                                        </div>

                                        <label for="onlyToday" class="col-md-3 col-form-label text-md-end">Только сегодня</label>
                                        <div class="col-md-1">
                                            <input id="onlyToday" type="checkbox" class="form-check-input" name="onlyToday" style="margin-left: 0;padding: 11px">
                                        </div>

                                        <label for="onlyTomorrow" class="col-md-3 col-form-label text-md-end">Завтра</label>
                                        <div class="col-md-1">
                                            <input id="onlyTomorrow" type="checkbox" class="form-check-input" name="onlyTomorrow" style="margin-left: 0;padding: 11px">
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="post enterType">
                                <div class="row mb-3">
                                    <label for="content_type" class="col-md-4 col-form-label text-md-end">Тип медиа</label>
                                    <div class="col-md-3">
                                        <select class="form-select-inline" onchange="enterMedia(this.value)" id="media_type" name="media_type" style="width:100%;margin-left: 0">
                                            <option value="noMedia">Без вложений</option>
                                            <option value="picture">Картинка</option>
                                            <option value="video">Видео</option>
                                            <option value="videoNote">Видео заметка</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3 picture" style="display: none">
                                    <label for="photoPost" class="col-md-4 col-form-label text-md-end">Картинка</label>
                                    <div class="col-md-3">
                                        <input id="photoPost" type="file" class="form-control" name="photoPost" accept="image/*">
                                    </div>
                                </div>
                                <div style="display: none" class="row mb-3 video">
                                    <label for="video" class="col-md-4 col-form-label text-md-end">Id видео</label>
                                    <div class="col-md-7" style="display: flex;">
                                        <input id="video" type="text" class="form-control" name="video">
                                    </div>
                                </div>
                                <div style="display: none" class="row mb-3 videoNote">
                                    <label for="videoNote" class="col-md-4 col-form-label text-md-end">Id видео заметки</label>
                                    <div class="col-md-7" style="display: flex;">
                                        <input id="videoNote" type="text" class="form-control" name="videoNote">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="textarea-element" class="col-md-4 col-form-label text-md-end">Текст</label>
                                    <div class="col-md-3">
                                        <textarea class="textarea-element" style="border-color: #9b9b9b" rows="10" name="text_post"></textarea>
                                    </div>
                                </div>



                                <div class="row mb-6 mb-3">
                                    <label for="button_type" class="col-md-4 col-form-label text-md-end">Настройки кнопки</label>
                                    <div class="col-md-7 d-flex">
                                        <select class="form-select-inline" onchange="selectButton(this)" id="button_type" name="button_type" style="width:50%;margin-left: 0">
                                            <option value="noButton">Без кнопки</option>
                                            <option value="inline_keyboard">inline_keyboard</option>
                                            <option value="KeyboardButton">KeyboardButton</option>
                                            <option value="remove_keyboard">remove_keyboard</option>
                                        </select>


                                        <label style="display: none" for="OneTimeKeyboard" class="col-md-4 buttonSetting col-form-label text-md-end buttonRemove OneTimeKeyboard">One time keyboard</label>
                                        <div style="display: none" class="col buttonSetting OneTimeKeyboard">
                                            <input id="OneTimeKeyboard" type="checkbox" class="form-check-input buttonRemove" name="OneTimeKeyboard" style="margin-left: 0;padding: 11px">
                                        </div>



                                        {{--                                            <label style="display: none"  for="contact" class="col-md-4 buttonSetting col-form-label text-md-end buttonRemove">Собирать контакт</label>--}}
                                        {{--                                            <div style="display: none"  class="col buttonSetting">--}}
                                        {{--                                                <input id="contact" type="checkbox" class="form-check-input buttonRemove" name="contact" style="margin-left: 0;padding: 11px">--}}
                                        {{--                                            </div>--}}
                                    </div>

                                    <div id="button_place" class="row mt-3 d-flex justify-content-center" >

                                        <div class="row mb-3 buttonRemove buttonSetting" style="display:none;">
                                            <label for="button_post_0" class="col-md-4 col-form-label text-md-end buttonRemove">Текст кнопки</label>
                                            <div class="col-md-8 d-flex justify-content" style="display: flex;">
                                                <div>
                                                    <input id="button_post_0" type="text" class="form-control buttonRemove" name="button_post" style="width: 250px; margin-right: 15px">
                                                </div>
                                                <div class="d-flex justify-content-start">
                                                    <select class="form-select-inline" onchange="button_type_inline(this)" id="button_type_inline_0" name="button_type_inline_0" style="margin-left: 0">
                                                        <option value="Action">Action</option>
                                                        <option value="URL">Url</option>
                                                    </select>
                                                    <input id="callback_action_0" type="text" class="form-control ml-3" name="callback_action">
                                                    <input id="url_0" type="text" class="form-control  ml-3" style="display: none" name="url">
                                                </div>
                                            </div>
                                        </div>



                                    </div>


                                </div>
                                <div class="col-md-4 col-form-label text-md-end buttonSetting buttonRemove" style="display: none">
                                    <a class="btn btn-primary" onclick="addButton()">
                                        Добавить кнопку
                                    </a>
                                </div>

                            </div>


                            <div class="row mb-0">
                                <div class="col-md-3 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Отправить запрос
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Фото</th>
                                <th scope="col">Текст </th>
                                <th scope="col">Кнопки </th>
                                <th scope="col">Callback</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($pipeline as $key => $step)
                                <tr class="selectsQuery" id="selectsQuery_{{$step->callback_data}}" data-id="{{$step->callback_data}}" data-bs-dismiss="modal">
                                    <td>
                                        {{$step->id}}
                                    </td>
                                    <td>
                                        @if($step['mediaUrl'] != null)
                                            <div id="emptyPicture">
                                                <img  style="max-width: 100px; max-height: 100px" id="photoImg" src="{{$step['mediaUrl']}}" alt="My Image">
                                                <input id="photoPost" type="file" class="form-control" name="photoPost" accept="image/*" style="display: none;">
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        <div style="background-color: #f1f0f0; max-height: 100px; max-width: 100%">
                                            <textarea class="textarea-element p-3" style="width: 100%;">
                                                {{$step['text']}}
                                            </textarea>
                                        </div>
                                    </td>
                                    <td>
{{--                                        @if($step['buttons'] != null)--}}
{{--                                            @foreach($step['buttons'] as $key => $button)--}}
{{--                                                @if($key == 'inline_keyboard')--}}
{{--                                                    <div class="d-flex justify-content-start" style="width: 80%">--}}
{{--                                                        <div type="button" id="addButton" class="telegramInlineButton">--}}
{{--                                                            {{$button['button_text']}}--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                @endif--}}

{{--                                                @if($key == 'KeyboardButton')--}}
{{--                                                    <div class="d-flex justify-content-start " style="width: 100%">--}}
{{--                                                        <div type="button" id="addButton" class="telegramKeyboardButton">--}}
{{--                                                            {{$button['button_text']}}--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}
                                    </td>
                                    <td>
                                        {{$step->callback_data}}
                                    </td>
                                </tr>
                            @endforeach
                            @foreach($mess as $key => $step)
                                <tr class="selectsQuery" id="selectsQuery_{{$step['callback_data']}}" data-id="{{$step['callback_data']}}" data-bs-dismiss="modal">
                                    <td>
                                        {{$step['id']}}
                                    </td>
                                    <td>
                                        @if($step['mediaUrl'] != null)
                                            <div id="emptyPicture">
                                                <img  style="max-width: 100px; max-height: 100px" id="photoImg" src="{{$step['mediaUrl']}}" alt="My Image">
                                                <input id="photoPost" type="file" class="form-control" name="photoPost" accept="image/*" style="display: none;">
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        <div style="background-color: #f1f0f0; max-height: 100px; max-width: 100%">
                                            <textarea class="textarea-element p-3" style="width: 100%;">
                                                {{$step['text']}}
                                            </textarea>
                                        </div>
                                    </td>
                                    <td>
                                        {{--                                        @if($step['buttons'] != null)--}}
                                        {{--                                            @foreach($step['buttons'] as $key => $button)--}}
                                        {{--                                                @if($key == 'inline_keyboard')--}}
                                        {{--                                                    <div class="d-flex justify-content-start" style="width: 80%">--}}
                                        {{--                                                        <div type="button" id="addButton" class="telegramInlineButton">--}}
                                        {{--                                                            {{$button['button_text']}}--}}
                                        {{--                                                        </div>--}}
                                        {{--                                                    </div>--}}
                                        {{--                                                @endif--}}

                                        {{--                                                @if($key == 'KeyboardButton')--}}
                                        {{--                                                    <div class="d-flex justify-content-start " style="width: 100%">--}}
                                        {{--                                                        <div type="button" id="addButton" class="telegramKeyboardButton">--}}
                                        {{--                                                            {{$button['button_text']}}--}}
                                        {{--                                                        </div>--}}
                                        {{--                                                    </div>--}}
                                        {{--                                                @endif--}}
                                        {{--                                            @endforeach--}}
                                        {{--                                        @endif--}}
                                    </td>
                                    <td>
                                        {{$step['callback_data']}}
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>

                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>



    <script>

        function selectButton(types){
            $("#button_place").empty();

            const buttonSetting = document.querySelectorAll('.buttonSetting');
            const buttonRemove = document.querySelectorAll('.buttonRemove');
            const OneTimeKeyboard = document.querySelectorAll('.OneTimeKeyboard');
            if (types.value === 'noButton'){
                buttonSetting.forEach((elem) => {
                    elem.style.display = "none";
                });
            }
            else if(types.value === 'remove_keyboard') {
                buttonRemove.forEach((elem) => {
                    elem.style.display = "none";
                })
            }
            else if(types.value === 'KeyboardButton') {
                buttonSetting.forEach((elem) => {
                    elem.style.display = "";
                });
                buttonRemove.forEach((elem) => {
                    elem.style.display = "";
                });
                OneTimeKeyboard.forEach((elem) => {
                    elem.style.display = "";
                })
            }
            else{
                buttonSetting.forEach((elem) => {
                    elem.style.display = "";
                });
                buttonRemove.forEach((elem) => {
                    elem.style.display = "";
                });
                OneTimeKeyboard.forEach((elem) => {
                    elem.style.display = "none";
                })
            }
        }
        function button_type_inline(thisis) {
            document.getElementById(thisis);
            let button_id = thisis.id.replace('button_type_inline_', '')

            // Создаем функцию для скрытия или показа элементов
            function toggleElements(value, id1, id2) {
                let element1 = document.getElementById(id1);
                let element2 = document.getElementById(id2);

                // Используем тернарный оператор для простого условия
                value === 'URL' ? (element1.style.display = "none", element2.style.display = "") : (element1.style.display = "", element2.style.display = "none");
            }

            // Вызываем функцию с нужными параметрами
            toggleElements(thisis.value, 'callback_action_' + button_id, 'url_' + button_id);
        }
        function enterType(type){
            document.querySelectorAll('.enterType').forEach((elem) => {
                elem.style.display = "none";
                var form = document.getElementById("pipeline_step"),
                    exclude = document.getElementById("pipeline_id"),
                    parentNode = exclude.parentNode;
                parentNode.removeChild(exclude);
                form.reset();
                var typez = document.getElementById("content_type");
                typez.value = type
                parentNode.appendChild(exclude);
            });

            document.querySelectorAll(("." + type)).forEach((elem) => {
                elem.style.display = "";
            });
        }
        function enterMedia(types) {
            document.querySelectorAll('.picture').forEach((picture) => {
                picture.style.display = "none";
            });
            document.querySelectorAll('.video').forEach((video) => {
                video.style.display = "none";
            });
            document.querySelectorAll('.videoNote').forEach((videoNote) => {
                videoNote.style.display = "none";
            });

            switch (types) {
                case 'picture':
                    document.querySelectorAll('.picture').forEach((picture) => {
                        picture.style.display = "";
                    });
                    break;
                case 'video':
                    document.querySelectorAll('.video').forEach((video) => {
                        video.style.display = "";
                    });
                    break;
                case 'videoNote':
                    document.querySelectorAll('.videoNote').forEach((videoNote) => {
                        videoNote.style.display = "";
                    });
                    break;
                default:
                    console.log('Invalid type');
            }
        }
        function delayType(typed){
            if (typed === 'time'){
                document.querySelectorAll('.minute').forEach((elem) => {
                    elem.style.display = "none";
                });
                document.querySelectorAll('.time').forEach((time) => {
                    time.style.display = "";
                });

            }
            else{
                document.querySelectorAll('.time').forEach((elem) => {
                    elem.style.display = "none";
                });
                document.querySelectorAll('.minute').forEach((time) => {
                    time.style.display = "";
                });
            }
        }


        var button_place =  document.getElementById('button_place');
        var buttons = 1
        var callback_action = 1
        var button_for_action = 0



        function addButton(qualifiedName, value) {

            let div = document.createElement("div")
            div.classList.add('row', 'mb-3');

            let label = document.createElement("label")
            label.for = 'button_post_' + buttons;
            label.classList.add('col-md-4','col-form-label','text-md-end')
            label.textContent = 'Текст кнопки'
            div.appendChild(label)

            let diva = document.createElement("div")
            diva.classList.add('col-md-8', 'd-flex', 'justify-content');
            diva.style.display = 'flex';

            let divb = document.createElement("div")

            let input = document.createElement("input")
            input.id = 'button_post_' + buttons;
            input.type = 'text'
            input.classList.add('form-control', 'buttonRemove');
            input.name = 'button_post_' + buttons;
            input.style.width = '250px';
            input.style.marginRight = '15px';

            let divc = document.createElement("div")
            divc.classList.add('d-flex', 'justify-content-start');

            let select = document.createElement("select")
            select.classList.add('form-select-inline');
            select.id = 'button_type_inline_' + buttons;
            select.name = 'button_type_inline_' + buttons;
            select.style.marginLeft = '0';

            let option = document.createElement("option")
            option.value = "Action";
            option.textContent = "Action";

            select.appendChild(option)

            if (document.getElementById("button_type").value === 'inline_keyboard'){
                let option1 = document.createElement("option")
                option1.value = "URL";
                option1.textContent = "URL";
                select.appendChild(option1)

            }

            divc.appendChild(select)
            divb.appendChild(input)

            let button = document.createElement("button");
            button.setAttribute("type", "button");
            button.setAttribute("class", "btn btn-primary ml-3");
            button.setAttribute("data-bs-toggle", "modal");
            button.setAttribute("data-bs-target", "#exampleModal");
            button.setAttribute("data-button-id", buttons);
            button.innerHTML = "Callback";

            divc.appendChild(button)


            let callback_action = document.createElement("input")
            callback_action.type = 'text';
            callback_action.name = 'callback_action_' + buttons;
            callback_action.id = 'callback_action_' + buttons;
            callback_action.classList.add('form-control','ml-3',);
            divc.appendChild(callback_action)

            if (document.getElementById("button_type").value === 'inline_keyboard'){
                let url = document.createElement("input")
                url.type = 'url';
                url.name = 'url_' + buttons;
                url.id = 'url_' + buttons;
                url.placeholder = 'https://example.com';
                url.pattern = "https://.*";
                url.size = 30;
                url.classList.add('form-control','ml-3',);
                url.style.display = 'none'
                divc.appendChild(url)
            }

            diva.appendChild(divb)
            diva.appendChild(divc)

            div.appendChild(diva)
            button_place.appendChild(div)

            select.addEventListener("change", function() {
                button_type_inline(select);
            })

            $("button[data-bs-toggle='modal']").click(function() {
                button_for_action = $(this).attr("data-button-id");
            });

            this.buttons = this.buttons + 1;
        }

        $(".selectsQuery").on("click", function() {
            console.log(this)
            addCallback(this)
        });

        function addCallback(thisiss){
            let idCallback = 'callback_action_' + this.button_for_action
            let element1 = document.getElementById(idCallback);
            element1.value = thisiss.getAttribute('data-id')
        }



    </script>


@endsection

