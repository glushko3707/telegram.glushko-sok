@extends('adminPanel.startAddProject')

@section('content')

    <div id="login" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button data-dismiss="modal" class="close">&times;</button>
                    <h4 class="mb-3" id="deal" style="text-align: center">Этап воронки</h4>
                    <div class="d-flex justify-content-center bg-gradient-white" style="height: 750px; border-radius: 15px">
                        <form action="{{route('updatePipelineStep.store',$id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="textareaModal mt-5"  style="background-color: #ffffff;border-color: rgba(224,224,224,30)">
                                <div id="emptyPicture" class="d-flex flex-column border-right border-left border-top" style="border-top-left-radius: 15px;border-top-right-radius: 15px;background-color: #ffffff;border-width: 1px;border-color: #0a0e14">
                                    <div class="d-flex justify-content-center">
                                        <img  style="align-content: center;border-top-left-radius: 15px;border-top-right-radius: 15px;background-color: #ffffff;max-width: 100%" id="photoImg" src="{{asset("dist/img/photo.png")}}" alt="My Image">
                                    </div>
                                    <div>
                                        <input id="photoPost" type="file" class="form-control" name="photoPost" accept="image/*" style="display: none;">
                                    </div>
                                </div>
                                <div style="background-color: #f1f0f0">
                                    <textarea id="textareaModal" class="textareaModal font-weight-normal p-3" style="background-color: #f1f0f0;border-color: rgba(224,224,224,0)" name="text" ></textarea>
                                </div>

                                <div id="addButtonses">

                                </div>
                                <div>
                                    <input id="pipeline_edit_id" type="hidden" class="form-control" name="pipeline_edit_id">

                                    <button class=" mt-3 btn btn-primary">
                                        Сохранить
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
  // Находим элементы по их id
  import TestComponent from "../../../js/TestComponent";
  var image = $("#photoImg");
  var input = $("#photoPost");

  // Добавляем обработчик события click к элементу <img>
  image.click(function() {
    // Показываем или скрываем элемент <input>
    input.toggle();
  });
    </script>

    <div class="content-wrapper">
        <main class="flex flex-grow-1 py-3">
                <div class="container" id="app">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Очередь</th>
                            <th scope="col">id</th>
                            <th scope="col">Пост </th>
                            <th scope="col">Воронка</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pipleline as $key => $step)
                        <tr>
                            <td style="width: 30px"><input id="priority" style="width: 50px" onblur="updatePriority(this,{{$step['id']}},{{$id}})"  type="number"  class="input-opacity" name="priority" value="{{$step['priority']}}"></td>

                            <td><p>{{$step['content_id']}}</p></td>

                            @if(isset($step['messege_id']))
                            <td>
{{--                                <textarea   class="textarea-element" id="post{{$step['messege_id']}}" name="text" >{{$step['content_type']}}</textarea>--}}

                                <div style="width: 250px; ">
                                    @if($step['mediaUrl'] != null)
                                    <div id="emptyPicture" class="border-right border-left border-top" style="border-top-left-radius: 15px;border-top-right-radius: 15px;background-color: #ffffff;border-width: 1px;border-color: #0a0e14">
                                        <img  style="border-top-left-radius: 15px;border-top-right-radius: 15px;background-color: #ffffff; max-width: 100%" id="photoImg" src="{{$step['media']}}" alt="My Image">
                                        <input id="photoPost" type="file" class="form-control" name="photoPost" accept="image/*" style="display: none;">
                                    </div>
                                    @endif

                                    <div style="background-color: #f1f0f0">
                                        <textarea id="post{{$step['messege_id']}}" class="textarea-element p-3" style="border-color: rgba(224,224,224,0)" name="text" >{{$step['content_type']}}</textarea>
                                    </div>
                                     @if(isset($step['button']) && $step['button'] != null)
                                         @foreach($step['button'] as $button)
                                                @if($button['type'] == 'inline_keyboard')
                                                <div class="d-flex justify-content-start" style="width: 80%">
                                                        <div type="button" id="addButton" class="telegramInlineButton">
                                                            {{$button['button_text']}}
                                                        </div>
                                                  </div>
                                                @endif

                                                @if($button['type'] == 'KeyboardButton')
                                                    <div class="d-flex justify-content-start " style="width: 100%">
                                                        <div type="button" id="addButton" class="telegramKeyboardButton">
                                                            {{$button['button_text']}}
                                                        </div>
                                                    </div>
                                                @endif
                                         @endforeach
                                    @endif
                            </div>


                                {{--                                       @if(isset($step['button']))--}}
{{--                                            @foreach($step['button'] as $button)--}}
{{--                                             <div style="justify-content: start">--}}
{{--                                                <div class="btn btn-outline-primary" style="width:250px ">--}}
{{--                                                    {{$button['button_text']}}--}}
{{--                                                </div>--}}
{{--                                             </div>--}}
{{--                                            @endforeach--}}
{{--                                       @endif--}}

                            </td>
                                @else
                                 <td>
                                    <div>
                                        {{$step['content_type']}}
                                    </div>
                                </td>
                                @endif
{{--                            <td><input type="text"  style=" font-size: 12px;white-space: pre-line;" size="50"     value="{{$step['content_type']}}" onblur="updateRow(this)"/></td>--}}
{{--                            <th scope="row" style="font-size: 12px;white-space: pre-line;">{{$step['content_type']}}</th>--}}



                            <td style="width: 30px"><input id="pipeline_id" style="width: 50px" onblur="updatePriority(this,{{$step['id']}},{{$id}})"  type="number"  class="input-opacity" name="pipeline_id" value="{{$step['pipeline_id']}}"></td>
                            <td>
                                <i onclick="deletes(this,{{$step['id']}},{{$id}})" class="fas fa-times" style="color: red"></i>
                            </td>
                            <td>
                                <a href="{{route('telegramPipeline.update',$step['id'])}}" class="fa fa-cog" style="color: green"></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center">
                        <div class="m-2">
                        <a href="{{route('createPipeline.index',$id)}}" class="btn btn-primary" style="align-items: center;margin: auto; width: 226px;">
                            Добавить новый этап!
                        </a>
                        </div>
                        <div class="m-2">
                        <a href="{{route('createMessage.index',$id)}}" class="btn btn-primary" style="align-items: center;margin: auto; width: 226px;">
                            Добавить сообщение
                        </a>
                        </div>
                    </div>


                </div>
        </main>
    </div>

    <script>

        function handleClick(id,pipleline,textarea) {

            if (pipleline[id].button.length > 0){
                if (pipleline[id].button[0].type == 'inline_keyboard'){

                    let addButton = document.getElementById('addButtonses');
                    $('#addButtonses').empty();
                    pipleline[id].button.forEach(el => {
                        let button = document.createElement("div")
                        let buttons = document.createElement("div")
                        button.classList.add('telegramInlineButton');
                        button.textContent = el.button_text
                        buttons.classList.add('d-flex', 'justify-content-start');
                        buttons.style.width = '80%';
                        buttons.appendChild(button)
                        addButton.appendChild(buttons)
                    })
                }

                else if (pipleline[id].button[0].type == 'KeyboardButton'){

                    let addButton = document.getElementById('addButtonses');
                    $('#addButtonses').empty();

                    pipleline[id].button.forEach(el => {
                        let button = document.createElement("div")
                        let buttons = document.createElement("div")
                        button.classList.add('telegramKeyboardButton');
                        button.textContent = el.button_text
                        buttons.classList.add('d-flex', 'justify-content-start');
                        buttons.style.width = '100%';
                        buttons.appendChild(button)
                        addButton.appendChild(buttons)

                        button.addEventListener("click", function() {
                            // Выводим сообщение в консоль при нажатии на новый элемент
                            console.log(el.callback_data + ' проект ' + id);
                        });

                    })
                }
                else {
                    $('#addButtonses').empty();
                }
            }

            if (pipleline[id].mediaUrl !== null){
                let photoImg = document.getElementById('photoImg');
                photoImg.src = pipleline[id].media; // установить новый путь к изображению
            } else{
                let photoImg = document.getElementById('photoImg');
                photoImg.src = "{{ asset("dist/img/photo.png") }}"; // установить новый путь к изображению
            }


            let elem = document.getElementById('deal');
            elem.textContent = 'Этап воронки ' + pipleline[id].id;
            document.getElementById('pipeline_edit_id').value = pipleline[id].id;
            document.getElementById('textareaModal').value = pipleline[id].content_type;
            document.getElementById('textareaModal').style.height =  document.getElementById('post'+ textarea).style.height;



        }




        function resizeTextarea(textarea) {
            // Устанавливаем высоту textarea равной его scrollHeight
            textarea.style.height = textarea.scrollHeight + 2 + "px";
        }


    </script>

    <script>
        // Получаем элемент textarea по id
        var textarea = document.getElementsByClassName("textarea-element");
        // Устанавливаем функцию, которая изменяет высоту textarea при вводе текста
        function autoResize() {
            // Сбрасываем высоту до минимальной
            document.querySelectorAll('.textarea-element').forEach(el => {
                if (el.value) {
                    el.style.height = (el.scrollHeight) + 3 + "px";
                }
            })

        }
        // Применяем функцию при загрузке страницы
        autoResize();
        // Применяем функцию при каждом изменении текста
    </script>
{{--    <script>--}}
{{--        --}}

{{--        function updateRow(row,messege_id,id){--}}

{{--            text = document.getElementById(row.id).value--}}
{{--            if (text.length > 1024){--}}
{{--                alert('Символов больше чем 1024')--}}
{{--                return 0;--}}
{{--            }--}}
{{--            $.ajax({--}}
{{--                url: "/api/telegram/pipeline/updatePost/" + id ,--}}
{{--                type: 'POST',--}}
{{--                data: {--}}
{{--                    'text': text,--}}
{{--                    'messege_id': messege_id,--}}
{{--                    'id': id,--}}
{{--                },--}}
{{--                success:function(response){--}}

{{--                }--}}
{{--            });--}}
{{--        }--}}
{{--    </script>--}}

    <script>
        // function updatePriority(row,messege_id,id){
        //
        //     let name = row.name
        //     let value = row.value
        //
        //     $.ajax({
        //         url: "/api/telegram/pipeline/updatePost/" + id ,
        //         type: 'POST',
        //         data: {
        //             'name': name,
        //             'value': value,
        //             'messege_id': messege_id,
        //             'id': id,
        //         },
        //         success:function(response){
        //
        //         }
        //     });
        //
        // }

        function deletes(row,pipeline_edit_id,id){
            confirm("Are you sure?");
            $.ajax({
                url: "/api/telegram/deletePipelineStep/store/" + id ,
                type: 'POST',
                data: {
                    'delete' : 'yes',
                    'pipeline_edit_id': pipeline_edit_id,
                    'id': id,
                },
                success:function(response){
                    window.location.reload()
                }
            });
        }

    </script>

@endsection
