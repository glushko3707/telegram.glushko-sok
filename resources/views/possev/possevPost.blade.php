@extends('adminPanel.startAddProject')
@section('title', 'Посты для посевов')

@section('content')

    <div class="content-wrapper">
        <main class="flex flex-grow-1 py-3">
                <div class="container">
                    <div class="row">
                        @foreach($PosevPost as $key => $step)
{{--                            <div>--}}
{{--                            </div>--}}
                        <div class="col-4 d-flex mt-3">
                            <div style="padding: 0 10px 0 0">
                                <div class="mb-1">
                                    {{ $step['name'] }}
                                </div>
                                <div>
{{--                                    <a href="{{route('newPossevLink.store',['post_id' => $step['id'], 'project_id' => $id])}}">--}}
                                        <button data-bs-toggle="modal" data-bs-target="#newPost" class="w-100 btn btn-sm btn-outline-primary">  Новая ссылка </button>
{{--                                    </a>--}}
                                </div>
                                <div class="mt-2">
                                    <button class="w-100 btn btn-sm btn-outline-primary"> Статистика </button>
                                </div>
                                <div class="mt-2">
                                    <a href="{{route('possevPost.delete',$step['id'])}}">
                                        <button class="w-100 btn btn-sm btn-outline-primary">  Удалить </button>
                                    </a>
                                </div>

                            </div>
                            <div style="width: 250px; height: 570px; overflow: auto; -ms-overflow-style: none; scrollbar-width: none;">
                                <div id="emptyPicture" class="text-left text-sm" style="width: 230px;">
                                    <img style="background-color: #ffffff;max-width: 100%" id="photoImg" src="{{$step['media_url']}}" alt="My Image">
                                    <div class="p-2 gradient-text">
                                        {!! $step['text'] !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </div>
                </div>
        </main>

        <div class="modal fade" id="newPost" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable">
                <div class="modal-content" style="height: fit-content">
                    <div class="modal-header ">
                        <h5 class="modal-title" id="exampleModalLabel">Укажите параметры</h5>
                        <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                            <input type="radio" class="btn-check" value="1" name="target_type" id="target_type1" autocomplete="off" checked>
                            <label class="btn btn-outline-primary" for="target_type1">В канал</label>

                            <input type="radio" class="btn-check" value="2" name="target_type" id="target_type2" autocomplete="off">
                            <label class="btn btn-outline-primary" for="target_type2">На сайт</label>

                            <input type="radio" class="btn-check" value="3" name="target_type" id="target_type3" autocomplete="off">
                            <label class="btn btn-outline-primary" for="target_type3">В чат-бот</label>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div id="target_type_1">
                            <form action="{{route('sendNewPossevLink.store',['post_id' => $step['id'], 'project_id' => $id])}}" method="GET">
                                <x-form.label-input class="mb-1 text-left w-100"  :value="'Введите название канала, где размещаетесь'" />
                                <input style="background: transparent;border-radius: 5px;border-color:#bdbdbd;height:55px"  id="timeStart" class="mt-2 w-100" name="group_name" placeholder="Название канала (не обязательно)"/>
                                <input id="target_type" name="target_type" value="1" type="hidden"/>

                                <div class="d-flex p-0 mt-3">
{{--                                <div class="col-4">--}}
{{--                                </div>--}}
                                    <label class="no-select" style="font-weight: 400;display: flex; align-items: center;cursor: pointer;">
                                        <input class="form-check-input ml-0 mt-0 position-relative" style="cursor: pointer;width: 25px; height: 25px;margin-right: 10px;" type="checkbox" name="creates_join_request">
                                        Заявка на вступление в канал для новых пользователей
                                    </label>
                                </div>
                                <button data-bs-dismiss="modal" class="btn mt-4 w-100 shine-button btn-primary" style="height: 50px;font-size: large; font-weight: 500;" type="submit"> Создать пост </button>
                            </form>
                        </div>
                        <div id="target_type_2" style="display: none;">
{{--                            <form action="{{route('newPossevLink.store',['post_id' => $step['id'], 'project_id' => $id])}}" method="GET">--}}
{{--                                <x-form.label-input class="mb-1 text-left w-100"  :value="'Введите название канала, где размещаетесь'" />--}}
{{--                                <input style="background: transparent;border-radius: 5px;border-color:#bdbdbd;height:55px"  id="timeStart" class="mt-2 w-100" name="group_name" placeholder="Название канала (не обязательно)"/>--}}
{{--                                <input id="target_type" name="target_type"  value="2" type="hidden"/>--}}

{{--                                <div class="d-flex p-0 mt-3">--}}
{{--                                <div class="col-4">--}}
{{--                                </div>--}}
{{--                                    <input type="checkbox" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px" name="creates_join_request" >--}}
{{--                                    <x-form.label-input class="text-left ml-2"  :value="'Заявка на вступление в канал для новых пользователей'" />--}}
{{--                                </div>--}}
{{--                                <x-form.label-input class="mb-1 mt-2 text-left w-100"  :value="'Кому отправить уведомления'" />--}}
{{--                                <select class="select2" name="telegram_ids[]" multiple style="width: 100%">--}}
{{--                                    @foreach($telegrams_ids as $telegrams_id)--}}
{{--                                        <option selected value="{{$telegrams_id->telegram_id}}"> {{$telegrams_id->full_name}} </option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                                <button data-bs-dismiss="modal" class="btn mt-4 w-100 shine-button btn-primary" style="" type="submit"> Создать пост </button>--}}
{{--                            </form>--}}
                            Скоро будет
                        </div>
                        <div id="target_type_3" style="display: none;">
{{--                            <form action="{{route('newPossevLink.store',['post_id' => $step['id'], 'project_id' => $id])}}" method="GET">--}}
{{--                                <x-form.label-input class="mb-1 text-left w-100"  :value="'Введите название канала, где размещаетесь'" />--}}
{{--                                <input style="background: transparent;border-radius: 5px;border-color:#bdbdbd;height:55px"  id="timeStart" class="mt-2 w-100" name="group_name" placeholder="Название канала (не обязательно)"/>--}}
{{--                                <input id="target_type" name="target_type"  value="3" type="hidden"/>--}}

{{--                                <div class="d-flex p-0 mt-3">--}}
{{--                                <div class="col-4">--}}
{{--                                </div>--}}
{{--                                    <input type="checkbox" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px" name="creates_join_request" >--}}
{{--                                    <x-form.label-input class="text-left ml-2"  :value="'Заявка на вступление в канал для новых пользователей'" />--}}
{{--                                </div>--}}
{{--                                <x-form.label-input class="mb-1 mt-2 text-left w-100"  :value="'Кому отправить уведомления'" />--}}
{{--                                <select class="select2" name="telegram_ids[]" multiple style="width: 100%">--}}
{{--                                    @foreach($telegrams_ids as $telegrams_id)--}}
{{--                                        <option selected value="{{$telegrams_id->telegram_id}}"> {{$telegrams_id->full_name}} </option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                                <button data-bs-dismiss="modal" class="btn mt-4 w-100 shine-button btn-primary" style="" type="submit"> Создать пост </button>--}}
{{--                            </form>--}}
                            Скоро будет
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    <style>
        .gradient-text {
            background: linear-gradient(135deg, #1e2b55, #393d55, #5a3249, #6e3d3f); /* Градиентный фон */
            padding: 20px;
            color: white; /* Цвет текста */
            line-height: 1.3; /* Межстрочный интервал */
        }

        /* Скрыть скроллбар для Webkit-браузеров (Chrome, Safari, Opera) */
        div[style*="overflow: auto;"]::-webkit-scrollbar {
            display: none;
        }

        /* Скрыть скроллбар для IE и Edge */
        div[style*="overflow: auto;"] {
            -ms-overflow-style: none; /* Скрыть скроллбар для IE и Edge */
            scrollbar-width: none; /* Скрыть скроллбар для Firefox */
        }

        .gradient-text p {
            margin: 0 0 10px; /* Отступы между абзацами */
        }

        .gradient-text b {
            color: #ffffff; /* Синий цвет для выделенного текста */
        }

        .gradient-text u {
            color: #768cfc; /* Синий цвет для подчёркнутого текста */
        }
        .gradient-text a {
            color: #768cfc; /* Синий цвет для подчёркнутого текста */
        }

        /* Изменение цвета текста и фона в выпадающем списке */
        .select2-container .select2-results__option {
            color: #333; /* Цвет текста */
            background-color: #f9f9f9; /* Цвет фона */
        }

        /* Изменение цвета текста и фона для выбранного элемента */
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #0d6efd; /* Цвет фона для выбранного элемента */
            color: #fff; /* Цвет текста для выбранного элемента */
        }

        /* Изменение цвета текста и фона при наведении */
        .select2-container .select2-results__option--highlighted {
            background-color: #0d6efd; /* Цвет фона при наведении */
            color: #fff; /* Цвет текста при наведении */
        }
        .no-select {
            user-select: none; /* Современные браузеры */
            -webkit-user-select: none; /* Safari */
            -moz-user-select: none; /* Firefox */
            -ms-user-select: none; /* Internet Explorer и старые версии Edge */
        }

    </style>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap5'
            })
        })
    </script>


<script>
        document.addEventListener('DOMContentLoaded', function () {
            const targetType1 = document.getElementById('target_type1');
            const targetType2 = document.getElementById('target_type2');
            const targetType3 = document.getElementById('target_type3');
            const targetDiv1 = document.getElementById('target_type_1');
            const targetDiv2 = document.getElementById('target_type_2');
            const targetDiv3 = document.getElementById('target_type_3');

            function toggleVisibility() {
                targetDiv1.style.display = targetType1.checked ? 'block' : 'none';
                targetDiv2.style.display = targetType2.checked ? 'block' : 'none';
                targetDiv3.style.display = targetType3.checked ? 'block' : 'none';
            }

            targetType1.addEventListener('change', toggleVisibility);
            targetType2.addEventListener('change', toggleVisibility);
            targetType3.addEventListener('change', toggleVisibility);

            // Initial check
            toggleVisibility();
        });
    </script>

@endsection
