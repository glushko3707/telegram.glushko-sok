@extends('adminPanel.startAddProject')
@section('title', 'Статистика посевов')

@section('content')

    <div class="content-wrapper">
        <main class="flex flex-grow-1 py-3">
                    @livewire('possev.possev-stat', ['id' => $id])
        </main>
    </div>

    <style>
        .thGroup {
            font-weight: 400;
            height: 30px;
            margin-bottom: 3px;
        }
        .thInput {
            background-color: rgba(0, 0, 0, 0);
            width: 120px;
            height: 30px;
        }
        .thInputSmall {
            background-color: rgba(0, 0, 0, 0);
            width: 100%;
            height: 30px;
        }
        .thH4 {
            width: 80px;
            font-weight: 500;
        }

        .thInputDate {
            background-color: rgba(0, 0, 0, 0);
            width: 100%;
            height: 30px;
        }


    </style>
@endsection
