<div class="container-fluid p-5">
    <div class="row mt-3" style="justify-content: center">
        <div class="card" style="width: 100%;padding: 0">
            <div class="card-tools">
                <div class="row">
                    <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                        <div class="w-100 row">
                            <div class="col-md-8 col-12">
                                <div class="row" style="padding: 15px 15px 0 15px">
                                    <div class="col-12 col-md-4 mb-2 d-flex align-items-center">
                                        <label class="mr-2" style="min-width: 35px">От</label>
                                        <input class="form-control" wire:model.lazy="date_from" type="date" name="date_from">
                                    </div>
                                    <div class="col-12 col-md-4 mb-2 d-flex align-items-center">
                                        <label class="mr-2" style="min-width: 35px">До</label>
                                        <input class="form-control" wire:model.lazy="date_to" type="date" name="date_to">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12 mb-2 d-flex align-items-center justify-content-end">
                                <button class="btn btn-primary ml-auto" wire:click="newLinkPossev" wire:loading.attr="disabled" wire:loading.class="btn-secondary">
                                    <span wire:loading.remove>Добавить Посев</span>
                                    <span wire:loading>Загрузка...</span>
                                </button>
                            </div>
                        </div>
                        </div>

                    </div>

                </div>
            </div>
            <!-- /.card-header -->
            <div class="card" style="margin-bottom: 0;box-shadow: unset">

                <div class="card-body table-responsive p-0" style="max-height: 450px;max-width: 1800px">
                    <table class="table table-head-fixed table-sm table-bordered text-nowrap">
                        <thead>
                            <tr>
                                <th style="width: 200px" rowspan="1">Посев</th>
                                <th rowspan="1" style="width: 170px;text-align: center">Пост</th>
                                <th colspan="1" style="width: 300px;text-align: center">Канал размещения</th>
                                <th colspan="1" style="width: 380px;text-align: center">Условия размещения</th>
                                <th colspan="1" style="width: 170px;text-align: center">Статистика</th>
                                <th colspan="1" style="width: 170px;text-align: center">Комментарий</th>
                                <th colspan="1"style="width: 170px;text-align: center">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($possevLink as $index => $link)
                                <tr>
                                    <th style="padding-left: 18px">
                                        <div class="row">
                                            <div class="col-9" style="font-size: 16px;padding: 0 0 0 0;font-weight: 400;">
                                            </div>
                                            <div class="col-3"  style="font-weight: 200; justify-content: flex-end;font-size: 12px;margin: 0; padding: 0 6px 0 0; text-align: right;">
                                                ID:  {{$link['id'] ?? ''}}
                                            </div>
                                        </div>
                                        <div class="row" style="font-size: 16px;padding: 1px 0 0 0;font-weight: 500;">
                                            {{$link['link_name_possev'] ?? ''}}
                                        </div>

                                        <div class="row" style="font-size: 13px;padding: 1px 0 0 0;font-weight: 400;">
                                            {{$link['link_possev']}}
                                        </div>

{{--                                        <input wire:model.lazy="possevLink.{{ $index }}.id" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 120px" name="id{{ $index }}">--}}
                                    </th>
                                    <th>
                                        <div class="row" style="font-weight: 200; justify-content: flex-end;font-size: 12px;margin: 0; padding: 0 2px 0 0; text-align: right;">
                                            ID: {{$link['post']['id'] ?? ''}}
                                        </div>
                                        @if(isset($link['post']['name']) && $link['post']['name'] != '')
                                        <div class="row" style="padding: 0 0 0 10px;font-weight: 400;">
                                                {{$link['post']['name']}}
                                        </div>
                                        @else
                                            <div class="row" style="padding: 0 0 0 00px">
                                                <div class="col-12">
                                                    <select wire:model.live="possevLink.{{ $index }}.posev_post_id" class="form-control">
                                                    <option class="w-100" value=""></option>

                                                    @foreach($posts as $post)
                                                            <option value="{{$post['id']}}">{{$post['name']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                        </div>
                                        @endif
                                    </th>
                                    <th class="p-2">
                                        <div class="d-flex align-items-center thGroup">
                                            <div class="thH4">
                                                Канал:
                                            </div>
                                            <div class="w-100">
                                                <input wire:model.live="possevLink.{{ $index }}.group_name" class="{{$link['group_name'] == '' ? 'form-control' : 'input-opacity'}} w-100 thInput">
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-center thGroup">
                                            <div class="thH4">
                                                Ссылка:
                                            </div>
                                            <div class="w-100">
                                                <input wire:model.live="possevLink.{{ $index }}.group_link" class="{{$link['group_link'] == '' ? 'form-control' : 'input-opacity'}} w-100 thInput">
                                            </div>
                                        </div>
{{--                                        <div style="display: none">--}}
{{--                                            <div class="d-flex align-items-center thGroup">--}}
{{--                                            <div class="thH4">--}}
{{--                                                Админ:--}}
{{--                                            </div>--}}
{{--                                            <div class="w-100">--}}
{{--                                                <input wire:model.lazy="possevLink.{{ $index }}.admin_link" class="form-control w-100 thInput" style="" name="id{{ $index }}">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        </div>--}}
                                    </th>
                                    <th class="p-2">
                                        <div class="row">
                                           <div class="col-6">
                                               <div class="d-flex align-items-center thGroup">
                                                   <div class="thH4">
                                                       Цена:
                                                   </div>
                                                   <div class="w-100">
                                                       <input type="number" wire:model.live="possevLink.{{ $index }}.money_price" class="{{$link['money_price'] == '' ? 'form-control' : 'input-opacity'}}  w-100 thInputSmall">
                                                   </div>
                                               </div>
                                               <div class="d-flex align-items-center thGroup">
                                                   <div class="w-100">
                                                       <input wire:model.live="possevLink.{{ $index }}.date" type="datetime-local" class="{{$link['date'] == '' ? 'form-control' : 'input-opacity'}} thInputDate">
                                                   </div>
                                               </div>


                                           </div>
                                           <div class="col-6">
                                               <div class="d-flex align-items-center thGroup">
                                                   <div class="thH4">
                                                       Охват:
                                                   </div>
                                                   <div class="w-100">
                                                       <input type="number" wire:model.lazy="possevLink.{{ $index }}.impressions" class="{{$link['impressions'] == '' ? 'form-control' : 'input-opacity'}} w-100 thInputSmall" style="" name="impressions{{ $index }}">
                                                   </div>
                                               </div>
                                               <div class="d-flex align-items-center thGroup">
                                                   <div class="thH4">
                                                       CPV:
                                                   </div>
                                                   <div class="w-100">
                                                       @if(!empty($link['impressions']) && $link['money_price'] > 0)
                                                           {{ round($link['money_price'] / $link['impressions']*1000) }}
                                                       @endif
                                                   </div>
                                               </div>

                                           </div>
                                        </div>

                                    </th>

                                    <th>
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="d-flex align-items-center thGroup">
                                                    <div class="col-12">
                                                        Подписки:
                                                    </div>
                                                    <div class="w-100">
                                                        {{$link['subscriptions'] ?? 0 }} ({{$link['unsubscribes']}})
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="d-flex align-items-center thGroup">
                                                    <div class="col-12">
                                                        Цена PDP:
                                                    </div>
                                                    <div class="w-100">
                                                        @if(isset($link['subscriptions']) && $link['subscriptions'] > 0)
                                                            {{ round($link['money_price'] / $link['subscriptions']) }} ₽
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <textarea rows="3" wire:model.live="possevLink.{{ $index }}.comment" class="input-opacity text-sm custom-textarea" style="line-height: 1.5;background-color: rgba(0, 0, 0, 0);width: 170px"> </textarea>
                                    </th>
                                    <th>
                                        <div class="p-2">
                                            <div class="row mb-1">
                                                <button wire:click="tgSend({{$link['id']}})" wire:loading.attr="disabled" class="btn-sm btn btn-outline-primary">
                                                    <span wire:loading.remove>Отправить в бот</span>
                                                    <span wire:loading>Загрузка...</span>
                                                </button>
                                            </div>
                                            <div class="row">
                                                <button wire:click="delete({{$link['id']}})" wire:loading.attr="disabled" class="btn-sm btn btn-outline-danger">
                                                    <span wire:loading.remove>Удалить</span>
                                                    <span wire:loading>Загрузка...</span>
                                                </button>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>


            </div>

        </div>

    <div class="modal fade" id="newPost" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content" style="height: fit-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="exampleModalLabel">Укажите параметры</h5>
                    <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                        <input type="radio" class="btn-check" value="1" name="target_type" id="target_type1" autocomplete="off" checked>
                        <label class="btn btn-outline-primary" for="target_type1">В канал</label>

                        <input type="radio" class="btn-check" value="2" name="target_type" id="target_type2" autocomplete="off">
                        <label class="btn btn-outline-primary" for="target_type2">На сайт</label>

                        <input type="radio" class="btn-check" value="3" name="target_type" id="target_type3" autocomplete="off">
                        <label class="btn btn-outline-primary" for="target_type3">В чат-бот</label>
                    </div>
                </div>
                <div class="modal-body">
                    <div id="target_type_1">
                        <form wire:submit.prevent="newLinkPossev">
                            <input id="target_type" wire:model="target_type" value="1" type="hidden"/>
                            <div class="d-flex p-0 mt-3">
                                <label class="no-select" style="font-weight: 400;display: flex; align-items: center;cursor: pointer;">
                                    <input class="form-check-input ml-0 mt-0 position-relative" style="cursor: pointer;width: 25px; height: 25px;margin-right: 10px;" type="checkbox" wire:model="creates_join_request">
                                    Заявка на вступление в канал для новых пользователей
                                </label>
                            </div>
                            <button class="btn mt-4 w-100 shine-button btn-primary" style="height: 50px;font-size: large; font-weight: 500;" type="submit" data-bs-dismiss="modal">Создать пост</button>
                        </form>
                    </div>
                    <div id="target_type_2" style="display: none;">
                        Скоро будет
                    </div>
                    <div id="target_type_3" style="display: none;">
                        Скоро будет
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


@script
<script>
    $wire.on('alerts', message => {
            alert(message.title);
        });
</script>
@endscript