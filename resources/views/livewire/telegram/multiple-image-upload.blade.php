<div class="card-body">
    <form method="POST" id="pipeline_step" enctype="multipart/form-data" action="{{ route('smartsenderAddPicture.store',$id) }}">
        <div class="row">
            <div class="col-6">
                @csrf
                <div class="row mb-1 picture">
                    <label for="photoPost post enterType" class="col-md-6 col-form-label text-md-end">Картинка</label>
                    <div class="col-md-6">
                        <input id="image" wire:model="image" type="file" class="form-control" name="photoPost" accept="image/*">
                    </div>
                    <div>
                    </div>
                </div>
                <div id="paramPicture" style="display: {{ $paramPicture_display }}">
                    <div class="row mb-1">
                        <label for="name_file" class="col-md-6 col-form-label text-md-end">Загрузка аватара</label>
                        <div class="col-md-6 col-form-label text-md-end" style="display: flex;">
                            <div class="form-switch" style="justify-content: left;display: flex;">
                                <input id="avatar" wire:model.live="avatar" checked type="checkbox" class="form-check-input">
                            </div>
                        </div>
                    </div>
                    <div id="avatar" style="display: {{ $avatar_display }}">
                        <div class="row mb-1" style="display: none">
                            <label for="name_file" class="col-md-6 col-form-label text-md-end">Название</label>
                            <div class="col-md-6 col-form-label text-md-end" style="display: flex;">
                                <input id="name_file" wire:model.live="name_file" type="text" class="form-control" name="name_file">
                                <div>@error('name_file') {{ $message }} @enderror</div>
                            </div>
                        </div>

                        <div class="row mb-1">
                            <label for="pipelineId" class="col-md-6 col-form-label text-md-end">Расположение по горизонтали</label>
                            <div class="col-md-4 col-form-label text-md-end" style="display: flex;">
                                <input wire:model.live="x" id="x" type="range" min="1" max="200" class="custom-range" name="x">
                            </div>
                            <div class="col-md-2 p-0" style="display: flex;">
                                <input id="size" wire:model.live="x" type="number" min="1" max="200" class="form-control numberFormControl" name="x">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label for="pipelineId" class="col-md-6 col-form-label text-md-end">Расположение по вертикали</label>
                            <div class="col-md-4 col-form-label text-md-end" style="display: flex;">
                                <input wire:model.live="y" id="y" type="range" min="1" max="200" class="custom-range" name="y">
                            </div>
                            <div class="col-md-2 p-0" style="display: flex;">
                                <input id="size" wire:model.live="y" type="number" min="1" max="200" class="form-control numberFormControl" name="y">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label for="pipelineId" class="col-md-6 col-form-label text-md-end">Размер</label>
                            <div class="col-md-4 col-form-label text-md-end" style="display: flex;">
                                <input id="size" wire:model.live="size" type="range" min="1" max="200" class="custom-range" name="size">
                            </div>
                            <div class="col-md-2 p-0" style="display: flex;">
                                <input id="size" wire:model.live="size" type="number" min="1" max="200" class="form-control numberFormControl" name="size">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-1">
                        <label for="name_file" class="col-md-6 col-form-label text-md-end">Вставить текст</label>
                        <div class="col-md-6 col-form-label text-md-end" style="display: flex;">
                            <div class="form-switch" style="justify-content: left;display: flex;">
                                <input wire:model.live="text"  type="checkbox" class="form-check-input">
                            </div>
                        </div>
                    </div>

                    <div id="text" style="display: {{ $text_display }}">
                        <div class="row mb-1">
                            <label for="pipelineId" class="col-md-6 col-form-label text-md-end">Содержание текста</label>
                            <div class="col-md-6 col-form-label text-md-end" style="display: flex;">
                                <input id="size" wire:model.live="text_text" value="Имя фамилия" type="text" class="form-control" name="text_text">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label for="pipelineId" class="col-md-6 col-form-label text-md-end">Выравнивание текста</label>
                            <div class="col-md-6 col-form-label text-md-end" style="display: flex;">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" wire:model.live="text_align" type="radio" name="text_align" id="inlineRadio1" value="left">
                                    <label class="form-check-label" for="inlineRadio1">Лево</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input"  wire:model.live="text_align"  type="radio" name="text_align" id="inlineRadio2" value="center">
                                    <label class="form-check-label" for="inlineRadio2">Центр</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input"  wire:model.live="text_align"  type="radio" name="text_align" id="inlineRadio3" value="right">
                                    <label class="form-check-label" for="inlineRadio3">Право</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label for="pipelineId" class="col-md-6 col-form-label text-md-end">По горизонтали</label>
                            <div class="col-md-4 col-form-label text-md-end" style="display: flex;">
                                <input id="size" wire:model.live="text_x" type="range" min="1" max="200" class="custom-range" name="text_x">
                            </div>
                            <div class="col-md-2 p-0" style="display: flex;">
                                <input id="size" wire:model.live="text_x" type="number" min="1" max="200" class="form-control numberFormControl" name="text_x">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label for="pipelineId" class="col-md-6 col-form-label text-md-end">По вертинкали</label>
                            <div class="col-md-4 col-form-label text-md-end" style="display: flex;">
                                <input id="size" wire:model.live="text_y" type="range" min="1" max="200" class="custom-range" name="text_y" >
                            </div>
                            <div class="col-md-2 p-0" style="display: flex;">
                                <input id="size" wire:model.live="text_y" type="number" min="1" max="200" class="form-control numberFormControl" name="text_y">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <label for="pipelineId" class="col-md-6 col-form-label text-md-end">Размер текста</label>
                            <div class="col-md-4 col-form-label text-md-end" style="display: flex;">
                                <input id="size" wire:model.live="font_size" type="range" min="1" max="200" class="custom-range" name="font_size">
                            </div>
                            <div class="col-md-2 p-0" style="display: flex;">
                                <input id="size" wire:model.live="font_size" type="number" min="1" max="200" class="form-control numberFormControl" name="font_size">
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="col-6 ">
                <div class="col-md-12 d-flex justify-content-center">
{{--                    <img id="preview-image-before-upload"  style="max-height: 250px;">--}}
                    @if ($image)
                                    <img src="{{ $imaging }}" style="max-height: 250px;">
                    @endif
                </div>
                @if ($image)

                    <div class="col mt-5 d-flex justify-content-center">
                        @if ($setting->platform_name == 'smartsender')
                        <div class="ml-1" x-data="copyUrl">
                            <button x-on:click="copyUrl" class="btn btn-outline-primary" type="button"> URL </button>
                        </div>
                        <div x-data="copyLinks" class="mr-1">
                            <input id="copyLink" value="{{$copyLink}}" wire:model.live="copyLink" type="hidden"  name="copyLink">
                            <button x-on:click="copyLinks" class="btn btn-outline-primary" type="button"> Тело запроса </button>
                        </div>
                        @endif

                        @if($setting->platform_name == 'salebot')
                            <div class="ml-1" x-data="copyUrlSalebot">
                                <button x-on:click="copyUrlSalebot" class="btn btn-outline-primary" type="button"> URL запроса</button>
                            </div>
                            <div class="ml-1" x-data="copyLinksSaleBot">
                                <input id="copyLinksSaleBot" value="{{$copyLinksSaleBot}}" wire:model.live="copyLinksSaleBot" type="hidden"  name="copyLinksSaleBot">
                                <button x-on:click="copyLinksSaleBot" class="btn btn-outline-primary" type="button"> JSON параметры Salebot</button>
                            </div>
                        @endif

                    </div>
                @endif

            </div>
        </div>
        <div class="d-flex mt-3 justify-content-center">
            <div class="m-2 {{$image == '' ? 'd-none' : ''}}">
                <button  type="submit" class="btn btn-primary" style="align-items: center;margin: auto; width: 226px;">
                    Сохранить
                </button>
            </div>
        </div>
    </form>
</div>


@script
<script>

    Alpine.data('copyLinks', () => {
        return {
            copyLinks() {
                // Получаем элемент input по id
                let input = document.getElementById("copyLink");
                // Получаем текст из значения input
                let text = input.value;

                // Копируем текст в буфер обмена с помощью Async Clipboard API
                navigator.clipboard.writeText(text)
                    .then(() => {
                        // Выводим сообщение об успехе
                        console.log("Текст скопирован в буфер обмена: " + text);
                    })
                    .catch((error) => {
                        // Выводим сообщение об ошибке
                        console.error("Не удалось скопировать текст в буфер обмена: ", error);
                    });
            },
        }
    })

    Alpine.data('copyLinksSaleBot', () => {
        return {
            copyLinksSaleBot(){
                let inputs = document.getElementById("copyLinksSaleBot");
                // Получаем текст из значения input
                let url = inputs.value;

                navigator.clipboard.writeText(url)
                    .then(() => {
                        // Выводим сообщение об успехе
                        console.log("Текст скопирован в буфер обмена: " + url);
                    })
                    .catch((error) => {
                        // Выводим сообщение об ошибке
                        console.error("Не удалось скопировать текст в буфер обмена: ", url);
                    });
            }
        }
    })

    Alpine.data('copyUrl', () => {
        return {
            copyUrl(){
                let url = "https://telegram.glushko-sok.ru/api/smartsender/new/sendPicture/{{$id}}";
                navigator.clipboard.writeText(url)
                    .then(() => {
                        // Выводим сообщение об успехе
                        console.log("Текст скопирован в буфер обмена: " + url);
                    })
                    .catch((error) => {
                        // Выводим сообщение об ошибке
                        console.error("Не удалось скопировать текст в буфер обмена: ", url);
                    });
            }
        }
    })
    Alpine.data('copyUrlSalebot', () => {
        return {
            copyUrlSalebot(){
                let url = "https://telegram.glushko-sok.ru/api/salebot/sendPicture/{{$id}}";
                navigator.clipboard.writeText(url)
                    .then(() => {
                        // Выводим сообщение об успехе
                        console.log("Текст скопирован в буфер обмена: " + url);
                    })
                    .catch((error) => {
                        // Выводим сообщение об ошибке
                        console.error("Не удалось скопировать текст в буфер обмена: ", url);
                    });
            }
        }
    })

</script>
@endscript

