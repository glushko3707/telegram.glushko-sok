<div>


    <div class="row">
        <div class="col-lg-2 col-6">
            <div class="small-box" style="height: 100px">
                <div class="inner">
                    <p> Всего подписчиков </p>
                    <h1> {{$stat ['count'] ?? ''}} </h1>
                </div>
                <div class="icon">
                    <i class="ion ion-speedometer"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box" style="height: 100px">
                <div class="inner d-flex">
                    <div>
                        <p> Покинули </p>
                        <div class="d-flex">
                         <h1> {{$stat ['left'] ?? ''}} </h1> <p class="text-sm"> ({{$stat ['uniqLeft']}})</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-6 mr-5">
            <!-- Виджет 1: Остаток бюджета -->
            <button wire:click="save" type="button"  class="btn period btn-outline-primary">Скачать файл</button>
        </div>
    </div>


    <div>
        <section class="content">
            <div class="container-fluid">
                <div class="row mt-3" style="justify-content: center">


                    <div class="card" style="padding: 0">


                        <div class="card-tools">
                            <div class="d-flex justify-content-between" style="padding: 10px">
                                <div style="align-items: center;" class="d-flex">
                                    <div>
                                        <ul class="nav nav-pills">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#detal" data-toggle="tab">Лиды</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="ml-3">
                                    <select class="form-control input-opacity" wire:model.live="linkName" name="linkName"  style="background-color: rgba(0, 0, 0, 0)">
                                        <option value="all">Все</option>
                                    @foreach($links as $link)
                                            <option value="{{$link}}">{{$link}}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <div>
                                    <div class="form-inline float-right">
                                        <label  class="mr-3">От</label>
                                        <input class="form-control mr-3" type="date" wire:model.live="date_from" name="date_from">
                                        <label  class="mr-3">До</label>
                                        <input class="form-control mr-3" type="date" wire:model.live="date_to" name="date_to">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active" id="detal">
                                <div class="card" style="margin-bottom: 0;box-shadow: unset">
                                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                                        <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">
                                            <thead>
                                            <tr>
                                                <th style="width: 150px">Дата и время</th>
                                                <th style="width: 150px">Telegram ID</th>
                                                <th style="width: 175px">UserName</th>
                                                <th style="width: 150px">Имя</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($leadses as $lead)
                                                <tr >

                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{\Illuminate\Support\Carbon::parse($lead['created_at'])->day  . ' ' . \Illuminate\Support\Carbon::parse($lead['created_at'])->locale('ru')->getTranslatedMonthName('Do MMMM') }}
                                                                {{ \Illuminate\Support\Carbon::parse($lead['created_at'])->format('H:i') }}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['telegram_id']}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['telegram_username'] ?? ''}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['fullName'] ?? ''}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /.card-header -->

                    </div>
                    <div class="d-flex justify-content-center">
                        {{$leadses->links()}}
                    </div>
                </div>
            </div>
        </section>
    </div>



</div>

