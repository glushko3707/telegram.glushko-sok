<button {!! $attributes->merge(['class' => 'w-100', 'style' => '
    font-size: 23px;
    color: #ffffff;
    background: #161616;
    border-radius: 5px;
    height:50px']) !!}>
    {{ $slot }}
</button>