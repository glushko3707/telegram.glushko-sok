<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900">
            {{ __('Новый проект') }}
        </h2>

    </header>


    <form method="post" action="{{ route('createProject.store') }}" class="mt-6 space-y-6">
        @csrf


        <div>
            <x-input-label for="name" :value="__('Имя проекта')" />
            <x-text-input id="name" name="name" type="text" class="mt-1 block w-full"  required autofocus autocomplete="name" />
            <x-input-error class="mt-2" :messages="$errors->get('name')" />
        </div>
        <div>
            <x-input-label for="botName" :value="__('botName')" />
            <x-text-input id="botName" name="botName" type="text" class="mt-1 block w-full"  />
            <x-input-error class="mt-2" :messages="$errors->get('botName')" />
        </div>
        <div class="flex items-center gap-4">
            <x-primary-button>
                {{ __('Создать') }}
            </x-primary-button>
        </div>

    </form>


</section>
