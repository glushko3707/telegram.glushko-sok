<section>
    <header>
        <a href="{{route('SmartsenderViewPicture.index',$project->id)}}" class="text-lg font-medium text-gray-900">
            {{ __($project->name) }}
        </a>

    </header>



    <form method="post" action="{{ route('project.update',$project->id) }}" class="mt-6 space-y-6">
        @csrf

        <div>
            <x-input-label for="name" :value="__('Имя проекта')" />
            <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $project->name ?? '')" required autofocus autocomplete="name" />
            <x-input-error class="mt-2" :messages="$errors->get('name')" />
        </div>
        <div>
            <x-input-label for="botName" :value="__('botName')" />
            <x-text-input id="botName" name="botName" type="text" class="mt-1 block w-full" :value="old('botName', $project->botName ?? '')" />
            <x-input-error class="mt-2" :messages="$errors->get('botName')" />
        </div>


        <div class="flex items-center gap-4">
            <a href="{{route('SmartsenderViewPicture.index',$project->id)}}" class="'inline-flex items-center px-4 py-2 bg-gradient-primary border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 transition ease-in-out duration-150'">
                Перейти
            </a>

            <x-primary-button>
                {{ __('Сохранить') }}
            </x-primary-button>
        </div>
    </form>
</section>
