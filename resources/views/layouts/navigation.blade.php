<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="shrink-0 flex items-center">
                    <a href="{{ route('profile.edit') }}">
                        <x-application-logo class="block h-9 w-auto fill-current text-gray-800" />
                    </a>
                </div>

                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('profile.edit')" :active="request()->routeIs('profile.edit')">
                        {{ __('Профиль') }}
                    </x-nav-link>
                </div>

                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('project.index')" :active="request()->routeIs('project.index')">
                        {{ __('Проект') }}
                    </x-nav-link>
                </div>
                @if(auth()->user()->name == 'Maks Glushko')
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('linkInvite.index')" :active="request()->routeIs('linkInvite.index')">
                        {{ __('Инвайты') }}
                    </x-nav-link>
                </div>
                @endif
            </div>

            <!-- Settings Dropdown -->
            <div class="hidden sm:flex sm:items-center sm:ml-6" >
                <div class="dropdown">
                    <button class="btn btn-sm text-gray border dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false" style="border-radius: 5px;padding: 6px 25px;">
                        {{ auth()->user()->name }}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('profile.edit')}}">Профиль</a>
                        <form action="{{route('logout')}}" method="POST">
                            @csrf
                            <button class="dropdown-item" onclick="this.form.submit()">Выйти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
</nav>
