@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <main class="flex flex-grow-1 py-3">
            <div class="container" >

                <div class="card" >
                    <div class="card-header text-center"> Добавить новую картинку Smartsender </div>

                    @livewire('Telegram.Multiple-image-upload', [
                    'id' => $id,
                    ])
                </div>
            </div>
        </main>
    </div>


@endsection

