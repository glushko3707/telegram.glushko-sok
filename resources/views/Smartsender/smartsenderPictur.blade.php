@extends('adminPanel.startAddProject')
@section('title', 'Редактор картинок')

@section('content')
    <script>
  // Находим элементы по их id
  var image = $("#photoImg");
  var input = $("#photoPost");

  // Добавляем обработчик события click к элементу <img>
  image.click(function() {
    // Показываем или скрываем элемент <input>
    input.toggle();
  });
</script>

    <div class="content-wrapper">
        <main class="flex flex-grow-1 py-3">
                <div class="container">
                    <div class="row">
                        @foreach($date as $key => $step)

                        <div class="col-3 p-3 mt-3" style="width: 250px;">

                                <div id="emptyPicture" class="text-center" style="width: 200px;">
                                    <img  style="background-color: #ffffff; max-height: 200px" id="photoImg" src="{{$step['picture']}}" alt="My Image">
                                    {{$step['picture_name']}}
                                </div>

                                <div class="d-flex mt-2">
                                    <div class="mr-1">
                                        <a style="width: 98px" href="{{route('smartsenderUpdatePicture.index',['id' => $id,'picture_name' => $step['picture_name']])}}" class="btn btn-sm btn-outline-primary">
                                            Запрос
                                        </a>
                                    </div>
                                    <div>
                                        <form action="{{route('smartsenderPicture.delete',$id)}}" method="POST">
                                            @csrf @method('DELETE')
                                            <input type="hidden" value="{{$step['picture_name']}}" name="picture_name" >
                                            <button style="width: 98px" type="submit" class="btn btn-sm btn-outline-danger"> Удалить </button>
                                        </form>
                                    </div>
                                </div>

                            </div>

                        @endforeach
                    </div>

                    <div class="d-flex mt-3 justify-content-center">
                        <div class="m-2">
                            <a href="{{route('smartsenderAddPicture.index',$id)}}" class="btn btn-primary" style="align-items: center;margin: auto; width: 226px;">
                                Добавить картинку
                            </a>
                        </div>
                    </div>


                </div>
        </main>
    </div>

@endsection
