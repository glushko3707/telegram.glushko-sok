@extends('adminPanel.startAddProject')

@section('content')


    <div class="container" id="app">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Настройки интеграции со SmartSender </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('platform_name.store',$id)}}">
                            @csrf
                            <div class="row mb-3">
                                <label for="platform_name" class="col-md-4 col-form-label text-md-end"> Выберите сервис</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <select onchange="this.form.submit()" class="form-control input-opacity" id="platform_name" name="platform_name" style="min-width: 150px; background-color: rgba(0, 0, 0, 0)">
                                            <option {{$setting->platform_name == 'salebot' ? 'selected' : '' }} value="salebot">salebot</option>
                                            <option {{$setting->platform_name == 'smartsender' ? 'selected' : '' }} value="smartsender">smartsender</option>
                                        </select>
                                    </div>
                                    @if($errors->any())
                                        <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>
                                    @endif
                                </div>
                            </div>
                        </form>
                            @if($setting->platform_name == 'smartsender')
                                <form action="{{route('Setting.store',$id)}}" method="POST">
                                <div class="row mb-3">
                                    <label for="smartSender_token" class="col-md-4 col-form-label text-md-end"> Smartsender Api Key </label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control" id="smartSender_token" value="{{$setting->smartSender_token}}" name="smartSender_token" >
                                            <span class="input-group-append">
                                      <button type="submit" class="btn btn-primary">Сохранить</button>
                                    </span>
                                        </div>
                                        @if($errors->any())
                                            <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>
                                        @endif
                                    </div>
                                </div>
                                </form>
                            @endif







                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>
        import TestComponent from "../../js/TestComponent";
        var copyButton = document.getElementById('copyButton');
        var paragraph = document.getElementById('paragraph');


        copyButton.addEventListener('click', function() {

            // Используем библиотеку clipboard.js для копирования текста в буфер обмена
            var temp = document.createElement("textarea");
            document.body.appendChild(temp);
            temp.value = paragraph.innerText;
            temp.select();
            document.execCommand("copy");
            document.body.removeChild(temp);


            // Визуальное подтверждение копирования
            copyButton.innerText = 'Скопировано!';
            setTimeout(function(){
                copyButton.innerText = 'Скопировать';
            }, 2000);
        });

    </script>



@endsection
