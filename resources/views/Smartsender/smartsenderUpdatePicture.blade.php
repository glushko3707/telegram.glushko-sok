@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <main class="flex flex-grow-1 py-3">
            <div class="container" >

                <div class="card" >
                    <div class="card-header text-center"> Добавить новую картинку Smartsender </div>

                    @livewire('Telegram.Image-update', [
                        'id' => $id,
                        'picture_name' => $picture_name,
                    ])


                </div>
            </div>
        </main>
    </div>


@endsection

