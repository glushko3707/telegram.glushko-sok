@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Настройки сервиса Telegram </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('telegramSetting.store',$id)}}">
                            @csrf



                            <div class="row mb-3">
                                <label for="bot_api_key" class="col-md-4 col-form-label text-md-end"> Telegram Api Key </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="bot_api_key" value="{{$setting->bot_api_key}}" name="bot_api_key" >
                                        <span class="input-group-append">
                                      <button type="submit" class="btn btn-primary">Сохранить</button>
                                    </span>
                                    </div>
                                    @if($errors->any())
                                        <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>
                                    @endif
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
