import './bootstrap';
import { createApp } from "vue/dist/vue.esm-bundler";
import TestComponent from "./components/updatePost/TestComponent.vue";

const app = createApp({
    components:{
        'TestComponent': TestComponent
    }
});


app.mount('#vue-app')