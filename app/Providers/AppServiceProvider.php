<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind('TelegramBotTrait', function () {
            return new \App\Traits\TelegramBotTrait;
        });

        $this->app->bind('SmartsenderTrait', function () {
            return new \App\Traits\SmartsenderTrait;
        });


    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Paginator::useBootstrap();
    }
}
