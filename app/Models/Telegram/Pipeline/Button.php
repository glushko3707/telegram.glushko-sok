<?php

namespace App\Models\Telegram\Pipeline;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Button extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','button_text','callback_action','contact','message_id','oneTimeKeyboard','type','url',
    ];

    public function getMessage(): BelongsTo
    {
        return $this->belongsTo(Message::class,'callback_action','callback_data');
    }

    public function getPipeline(): BelongsTo
    {
        return $this->belongsTo(Pipeline::class,'callback_action','callback_data');
    }


}
