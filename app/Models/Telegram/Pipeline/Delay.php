<?php

namespace App\Models\Telegram\Pipeline;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delay extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','daley','onlyToday','tomorrow','timeSend','type',
    ];
}
