<?php

namespace App\Models\Telegram\Pipeline;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Pipeline extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','pipeline_id','priority','content_type','content_id','callback_data',
    ];

    public function getMessage(): HasOne
    {
        return $this->hasOne(Message::class,'id','content_id');
    }

    public function getDaley(): HasOne
    {
        return $this->hasOne(Delay::class,'id','content_id');
    }



}
