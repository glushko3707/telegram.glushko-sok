<?php

namespace App\Models\Telegram\Pipeline;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Message extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','text','mediaUrl','media_type','avatar','callback_data',
    ];

    public function getButtons(): hasMany
    {
        return $this->hasMany(Button::class,'message_id');
    }

}
