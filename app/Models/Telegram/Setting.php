<?php

namespace App\Models\Telegram;

use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Setting extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','api_key','smartSender_token','platform_name','group_telegram',
    ];

    public function getProject(): BelongsTo
    {
        return $this->belongsTo(Project::class,'id','project_id');
    }

}
