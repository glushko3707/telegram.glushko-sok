<?php

namespace App\Models\Telegram;

use App\Models\InviteLink;
use App\Models\LefmTelegramMember;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class UserChannel extends Model
{
    use HasFactory;

    protected $fillable = [
        'channel_id', 'project_id', 'link', 'active',
        'telegram_username', 'fullName', 'telegram_id', 'link_name', 'is_bot'
    ];

    public function getLink(): HasOne
    {
        return $this->hasOne(InviteLink::class, 'link', 'link');
    }

    public function getLeftUser(): HasOne
    {
        return $this->hasOne(LefmTelegramMember::class, 'telegram_id', 'telegram_id')
                    ->whereColumn('channel_id', 'channel_id');
    }
}
