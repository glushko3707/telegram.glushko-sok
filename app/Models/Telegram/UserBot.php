<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBot extends Model
{
    use HasFactory;

    protected $fillable = [
        'bot_username','project_id','active','start_bot',
        'telegram_username','telegram_id','fullName'
    ];

}
