<?php

namespace App\Models\Telegram;

use App\Models\Webinar\WebinarUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoryTelegramMessage extends Model
{
    use HasFactory;

      protected $fillable = [
          'project_id','telegram_id','message_id','type','text','media','media_type',
      ];

    public function getUserBot()
    {
        return $this->hasOne(UserBot::class,'telegram_id','telegram_id');
    }

}
