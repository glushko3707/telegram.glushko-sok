<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InviteStat extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id',
        'telegram_id',
        'inviteFirst',
        'inviteSecond',
        'inviteThird',
    ];
}
