<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LefmTelegramMember extends Model
{
    use HasFactory;
    protected $fillable = [
        'channel_id', 'project_id','telegram_id',
        'telegram_username','fullName'
    ];

    protected $table = 'lefm_telegram_members';
}

