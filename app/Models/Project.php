<?php

namespace App\Models;

use App\Models\Telegram\Setting;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Project extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','botName', 'created_at','updated_at',

    ];

    public function getProjectSetting(): HasOne
    {
        return $this->hasOne(Setting::class,'project_id');
    }

    public function getChannel(): HasOne
    {
        return $this->hasOne(Channel::class,'project_id');
    }

    public function getLink(): HasOne
    {
        return $this->hasOne(InviteLink::class,'project_id');
    }

}
