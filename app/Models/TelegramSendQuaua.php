<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegramSendQuaua extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id',  'telegram_id',  'pipeline_step',  'timeSend', 'pipeline_id',
        ];
}
