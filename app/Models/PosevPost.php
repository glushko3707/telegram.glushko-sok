<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PosevPost extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id','name','text','media_type','media','active',
    ];

}
