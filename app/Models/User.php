<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Telegram\UserChannel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'telegram_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];


    public function getLink(): HasMany
    {
        return $this->hasMany(InviteLink::class,'user_id');
    }

    public function getMyInvite(): HasManyThrough
    {
        return $this->hasManyThrough(UserChannel::class,InviteLink::class,
            'user_id', // Foreign key on the InviteLink... т.е. вторая
            'link',
            'id', // От модели, где делается связь (USER)
            'link',
        );
    }


    public function getPermissionsProject(): HasManyThrough
    {
        return $this->hasManyThrough(Project::class, Permissoin::class,
            'user_id', // Foreign key on the cars table...
            'id', // Foreign key on the owners table...
            'id', // Local key on the mechanics table...
            'project_id'); // Local key on the cars table...
    }


    public function getCode(): HasOne
    {
        return $this->hasOne(ConnectTelegram::class,'user_id');
    }



}
