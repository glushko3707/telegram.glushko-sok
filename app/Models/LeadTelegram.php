<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeadTelegram extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id',
        'telegram_id',
        'link',
        'telegram_username',
    ];
}
