<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use HasFactory;

    protected $fillable =  [
    'project_id', 'bot_username', 'chat_id', 'chat_title',
    'created_at','updated_at',
        ];
}
