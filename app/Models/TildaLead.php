<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TildaLead extends Model
{
    use HasFactory;

    protected $fillable = [
       'phone', 'name', 'project_id',
       'utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term',
        'updated_at','created_at',
    ];

}
