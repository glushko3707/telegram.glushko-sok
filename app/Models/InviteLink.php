<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class InviteLink extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'project_id', 'link',
        'created_at','updated_at','channel_id',
    ];

    public function getParent(): HasOne
    {
        return $this->hasOne(User::class,'id','user_id');
    }





}
