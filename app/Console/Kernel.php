<?php

namespace App\Console;

use App\Jobs\Telegram\PipelineStepJob;
use App\Models\TelegramSendQuaua;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->call(function () {
            $telegramSendQuaua = TelegramSendQuaua::query()->where('timeSend','<',now());
            $telegramSendQuauaGet = $telegramSendQuaua->get();

            if (count($telegramSendQuauaGet) > 0) {
                foreach ($telegramSendQuauaGet as $date) {
                    PipelineStepJob::dispatch($date->project_id,$date->telegram_id,$date->pipeline_step)->onQueue('high');
                }
            }
            $telegramSendQuaua->delete();

        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
