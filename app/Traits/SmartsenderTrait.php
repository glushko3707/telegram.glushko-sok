<?php

namespace App\Traits;


use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait SmartsenderTrait
{
    public function processText($imageJpg,$fullname,$x,$y,$size){
        // Меняем размер и формат изображения
        $avatarSize = 320*$size/100;
        $imgText = Image::canvas($avatarSize, $avatarSize, array(255,255,255,0));
        $imgText->resize($avatarSize, $avatarSize);
        $imgText->encode('png');

        // Получаем ширину и высоту изображения
        $width = $imgText->getWidth();
        $height = $imgText->getHeight();

        $imgText->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#080808');
        });

        if (strlen($fullname) < 22){
            $sizeText = 40;
        } elseif (strlen($fullname)< 28){
            $sizeText = 30;
        } elseif (strlen($fullname) < 45){
            $sizeText = 20;
        }
        else {
            $sizeText = 14;
        }

        $imgText->text($fullname, $width/2, $height/2, function($font) use($sizeText) {
            $font->file(public_path("dist/font/Golos-UI_Bold.ttf"));
            $font->size($sizeText);
            $font->color('#fdf6e3');
            $font->align('center');
            $font->valign('center');
        });

        $widthBanner = $imageJpg->getWidth();
        $heightBanner = $imageJpg->getHeight();
        $xPosition = $widthBanner*$x/100-$widthBanner;
        $yPosition = $heightBanner*$y/100-$heightBanner;

        $imageJpg->insert($imgText, 'center',$xPosition,$yPosition);

        return $imageJpg;
    }
    public function processTextNew($imageJpg,$fullname,$x,$y,$size){
        // Меняем размер и формат изображения
        $avatarSize = 320*$size/100;

        $imgText = Image::canvas($avatarSize, $avatarSize, array(255,255,255,0));
        $imgText->resize($avatarSize, $avatarSize);
        $imgText->encode('png');

        // Получаем ширину и высоту изображения
        $width = $imgText->getWidth();
        $height = $imgText->getHeight();

        $imgText->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#080808');
        });

        if (strlen($fullname) < 22){
            $sizeText = 40;
        } elseif (strlen($fullname)< 28){
            $sizeText = 30;
        } elseif (strlen($fullname) < 45){
            $sizeText = 20;
        }
        else {
            $sizeText = 14;
        }

        $imgText->text($fullname, $width/2, $height/2, function($font) use($sizeText) {
            $font->file(public_path("dist/font/Golos-UI_Bold.ttf"));
            $font->size($sizeText);
            $font->color('#fdf6e3');
            $font->align('center');
            $font->valign('center');
        });

        $widthBanner = $imageJpg->getWidth();
        $heightBanner = $imageJpg->getHeight();
        $xPosition = round(($x/100*$widthBanner/2)-$avatarSize/2);
        $yPosition = round(($y/100*$heightBanner/2)-$avatarSize/2);

        $imageJpg->insert($imgText,'top-left',$xPosition,$yPosition);

        return $imageJpg;
    }

    private function processAvatarNew($avatar, $imageJpg, $x, $y, $size)
    {
        // Создаем изображение из URL аватара и сразу применяем необходимые преобразования
        $avatarSize = 160 * $size / 100;
        $image = Image::make(file_get_contents($avatar))
            ->resize($avatarSize, $avatarSize) // Изменение размера
            ->mask(Image::canvas($avatarSize, $avatarSize)->circle($avatarSize, $avatarSize / 2, $avatarSize / 2, function ($draw) {
                $draw->background('#fff');
            }), false) // Применение маски
            ->resize($avatarSize * 2, $avatarSize * 2) // Увеличение размера для более высокого качества
            ->encode('png'); // Кодирование в PNG

        // Вычисление позиции для вставки аватара на баннер
        $widthBanner = $imageJpg->getWidth();
        $heightBanner = $imageJpg->getHeight();
        $xPosition = round(($x / 100 * $widthBanner / 2) - $avatarSize);
        $yPosition = round(($y / 100 * $heightBanner / 2) - $avatarSize);

        // Вставка аватара на изображение баннера
        $imageJpg->insert($image, 'top-left', $xPosition, $yPosition);

        return $imageJpg;
    }
    public function addText($images,$text,$x_text,$y_text,$font_size,$text_align){

        if (strlen($text) < 22){
            $sizeText = 48*$font_size/100;
        } elseif (strlen($text)< 28){
            $sizeText = 36*$font_size/100;
        } elseif (strlen($text) < 45){
            $sizeText = 24*$font_size/100;
        }
        else {
            $sizeText = 165*$font_size/100;
        }

        $widthBanner = $images->getWidth();
        $heightBanner = $images->getHeight();

        $xPosition = 1 + $widthBanner/100*$x_text/2;
        $yPosition = 1 + $heightBanner/100*$y_text/2;

        $images->text($text, $xPosition, $yPosition, function($font) use($sizeText,$text_align) {
            $font->file(public_path("dist/font/Golos-UI_Bold.ttf"));
            $font->size($sizeText);
            $font->color('#ffffff');
            $font->align($text_align);
            $font->valign('top');
        });

        return $images;
    }
    public function copyLinks($name_file){
        if ($this->avatar && $this->text == false){
            $this->copyLink = '{"picture":"'.$name_file.'","ssid":"{{ userId }}","fullName":"{{ fullName }}","photo":"{{ photo }}","x":'."$this->x".',"y":'."$this->y".',"size":'."$this->size".'}';
            $this->copyLinksSaleBot = '{"picture":"'.$name_file.'","platform_id":"#{platform_id}","fullName":"#{full_name}","photo":"#{avatar}","x":'."$this->x".',"y":'."$this->y".',"size":'."$this->size".'}';

        }
        if ($this->avatar && $this->text == true){
            $this->copyLink = '{"picture":"'.$name_file.'","ssid":"{{ userId }}","fullName":"{{ fullName }}","photo":"{{ photo }}","x":'."$this->x".',"y":'."$this->y".',"size":'."$this->size".
                ',' . '"text_x":"' . "$this->text_x".'"' .
                ',' . '"text_y":"' . "$this->text_y".'"' .
                ',' . '"text_align":"' . "$this->text_align".'"' .
                ',' . '"font_size":"' . "$this->font_size".'"' .
                ',' .  '"text":"{{ fullName }}"' . '}';

            $this->copyLinksSaleBot = '{"picture":"'.$name_file.'","platform_id":"#{platform_id}","fullName":"#{full_name}","photo":"#{avatar}","x":'."$this->x".',"y":'."$this->y".',"size":'."$this->size".
                ',' . '"text_x":"' . "$this->text_x".'"' .
                ',' . '"text_y":"' . "$this->text_y".'"' .
                ',' . '"text_align":"' . "$this->text_align".'"' .
                ',' . '"font_size":"' . "$this->font_size".'"' .
                ',' .  '"text":"#{full_name}"' . '}';
        }

    }


}

