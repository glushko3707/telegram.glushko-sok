<?php

namespace App\Traits;


use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Telegram\InviteLinkController;
use App\Http\Controllers\Telegram\MessageController;
use App\Http\Controllers\Telegram\Pipeline\PipelineController;
use App\Jobs\Telegram\SendMessageJob;
use App\Models\Channel;
use App\Models\Project;
use App\Models\Telegram\Pipeline\Button;
use App\Models\Telegram\Pipeline\Pipeline;
use App\Models\Telegram\StoryTelegramMessage;
use App\Models\Telegram\UserBot;
use App\Models\TelegramSendQuaua;
use App\Models\User;
use Klev\TelegramBotApi\Methods\SendMessage;
use Klev\TelegramBotApi\Methods\SendPhoto;
use Klev\TelegramBotApi\Methods\SendVideo;
use Klev\TelegramBotApi\Methods\SendVideoNote;
use Klev\TelegramBotApi\Telegram;
use Klev\TelegramBotApi\TelegramException;
use Klev\TelegramBotApi\Types\InlineKeyboardButton;
use Klev\TelegramBotApi\Types\InlineKeyboardMarkup;
use Klev\TelegramBotApi\Types\KeyboardButton;
use Klev\TelegramBotApi\Types\ReplyKeyboardMarkup;
use Throwable;

trait TelegramBotTrait
{

    public function acceptInvite($id,$telegram_id){
        $project_telegram = Project::query()->find($id);
        $setting = $project_telegram->getProjectSetting;
        $api_key = $setting->api_key;

        $bot = new Telegram($api_key);
        $channel = Channel::query()->where('project_id',$id)->get()->first();
        $bot->approveChatJoinRequest($channel->chat_id,$telegram_id);
    }

    public function sendMessage($id,MessageController $message,$telegram_id){

        $project_telegram = Project::query()->find($id);
        $setting = $project_telegram->getProjectSetting;
        $api_key = $setting->api_key;
        try {
            $bot = new Telegram($api_key);

            $this->setVariables($message,$telegram_id,$project_telegram);

            if ((!empty($message->mediaType)) && $message->mediaType == 'photo') {
                if (!empty($message->media_id)) {
                    $photoUrl = $message->media_id;
                } else {
                    $photoUrl = 'http://telegram.glushko-sok.ru/storage/image/' .$id.'/'.$message->mediaUrl;
                }

                $msg = new SendPhoto($telegram_id,$photoUrl);
                $msg->caption = $message->text;
                if (isset($message->buttons->first()->type)){
                    $msg->reply_markup = $this->getReplyMarkup($message);
                }
                $message = $bot->sendPhoto($msg);

            } elseif ((!empty($message->mediaType)) && $message->mediaType == 'video') {
                $mediaUrl = $message->mediaUrl;
                $msg =  new SendVideo($telegram_id,$mediaUrl);
                $msg->video = $mediaUrl;
                $msg->caption = $message->text;
                if (isset($message->buttons->first()->type)){
                    $msg->reply_markup = $this->getReplyMarkup($message);
                }
                $message = $bot->sendVideo($msg);

            }
            elseif ((!empty($message->mediaType)) && $message->mediaType == 'videoNote') {
                $mediaUrl = $message->mediaUrl;
                $msg =  new SendVideoNote($telegram_id,$mediaUrl);
                $msg->video_note = $mediaUrl;
                if (isset($message->buttons->first()->type)){
                    $msg->reply_markup = $this->getReplyMarkup($message);
                }
                $message = $bot->sendVideoNote($msg);

            }

            else {
                $msg = new SendMessage($telegram_id, $message->text);
                $msg->parse_mode = 'html';
                if ($message->buttons != null && isset($message->buttons->first()->type)){
                     $msg->reply_markup = $this->getReplyMarkup($message);
                }
                $message = $bot->sendMessage($msg);
            }

            StoryTelegramMessage::query()->create([
                'project_id' => $id,
                'telegram_id' => $telegram_id,
                'message_id' => $message->message_id,
                'type' => 'outside',
                'text' => json_encode($message->text) ?? null,
            ]);

        } catch (Throwable $e) {
            report($e);
        }

    }

    public function setNowStep($step){
        $messageModel = $step->getMessage;
        $this->messageModel = $messageModel;
        $this->buttons =  $messageModel->getButtons;
        $this->mediaUrl = $messageModel->mediaUrl;
        $this->mediaType =  $messageModel->media_type;
        $this->messageId = $messageModel->id;
        $this->text = $messageModel->text;
    }

    public function setMessageModel($messageModel){
        $this->messageModel = $messageModel;
        $this->buttons =  $messageModel->getButtons;
        $this->mediaUrl = $messageModel->mediaUrl;
        $this->mediaType =  $messageModel->media_type;
        $this->messageId = $messageModel->id;
        $this->text = $messageModel->text;
    }




    public function getReplyMarkup($message){

        if ($message->buttons->first()->type == 'inline_keyboard') {
            $reply_markup = new InlineKeyboardMarkup();

            foreach ($message->buttons as $key => $button){
                if ($button->type == 'inline_keyboard'){
                    $inlineButton[$key] = new InlineKeyboardButton();
                    $inlineButton[$key]->text = $button->button_text;
                    if (!empty($button->url)){
                        $inlineButton[$key]->url = $button->url;
                    } else if ($button->callback_action) {
                        $inlineButton[$key]->callback_data = $button->callback_action;
                    }
                }
                $reply_markup->inline_keyboard[] = [$inlineButton[$key]];
            }
        }

        if ($message->buttons->first()->type == 'KeyboardButton') {
            $reply_markup = new ReplyKeyboardMarkup();

            foreach ($message->buttons as $key => $button){
                if ($button->type == 'KeyboardButton'){

                    if ($button->contact == true){
                        $request_contact = true;
                    } else {
                        $request_contact = false;
                    }
                    $keyboardButton[$key] = new KeyboardButton($button->button_text,$request_contact);

                }
                $reply_markup->keyboard[] = [$keyboardButton[$key]];
            }

            $reply_markup->resize_keyboard = true;

            if ($message->buttons->first()->contact == true){
                $reply_markup->request_contact = true;
            }
            if ($message->buttons->first()->oneTimeKeyboard == true){
                $reply_markup->one_time_keyboard = true;
            }
        }
        if (!isset($reply_markup)){
            $reply_markup = null;
        }
        return $reply_markup;
    }

    function setVariables($message,$telegram_id,$projectTelegram = null){
        if (str_contains($message->text, '{$link}')){
            $user = User::query()->where('telegram_id',$telegram_id)->get()->first();

            if (empty($user)){
                $user = (new RegisteredUserController())->createUserTelegramVarible($projectTelegram->id,$telegram_id);
                try {
                    $link = (new InviteLinkController())->createLinktelegram($projectTelegram->id,$user);
                } catch (Throwable $e) {
                    report($e);
                }
            } else {
                $getLink = $user->getLink->firstWhere('project_id', $projectTelegram->id);
                if (!isset($getLink->link)){
                    $link = (new InviteLinkController())->createLinktelegram($projectTelegram->id,$user);
                } else {
                    $link = $getLink->link;
                }
            }

            $message->text = str_replace('{$link}', $link, $message->text);
        }

    }

    public function makePipeline($id,$pipeline_step,$telegram_id){

            try {
                $piplelineController = new PipelineController();
                $piplelineController->setStep($pipeline_step);
                if (count($piplelineController->pipeline) == 1){
                    $lastStep = true;
                }
                foreach ($piplelineController->pipeline as $step){
                    if ($step->content_type == 'post'){
                        $message = new MessageController();
                        $message->setNowStep($step);
                        SendMessageJob::dispatch($id,$message,$telegram_id)->onQueue('top');
//                        $this->sendMessage($id,$message,$telegram_id);
                    }

                    if ($step->content_type == 'stop_pipeline'){
                        TelegramSendQuaua::query()->where('project_id',$id)->where('telegram_id',$telegram_id)->where('pipeline_id',$step->content_id)->delete();
                    }

                    if ($step->content_type == 'acceptInvite'){
                            try {
                                $this->acceptInvite($id,$telegram_id);
                            }  catch (Throwable $e) {
                            report($e);
                        }
                    }

                    if ($step->content_type == 'delay'){
                        $piplelineController->setNextStep($piplelineController);
                        $piplelineController->setDeleyStep($piplelineController);
                        $piplelineController->setTelegramSendQuaua($id,$telegram_id,$piplelineController);
                        $delay = true;
                        break;
                    }



                    sleep(2);
                }

                if (!isset($delay)){
                    TelegramSendQuaua::query()->where('project_id', '=', $id,)->where('telegram_id', '=', $telegram_id)->where('pipeline_id', '=', $piplelineController->nowStep->pipeline_id)->delete();
                }


            } catch (Throwable $e) {
                report($e);
            }

        }

    public function getDefaultStepId($id){
            $project = Project::query()->find($id);
            $projectSetting = $project->getProjectSetting;
            $pipeline_step = Pipeline::query()->where('project_id',$id)->where('pipeline_id',$projectSetting->pipeline_default)->get()->first();
            if (!empty($pipeline_step)){
                return $pipeline_step->id;
            } else {
                $pipeline = Pipeline::query()->where('project_id',$id)->get()->first();
                if (!empty($pipeline)){
                    return $pipeline->id;
                } else{
                    return null;
                }
            }
        }

    public function getRequesStepId($id){
        $project = Project::query()->find($id);
        $projectSetting = $project->getProjectSetting;
        $pipeline_step = Pipeline::query()->where('project_id',$id)->where('pipeline_id','=',0)->get()->first();
        if (!empty($pipeline_step)){
            return $pipeline_step->id;
        } else {
            return null;
        }
    }

    public function getCallbackButton($text,$id){
        return Button::query()->where('project_id',$id)->where('button_text',$text)->get()->last();
    }

    public function setMessageObject($messageModel,$messageController){
        $messageController->buttons =  $messageModel->getButtons ?? '';
        $messageController->mediaUrl = $messageModel->mediaUrl ?? '';
        $messageController->mediaType =  $messageModel->media_type ?? '';
        $messageController->messageId = $messageModel->id ?? '';
        $messageController->text = $messageModel->text ?? '';
    }

    public function startUserBotMessage($request,$id){


            $firstName = $request['message']['from']['first_name'] ?? '';
            $lastName = $request['message']['from']['last_name'] ?? '';
            $fullname = $firstName.' '.$lastName;


        $channel = Channel::query()->where('project_id',$id)->get()->first();
        $bot_username = $channel->bot_username ?? '';

        $telegram_username = $request['message']['from']['username'] ?? '';


        UserBot::query()->updateOrCreate([
            'project_id' => $id,
            'telegram_id' => $request['message']['from']['id'],
        ],[
            'telegram_username' => $telegram_username,
            'bot_username' => $bot_username,
            'fullName' => $fullname,
            'start_bot' => 1,
            'active' => 1,
        ]);
    }

}

