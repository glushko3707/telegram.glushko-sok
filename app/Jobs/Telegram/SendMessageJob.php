<?php

namespace App\Jobs\Telegram;

use App\Http\Controllers\Telegram\MessageController;
use App\Http\Controllers\Telegram\TgSendMessegeController;
use App\Traits\TelegramBotTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use TelegramBotTrait;

    public $id;
    public $message;
    public $telegram_id;


    public function __construct($id,MessageController $message,$telegram_id)
    {
        $this->id = $id;
        $this->message = $message;
        $this->telegram_id = $telegram_id;
    }


    public function handle(): void
    {
        (new TgSendMessegeController())->sendMessage($this->id,$this->message,$this->telegram_id);
    }
}
