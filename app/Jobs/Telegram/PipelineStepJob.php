<?php

namespace App\Jobs\Telegram;

use App\Traits\TelegramBotTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PipelineStepJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use TelegramBotTrait;


    public $id;
    public $pipeline_step;
    public $telegram_id;


    public function __construct($project_id,$telegram_id,$pipeline_step)
    {
        $this->id = $project_id;
        $this->pipeline_step = $pipeline_step;
        $this->telegram_id = $telegram_id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->makePipeline($this->id,$this->pipeline_step,$this->telegram_id);
        usleep( 1000); // 1/10 секунды
    }
}
