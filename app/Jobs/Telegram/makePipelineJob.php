<?php

namespace App\Jobs\Telegram;

use App\Traits\TelegramBotTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class makePipelineJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use TelegramBotTrait;


    public $id;
    public $pipeline_step;
    public $telegram_id;



    /**
     * Create a new job instance.
     */
    public function __construct($id,$pipeline_step,$telegram_id)
    {
        $this->id = $id;
        $this->pipeline_step = $pipeline_step;
        $this->telegram_id = $telegram_id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->makePipeline($this->id,$this->pipeline_step,$this->telegram_id);

    }
}
