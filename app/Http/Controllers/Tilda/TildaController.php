<?php

namespace App\Http\Controllers\Tilda;

use App\Http\Controllers\Controller;
use App\Models\TildaLead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TildaController extends Controller
{
    public function newLead(Request $request,$id)
    {
        $data['utm_source'] = $request->utm_source ?? '';
        $data['utm_medium'] = $request->utm_medium ?? '';
        $data['utm_campaign'] = $request->utm_campaign ?? '';
        $data['utm_content'] = $request->utm_content ?? '';
        $data['utm_term'] = $request->utm_term ?? '';
        $data['utm_term'] = $request->utm_term ?? '';
        $data['updated_at'] = now();
        $data['created_at'] = now();

        $data['phone'] = $request->Phone ?? '';
        $data['name'] = $request->Name ?? '';
        $data['project_id'] = $id;

        TildaLead::query()->insert($data);

        if ($id == 4){
            Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->get('https://morskoy-sport.ru/api/newAll/55',$data);
        }

    }
}
