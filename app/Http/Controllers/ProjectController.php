<?php

namespace App\Http\Controllers;

use App\Models\Permissoin;
use App\Models\Project;
use App\Models\Telegram\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{

    public function projectIndex()
    {
        $user = auth()->user();
//        $projects = Project::query()->get();
        $projects = $user->getPermissionsProject;
        return view('project.edit',compact('user','projects'));
    }

    public function projectUpdate(Request $request,$id){

        $validatedData = $request->validate([
            'name' => 'required|min:2',
            'botName' => 'required|min:2',
        ], [
            'name' => 'name Обязательно',
            'botName' => 'botName Обязательно',
        ]);

        $date['name'] = $request->name;
        $date['botName'] = $request->botName;

        $project = Project::query()->updateOrCreate([
            'id' => $id
        ],
            $date);

        return back();
    }

    public function createProjectStore(Request $request){

        $validatedData = $request->validate([
            'name' => 'required|min:2',
            'botName' => 'required|min:2',
        ], [
            'name' => 'name Обязательно',
            'botName' => 'botName Обязательно',
        ]);

        $date['name'] = $request->name;
        $date['botName'] = $request->botName;
        $date['created_at'] = now();
        $date['updated_at'] = now();

        $project = Project::query()->create($date);

        Permissoin::query()->create([
            'project_id' => $project->id,
            'user_id' => auth()->user()->id,
        ]);

        Setting::query()->create([
            'project_id' => $project->id,
        ]);

        $pathSave = "public/image/".$project->id . '/';

        if (!Storage::exists($pathSave)) {
            Storage::makeDirectory($pathSave);
            Storage::setVisibility($pathSave,'public');
        }


        return back();
    }

    public function destroyProjectStore($id){
        Project::query()->find($id)->delete();
    }

    public function getPermissionProject(){
        return auth()->user()->getPermissionsProject;
    }

}
