<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Telegram\InviteLinkController;
use App\Models\User;
use Illuminate\Http\Request;
use Throwable;

class VariableController extends Controller
{
    function setVariables($message,$telegram_id,$projectTelegram = null){
        if (str_contains($message->text, '{$link}')){
            $user = User::query()->where('telegram_id',$telegram_id)->get()->first();

            if (empty($user)){
                $user = (new RegisteredUserController())->createUserTelegramVarible($projectTelegram->id,$telegram_id);
                try {
                    $link = (new InviteLinkController())->createLinktelegram($projectTelegram->id,$user);
                } catch (Throwable $e) {
                    report($e);
                }
            } else {
                $getLink = $user->getLink->firstWhere('project_id', $projectTelegram->id);
                if (!isset($getLink->link)){
                    $link = (new InviteLinkController())->createLinktelegram($projectTelegram->id,$user);
                } else {
                    $link = $getLink->link;
                }
            }

            $message->text = str_replace('{$link}', $link, $message->text);
        }

    }

}
