<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PossevService\CallbackController;
use App\Http\Controllers\PossevService\PossevPostController;
use App\Http\Controllers\PossevService\PossevServiceController;
use App\Http\Controllers\Telegram\Group\GroupUserController;
use App\Http\Controllers\Telegram\Pipeline\DefaultStartController;
use App\Http\Controllers\Telegram\Pipeline\PipelineController;
use App\Jobs\Telegram\makePipelineJob;
use App\Jobs\Telegram\SendMessageJob;
use App\Models\ConnectTelegram;
use App\Models\PosevPost;
use App\Models\Project;
use App\Models\Telegram\Pipeline\Button;
use App\Models\Telegram\Pipeline\Message;
use App\Models\Telegram\StoryTelegramMessage;
use App\Models\Telegram\UserBot;
use App\Models\Telegram\UserChannel;
use App\Models\TelegramSendQuaua;
use App\Models\User;
use App\Services\Telegram\Pipeline\Service;
use App\Traits\TelegramBotTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Klev\TelegramBotApi\Events\CallbackQueryEvent;
use Klev\TelegramBotApi\Events\ChannelPostEvent;
use Klev\TelegramBotApi\Events\ChatMemberEvent;
use Klev\TelegramBotApi\Events\EditedMessageEvent;
use Klev\TelegramBotApi\Events\InlineQueryEvent;
use Klev\TelegramBotApi\Events\MessageEvent;
use Klev\TelegramBotApi\Events\MyChatMemberEvent;
use Klev\TelegramBotApi\Events\PollEvent;
use Klev\TelegramBotApi\Methods\AnswerCallbackQuery;
use Klev\TelegramBotApi\Telegram;
use mysql_xdevapi\Collection;
use Throwable;

class TelegramController extends Controller
{
    use TelegramBotTrait;

    public function getWebhook (Request $request,$id)
    {
        try {

        $typeWebhook = $this->getTypeWebhook($request->all(),$id);
        switch ($typeWebhook) {
            case 'botToChannel':
                $chat_id = $request['my_chat_member']['chat']['id'] ?? '';
                $chat_title = $request['my_chat_member']['chat']['title'] ?? '';
                $bot_username = $request['my_chat_member']['new_chat_member']['user']['username'] ?? '';

                (new ChannelController())->addBotToChannel($id,$chat_id,$chat_title,$bot_username);
                return 200;

            case 'newUserAdded':
                $GroupUserController = new GroupUserController();
                $first_name = $request['chat_member']['new_chat_member']['user']['first_name'] ?? '';
                $last_name = $request['chat_member']['new_chat_member']['user']['last_name'] ?? '';
                $data = [
                    'channel_id' => $request['chat_member']['chat']['id'],
                    'telegram_user_id' => $request['chat_member']['new_chat_member']['user']['id'],
                    'fullName' => $first_name . ' ' . $last_name,
                    'username' => $request['chat_member']['new_chat_member']['user']['username'] ?? '',
                    'bot' => $request['chat_member']['new_chat_member']['user']['is_bot'] ?? '',
                    'invite_link' => $request['chat_member']['invite_link']['invite_link'] ?? '',
                    'link_name' => $request['chat_member']['invite_link']['name'] ?? '',
                ];

                $GroupUserController->newChatMember($data,$id);
                return 200;

            case 'userLeftChat':
                $GroupUserController = new GroupUserController();
                $first_name = $request['chat_member']['new_chat_member']['user']['first_name'] ?? '';
                $last_name = $request['chat_member']['new_chat_member']['user']['last_name'] ?? '';
                $data = [
                    'channel_id' => $request['chat_member']['chat']['id'],
                    'telegram_user_id' => $request['chat_member']['new_chat_member']['user']['id'],
                    'fullName' => $first_name . ' ' . $last_name,
                    'username' => $request['chat_member']['new_chat_member']['user']['username'] ?? '',
                ];

                $GroupUserController->leftChatMember($data,$id);
                return 200;

//            case 'joinRequest':
//
//                      // добавляем в базу данных и отправляем сообщение родителю
//
//                          $ChannelController =   new ChannelController();
//                          $UserChannel = $ChannelController->joinRequestTelegram($id,$request->all(),$ChannelController);
//                          $userParent =  $ChannelController->getUserParentModel($UserChannel);
//                          if (!empty($userParent))  {
//                              $ChannelController->setInvitePeople($userParent,$id);
//                              $messageController = new MessageController;
//                              $messageController->setMessageParent($messageController,$userParent,$UserChannel);
//                              SendMessageJob::dispatch($id,$messageController,$userParent->telegram_id)->onQueue('top');
//                          }
//
//                        // пишем новому пользователю
//                        $pipeline_step = $this->getRequesStepId($id);
//                          if ($pipeline_step == null) {
//                              return 200;
//                          }
//                            $this->makePipeline($id,$pipeline_step,$request['chat_join_request']['from']['id']);
//                        return 200;

            case 'confirmationRegistration':
                        $code = substr($request['message']['text'], 7);
                        $telegram_id = $request['message']['from']['id'] ?? '';
                        $connectTelegram = ConnectTelegram::query()->where('code',$code)->get()->first();
                        if (!empty($connectTelegram) && !empty(User::query()->find($connectTelegram->user_id)) ) {
                            User::query()->find($connectTelegram->user_id)->update(['telegram_id' => $telegram_id ]);
                        }
                        return 200;

            case 'callback_query':

                        $project_telegram = Project::query()->find($id);

                        if (env('APP_ENV') != 'local'){
                            $setting = $project_telegram->getProjectSetting;
                            $api_key = $setting->api_key;
                            $bot = new Telegram($api_key);
                            $bot->answerCallbackQuery(new AnswerCallbackQuery($request['callback_query']['id'])) ;
                        }

                       $callback_query = $request['callback_query']['data'];
                       $callbackController = new CallbackController();

                       if ($callback_query == 'create_post'){
                           $callbackController->createPostCallback($id,$request['callback_query']['from']['id'],$request['callback_query']['message']['message_id']);
                       }

                        if ($callback_query == 'Cancel'){
                            $nextStepKey = "telegram_await_project_{$id}_chat_id_" . $request['callback_query']['from']['id'];
                            Cache::forget($nextStepKey);
                            $this->startOrSetting($id, $request['callback_query']['from']['id']);
                        }



                $piplelineController = new PipelineController();
                        if ($callback_query == 'nextStep'){
                            $nextStep = null;
                        } else {
                            $piplelineController->setStepCallbackQuery($callback_query,$id,$piplelineController);
                        }
                            if (!empty($piplelineController->nowStep)){
                                $this->makePipeline($id,$piplelineController->nowStep->id,$request['callback_query']['from']['id']);
                            }   else {
                                $messageModel = $piplelineController->getMessageModelByCallbackQuery($id,$callback_query);
                                $messageController = new MessageController;
                                $this->setMessageObject($messageModel,$messageController);
                                if (!empty($messageController->messageId)){
                                    SendMessageJob::dispatch($id,$messageController,$request['callback_query']['from']['id'])->onQueue('top');
                                }
                            }

                        break;
            case 'start':
                $this->startOrSetting($id,$request['message']['from']['id']);

//                            $pipeline_step = $this->getDefaultStepId($id);
//
//                            $user = \App\Models\User::query()->where('telegram_id',$request['message']['from']['id'])->get()->first();
//                            if (empty($user)){
//                                $user = (new RegisteredUserController())->createUserTelegramStart($request);
//                                    $link = (new InviteLinkController())->createLinktelegram($id,$user);
//                            }
//
//                            $inviteLink = \App\Models\InviteLink::query()->where('project_id',$id)->where('user_id',$user->id)->exists();
//                            if (!$inviteLink){
//                                   $link = (new InviteLinkController())->createLinktelegram($id,$user);
//                           }
//                                makePipelineJob::dispatch($id,$pipeline_step,$request['message']['from']['id']);
////                                $this->makePipeline($id,$pipeline_step,$request['message']['from']['id']);

                            break;
//                case 'start':
//                    $this->startUserBotMessage($request,$id);
//                    $pipeline_step = $this->getDefaultStepId($id);
//
//                    $user = \App\Models\User::query()->where('telegram_id',$request['message']['from']['id'])->get()->first();
//                    if (empty($user)){
//                        $user = (new RegisteredUserController())->createUserTelegramStart($request);
//                        $link = (new InviteLinkController())->createLinktelegram($id,$user);
//                    }
//
//                    $inviteLink = \App\Models\InviteLink::query()->where('project_id',$id)->where('user_id',$user->id)->exists();
//                    if (!$inviteLink){
//                        $link = (new InviteLinkController())->createLinktelegram($id,$user);
//                    }
//                    makePipelineJob::dispatch($id,$pipeline_step,$request['message']['from']['id']);
////                                $this->makePipeline($id,$pipeline_step,$request['message']['from']['id']);
//
//                    break;

                case 'newMessageBot':

                    $text = $message['message']['text'] ?? $message['message']['caption'] ?? '';
                    StoryTelegramMessage::query()->create([
                        'project_id' => $id,
                        'telegram_id' => $request['message']['from']['id'],
                        'message_id' => $request['message']['message_id'],
                        'type' => 'inside',
                        'text' => json_encode($text),
                    ]);

                    // Получение platform_id
                    $platform_id = $request['message']['from']['id'];

// Ключ для кеша, связанный с ожиданием поста
                    $nextStepKey = "telegram_await_project_{$id}_chat_id_$platform_id";
                    $nextStep = Cache::get($nextStepKey);
// Проверка наличия значения в кеше
                    if ($nextStep == 'await_post') {
                        // Удаление значения из кеша

                        // Сохранение нового поста
                        $possevPostController = new PossevPostController();
                        $post_id = $possevPostController->saveNewPost($request['message'], $id);

                        // Отправка сообщения пользователю
                        $message = new MessageController();
                        $message->text = 'Отправьте название поста, если хотите';

                        // Создание кнопок
                        $buttons = collect([
                            (object)[
                                'callback_action' => 'Cancel',
                                'button_text' => 'Отмена',
                                'type' => 'inline_keyboard',
                            ]
                        ]);

                        // Присваиваем кнопки сообщению
                        $message->buttons = $buttons;

                        // Отправляем сообщение с задачей в очередь
                        SendMessageJob::dispatch($id, $message, $platform_id)->onQueue('top');

                        // Сохраняем пост ID в кеш с новым ключом
                        $post_id_cache = "telegram_awaitPostName_project_$id" . "_chat_id_$platform_id";

                        Cache::put($nextStepKey, 'await_postName', now()->addHours(24));
                        Cache::put($post_id_cache, $post_id, now()->addHours(24));

                        return response()->json(['status' => 200], 200);
                    }

// Проверка наличия значения в кеше
                    if ($nextStep == 'await_postName') {
                        // Получаем post_id из кеша
                        $post_id_cache = "telegram_awaitPostName_project_$id" . "_chat_id_$platform_id";
                        $post_id = Cache::get($post_id_cache);

                        // Обновление имени поста
                        $PosevPost = PosevPost::query()->find($post_id);
                        $PosevPost->name = $request['message']['text'] ?? null;
                        $PosevPost->save();

                        // Отправка сообщения о сохранении поста
                        $message = new MessageController();
                        $message->text = 'Пост сохранён';
                        SendMessageJob::dispatch($id, $message, $platform_id)->onQueue('top');

                        // Выполнение других действий (например, возврат к старту или настройкам)
                        $this->startOrSetting($id, $platform_id);

                        // Удаление значения из кеша
                        Cache::forget($nextStepKey);
                        Cache::forget($post_id_cache);

                        return response()->json(['status' => 200], 200);
                    }


//                            $this->startUserBotMessage($request,$id);
//                            $button = $this->getCallbackButton($request['message']['text'],$id);
//                            if ($button != null){
//                                $pipeline = $button->getPipeline;
//                                if ($pipeline != null){
////                                    $this->makePipeline($id,$pipeline->id,$request['message']['from']['id']);
//                                    makePipelineJob::dispatch($id,$pipeline->id,$request['message']['from']['id']);
//
//                                } else {
//                                    $messageModel = $button->getMessage;
//                                    $message = new MessageController();
//                                    $message->setMessageModel($messageModel);
//                                    SendMessageJob::dispatch($id,$message,$request['message']['from']['id'])->onQueue('top');
//                                }
//                            }
                            break;
        }
  } catch (Throwable $e) {
        report($e);
    }
        return 200;
    }

    function getTypeWebhook($data,$id){

        $project_telegram = Project::query()->find($id);
        $setting = $project_telegram->getProjectSetting;
        $api_key = $setting->api_key;
        $bot = new Telegram($api_key);
        $updates = $bot->getWebhookUpdates();

        if (isset($updates->my_chat_member->new_chat_member->status) && $updates->my_chat_member->new_chat_member->status == 'administrator' && $updates->my_chat_member->new_chat_member->user->is_bot){
            return 'botToChannel';
        }

         // Добавлено отслеживание вступления новых заявок в канал
         if (isset($data['chat_member']['new_chat_member']) && $data['chat_member']['new_chat_member']['status'] == 'member') {
            return 'newUserAdded';
        }

         // Добавлено отслеживание выхода из чата
         if (isset($data['chat_member']['new_chat_member']) && $data['chat_member']['new_chat_member']['status'] == 'left') {
            return 'userLeftChat';
        }
        
        if (isset($data['callback_query']))  {
            return 'callback_query';
        }

        if (isset($data['message']['entities'][0]['type']) && $data['message']['text'] == '/start')  {
            return 'start';
        }

        if (isset($data['message']['entities'][0]['type']) && $data['message']['entities'][0]['type'] == 'bot_command' && str_starts_with($data['message']['text'], '/start')){
            return 'confirmationRegistration';
        }

        if (isset($data['message']['message_id']) && $data['message']['chat']['type'] == 'private'){
            return 'newMessageBot';
        }

        if (isset($data['chat_join_request']['user_chat_id']) && isset($data['chat_join_request']['invite_link'])){
            return 'joinRequest';
        }


        return null;
    }


    public function getTelegramChatIdWebhook(Request $request)
    {
        $telegram_id = $request['message']['from']['id']?? '';
        $text = $request['message']['forward_origin']['chat']['id'] ?? $request['message']['forward_origin']['sender_chat']['id'] ?? 'Вам нужно переслать любое сообщение с чата или канала';
        $messageController = new MessageController();
        $bot_api_key = '6498310871:AAGaYxReYt3VbigwmXXYd9vmZUKwyLrtR_8';

        $messageController->sendTextTelegram($text,$telegram_id,$bot_api_key);
    }

    public function startOrSetting($id,$platform_id)
    {
        $message = new MessageController();
        $message->text = (new PossevServiceController())->setWelcomeStartMessege();
        $buttons = new \Illuminate\Support\Collection(
            new \Illuminate\Support\Collection([
                (object)[
                    'callback_action' => 'create_post',
                    'button_text' => 'Создать пост',
                    'type' => 'inline_keyboard',
                ]
            ]),
        );

        // Добавление элемента в коллекцию
        $message->buttons = $buttons;

        SendMessageJob::dispatch($id,$message,$platform_id)->onQueue('top');
//                            $this->startUserBotMessage($request,$id);
    }
}
