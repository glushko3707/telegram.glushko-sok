<?php

namespace App\Http\Controllers\Telegram\Pipeline;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DefaultStartController extends Controller
{
    public function defaultPipelineStart(Request $request, $id){
        return 200;
    }
}
