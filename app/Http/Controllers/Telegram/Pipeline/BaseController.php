<?php

namespace App\Http\Controllers\Telegram\Pipeline;

use App\Http\Controllers\Controller;
use App\Services\Telegram\Pipeline\Service;

class BaseController extends Controller
{
    public $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }
}
