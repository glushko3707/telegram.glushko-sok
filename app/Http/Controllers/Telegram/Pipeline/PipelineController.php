<?php

namespace App\Http\Controllers\Telegram\Pipeline;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\MessageController;
use App\Models\Project;
use App\Models\Telegram\Pipeline\Button;
use App\Models\Telegram\Pipeline\Delay;
use App\Models\Telegram\Pipeline\Message;
use App\Models\Telegram\Pipeline\Pipeline;
use App\Models\Telegram\Setting;
use App\Models\TelegramSendQuaua;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PipelineController extends Controller
{
    public $defaultStep;
    public $pipeline;
    public $nowStep;
    public $nextTime;



    // метод, просмотра воронки
    public function viewPipline($id){

        $pipleline = Pipeline::query()->where('project_id',$id)->orderBy('priority')->get();
        $pipleline = $pipleline->whereNotNull('priority')->toArray();
        $messege = Message::query()->where('project_id',$id)->get();
        $projectTelegram = Project::query()->where('id',$id)->get()->first();
        $delays = Delay::query()->where('project_id',$id)->get();

        foreach ($pipleline as $key => $Step) {
            if ($Step ['content_type'] == 'condition'){
                $pipleline[$key]['content_type'] = 'condition';
                continue;
            }

            if ($Step['content_type'] == 'notification') {
                $pipleline[$key]['content_type'] = 'Установка уведомлений о вебе';
                continue;
            }

            if ($Step['content_type'] == 'stop_pipeline') {
                $pipleline[$key]['content_type'] = 'Остановить воронку ' . $Step['content_id'];
                continue;
            }



            if ($Step['content_type'] == 'delay'){
                $delay = $delays->where('id',$Step['content_id'])->first();
                $pipleline[$key]['delay']['type'] = $delay->type;
                $pipleline[$key]['delay']['time'] = $delay->daley;
                continue;
            }

            if ($Step['content_type'] == 'post' && !isset($messege->find($Step['content_id'])->media_type) && $messege->find($Step['content_id'])->media_type != null){
                dd('не могу найти шаг ' . $Step['content_id'] );
            }



            if ($Step['content_type'] == 'post'){

                if ($messege->find($Step['content_id'])->media_type == 'videoNote') {
                    $pipleline[$key]['content_type'] = 'Видео заметка';
                    continue;
                }


                if (!empty($messege->find($Step['content_id'])->getButtons)){
                    foreach ($messege->find($Step['content_id'])->getButtons->toArray() as $button){
                        $pipleline[$key]['button'][] = [
                            'button_text' => $button['button_text'],
                            'type' => $button['type'],
                            'contact' => $button['contact'],
                            'callback_data' => $button['callback_action'],
                            'oneTimeKeyboard' => $button['oneTimeKeyboard'],
                        ];
                    }
                }
                $pipleline[$key]['messege_id'] = $messege->find($Step['content_id'])->id;
                $pipleline[$key]['content_type'] = $messege->find($Step['content_id'])->text;
                $pipleline[$key]['mediaUrl'] = $messege->find($Step['content_id'])->mediaUrl;

                if (isset($pipleline[$key]['mediaUrl'])){

                    $pipleline[$key]['media'] = env('APP_URL') . "/storage/image/".$id.'/' . $pipleline[$key]['mediaUrl'];
                } else {
                    $pipleline[$key]['media'] = null;
                }
                }
        }

        return view('telegram.pipeline.telegramPipeline',compact('id','pipleline'));
    }
    public function createPipeline($id){

        $pipeline = Pipeline::query()->where('project_id',$id)->get();

        foreach ($pipeline as $key => $value){
            if ($value->content_type == "post"){
                $message = $value->getMessage;
                $pipeline[$key]['text'] = $message->text;
                $pipeline[$key]['mediaUrl'] = env('APP_URL') . "/storage/image/".$id.'/' . $message->mediaUrl;
                $pipeline[$key]['pipeline_id'] = $value->pipeline_id;
                // добавляем кнопки
                $getButtons = $message->getButtons;
                $buttons = [];
                foreach ($getButtons as $keys => $getButton) {
                    $buttons[$getButton->type][$keys]['type'] = $getButton->type;
                    $buttons[$getButton->type][$keys]['button_text'] = $getButton->button_text;
                    $buttons[$getButton->type][$keys]['callback_action'] = $getButton->callback_action;
                }
                $pipeline[$key]['buttons'] = $buttons;
            }
        }

        $messages = Message::query()->where('project_id',$id)->get();

        foreach ($messages as $key => $message){
            $mess[$key]['text'] = $message->text;
            $mess[$key]['callback_data'] = $message->callback_data ?? '';
            $mess[$key]['id'] = $message->id;
            $mess[$key]['mediaUrl'] =  env('APP_URL') . "/storage/image/".$id.'/' . $message->mediaUrl;
            $mess[$key]['message_id'] = $message->id;
            // добавляем кнопки
            $getButtons = $message->getButtons;
            $buttons = [];
            foreach ($getButtons as $keys => $getButton) {
                $buttons[$getButton->type][$keys]['type'] = $getButton->type;
                $buttons[$getButton->type][$keys]['button_text'] = $getButton->button_text;
                $buttons[$getButton->type][$keys]['callback_action'] = $getButton->callback_action;
            }
            $mess[$key]['buttons'] = $buttons;
        }

        $mess = $mess ?? [];
        return view('telegram.pipeline.addPipelineStep',compact('id','pipeline','mess'));
    }

    public function viewPiplineUpdate(Request $request,$step_id){
        // здесь меню редактирования поста
        $pipelineStep = Pipeline::query()->find($step_id);
        $message = $pipelineStep->getMessage;
        $id = $pipelineStep->project_id;
        $pipeline = Pipeline::query()->where('project_id',$id);
        $getButtons = $message->getButtons->toArray();
        $message->button = $getButtons;
        $pipeline = $pipeline->get();

        foreach ($pipeline as $key => $value){
            if ($value->content_type == "post"){
                $messages = $value->getMessage;
                $pipeline[$key]['text'] = $messages->text;
                $pipeline[$key]['mediaUrl'] = env('APP_URL') . "/storage/image/".$id.'/' . $messages->mediaUrl;
                $pipeline[$key]['pipeline_id'] = $value->pipeline_id;
                // добавляем кнопки
                $getButtons = $messages->getButtons;
                $buttons = [];
                foreach ($getButtons as $keys => $getButton) {
                    $buttons[$getButton->type][$keys]['type'] = $getButton->type;
                    $buttons[$getButton->type][$keys]['button_text'] = $getButton->button_text;
                    $buttons[$getButton->type][$keys]['callback_action'] = $getButton->callback_action;
                }
                $pipeline[$key]['buttons'] = $buttons;
            }
        }
        return view('telegram.pipeline.updatePipelineStep',compact('id','pipeline','pipelineStep','message','getButtons'));
    }

    public function updatePipelineStep(Request $request,$step_id){

        $pipelineStep = Pipeline::query()->find($step_id);
        $message = $pipelineStep->getMessage;
        $id = $pipelineStep->project_id;


        if ($request->file('photoPost') !== null){

            if ($message->media_type == 'photo' && !empty($message->mediaUrl)){
                Storage::delete('public/image/' .$id . '/' . $message->mediaUrl);
            }

            $path = "public/image/".$id;

            $name = $request->file('photoPost')->getClientOriginalName();
            $name = str_replace(' ', '', $name);
            $maks = $request->file('photoPost')->storeAs($path,$name);

            $date['mediaUrl'] = $name;
            $date['media_type'] = 'photo';
        }

        if ($request->media_type == 'video' && $request->video != ''){
            $date['mediaUrl'] = $request->video;
            $date['media_type'] = 'video';
        }

        if ($request->media_type == 'videoNote' && $request->videoNote != ''){
            $date['mediaUrl'] = $request->videoNote;
            $date['media_type'] = 'videoNote';
        }


        if ($request->media_type == 'noMedia'){
            $date['mediaUrl'] = '';
            $date['media_type'] = '';
        }

        $date['text'] = $request->text_post;
        $message->update($date);

        DB::transaction(function() use($message,$pipelineStep,$id,$request) {
            $getButtons = $message->getButtons;
            foreach ($getButtons as $but){
                $but->delete();
            }

            if ($request->button_type != 'noButton'){

                $requests = collect($request->all());
                $filteredButton = $requests->filter(function ($value, $key)
                { return Str::startsWith($key, 'button_post'); });

                if ($requests['button_type'] == 'inline_keyboard'){

                    foreach ($filteredButton as $key => $value){
                        $date = [];
                        $idButton = Str::after($key, 'button_post_');

                        $date['button_text'] = $requests['button_post_' . $idButton];
                        $action = $requests['button_type_inline_' . $idButton];
                        if ($action == 'Action'){
                            $date['callback_action'] = $requests['callback_action_' . $idButton];
                        } else {
                            $date['url'] = $requests['url_' . $idButton];
                        }
                        $date['type'] = $requests['button_type'];

                        Button::query()->updateOrCreate([
                            'message_id' => $message->id,
                            'project_id' => $id,
                            'button_text' => $date['button_text']
                        ],$date);

                    }
                }


                if ($requests['button_type'] == 'KeyboardButton'){

                    foreach ($filteredButton as $key => $value){
                        $date = [];

                        if ($key == 'button_post'){
                            continue;
                        }

                        $idButton = Str::after($key, 'button_post_');

                        $date['button_text'] = $requests['button_post_' . $idButton];
                        $action = $requests['button_type_inline_' . $idButton];
                        if ($action == 'Action'){
                            $date['callback_action'] = $requests['callback_action_' . $idButton];
                        }

                        $date['type'] = $requests['button_type'];

                        if (isset($request->OneTimeKeyboard)){
                            $date['oneTimeKeyboard'] = 1;
                        } else {
                            $date['oneTimeKeyboard'] = 0;
                        }


                        Button::query()->updateOrCreate([
                            'message_id' => $message->id,
                            'project_id' => $id,
                            'button_text' => $date['button_text']
                        ],$date);

                    }
                }



            }
        });






        return back();
    }

    public function deletePipelineStep(Request $request,$id){

        $pipelineStep = Pipeline::query()->find($request->pipeline_edit_id);
        $message = $pipelineStep->getMessage;

        if ($message->media_type == 'photo' && !empty($message->mediaUrl)){
            $message = Message::query()->where('project_id',$id)->where('mediaUrl',$message->mediaUrl)->get();
            if (count($message) < 2){
                Storage::delete('public/image/' .$id . '/' . $message->mediaUrl);
            }
        }


        DB::transaction(function() use($message,$pipelineStep,$id) {
            Button::query()->where('project_id',$id)->where('message_id',$message->id)->delete();
            $message->delete();
            $pipelineStep->delete();
        });

        return 200;
    }


    public function createPipelineStore(Request $request,$id){



        DB::transaction(function() use($request,$id) {
            $projectTelegram = Project::query()->find($id);
            $projectName = $projectTelegram->projectName;
            $pipeline_id = $request->pipeline_id ?? '1';

            $callbackData = $request->callbackData ?? null;
            $avatar = $request->avatar ?? '0';

            if ($request->content_type == 'post'){
                if ($request->file('photoPost') !== null){
                    $path = "public/image/".$id;
                    $name = $request->file('photoPost')->getClientOriginalName();
                    $request->file('photoPost')->storeAs($path,$name);
                    $mediaType = 'photo';
                } else{
                    $name = null;
                    $mediaType = null;
                }
                $contentType = 'post';

                $telegramMessege = Message::query()->create([
                    'text' => $request->text_post,
                    'mediaUrl' => $name,
                    'project_id' => $id,
                    'media_type' => $mediaType,
                    'callback_data' => $callbackData,
                    'avatar' => $avatar,
                ]);


                if ($request->button_type != 'noButton'){

                    $requests = collect($request->all());
                    $filteredButton = $requests->filter(function ($value, $key)
                    { return Str::startsWith($key, 'button_post'); });




                    if ($requests['button_type'] == 'inline_keyboard'){

                        foreach ($filteredButton as $key => $value){
                            $date = [];
                            $idButton = Str::after($key, 'button_post_');

                            $date['button_text'] = $requests['button_post_' . $idButton];
                            $action = $requests['button_type_inline_' . $idButton];
                            if ($action == 'Action'){
                                $date['callback_action'] = $requests['callback_action_' . $idButton];
                            } else {
                                $date['url'] = $requests['url_' . $idButton];
                            }
                            $date['type'] = $requests['button_type'];

                            Button::query()->updateOrCreate([
                                'message_id' => $telegramMessege->id,
                                'project_id' => $id,
                                'button_text' => $date['button_text']
                            ],$date);

                        }
                    }


                    if ($requests['button_type'] == 'KeyboardButton'){

                        foreach ($filteredButton as $key => $value){
                            $date = [];
                            $idButton = Str::after($key, 'button_post_');

                            $date['button_text'] = $requests['button_post_' . $idButton];
                            $action = $requests['button_type_inline_' . $idButton];
                            if ($action == 'Action'){
                                $date['callback_action'] = $requests['callback_action_' . $idButton];
                            }

                            $date['type'] = $requests['button_type'];

                            Button::query()->updateOrCreate([
                                'message_id' => $telegramMessege->id,
                                'project_id' => $id,
                                'button_text' => $date['button_text']
                            ],$date);

                        }
                    }



                }


                $priorityPipenine = Pipeline::query()->orderBy('priority')->where('project_id',$id)->get()->last();

                if ($priorityPipenine != null){$priorityPipelines = $priorityPipenine->priority + 2;}else{$priorityPipelines = 1;}

                if (isset($request->onlyToday)){$onlyToday = '1';} else {$onlyToday = '0';}
                $timeSend = $request->timeSend ?? null;

                $pipeline = Pipeline::query()->create([
                    'project_id' => $id,
                    'content_id' => $telegramMessege->id,
                    'content_type' => $contentType,
                    'callback_data' => "$id" . "_$priorityPipelines",
                    'priority' => $priorityPipelines,
                    'pipeline_id' => $pipeline_id,
                ]);

                $pipeline->update(['callback_data' => "$id" . "_$pipeline->id",]);

            }

            if ($request->content_type == 'delay') {

                if ($request->delay_type == 'minute'){
                    $priorityPipenine = Pipeline::query()->orderBy('priority')->where('project_id',$id)->get()->last();
                    if ($priorityPipenine != null){$priorityPipelines = $priorityPipenine->priority + 2;}else{$priorityPipelines = 1;}

                    $delay = Delay::query()->create([
                        'project_id' => $id,
                        'daley' => $request->delay,
                        'onlyToday' => $request->onlyToday ?? '0',
                        'tomorrow' => $request->onlyTomorrow ?? '0',
                        'timeSend' => $request->timeSend ?? null,
                        'type' => $request->delay_type,
                    ]);

                    Pipeline::query()->create([
                        'project_id' => $id,
                        'content_id' => $delay->id,
                        'content_type' => $request->content_type,
                        'callback_data' => "$id" . "_$priorityPipelines",
                        'priority' => $priorityPipelines,
                        'pipeline_id' => $pipeline_id,
                    ]);
                }
            }

        });

        return redirect(route('telegramPipeline.index',$id));
    }

    public function getDefaultStepId($id){
        $project = Project::query()->find($id);
        $projectSetting = $project->getProjectSetting;

        $pipeline = Pipeline::query()->where('project_id',$id)->get();
        $this->pipeline = $pipeline;

        $pipeline_default_id = $projectSetting->pipeline_default;
        $pipeline_default = $pipeline->where('pipeline_id',$pipeline_default_id)->first();
        $this->nowStep = $pipeline_default;
    }

    public function setStep($step_id){
        $pipelineStep = Pipeline::query()->find($step_id);
        $project_id = $pipelineStep->project_id;
        $this->nowStep = $pipelineStep;
        $this->pipeline = Pipeline::query()->where('project_id',$project_id)->where('pipeline_id',$pipelineStep->pipeline_id)->where('priority','>=',$pipelineStep->priority)->get();
    }

    public function setNextStep($piplelineController)
    {
        $pipeline = $piplelineController->pipeline;
        $nowStep = $piplelineController->nowStep;
        $nextStep = $pipeline->where('priority','>',$nowStep->priority)->first();
        $this->nowStep = $nextStep;
    }

    public function setDeleyStep($piplelineController){
        $pipeline = $piplelineController->pipeline;
        $nowStep = $piplelineController->nowStep;
        $daleyModel = $nowStep->getDaley;
        if ($daleyModel->type == 'minute'){
            $this->nextTime = now()->addMinutes($daleyModel->daley);
        }
    }

    public function setTelegramSendQuaua($id, $telegram_id, $pipelineController)
    {
        $pipeline = $pipelineController->pipeline;
        $nowStep = $pipelineController->nowStep;
        $nextStep = $pipeline->where('priority','>',$nowStep->priority)->first();
        if (!empty($nextStep)) {
            TelegramSendQuaua::query()->updateOrCreate( [
                'project_id' => $id,
                'telegram_id' => $telegram_id,
                'pipeline_id' => $nowStep->pipeline_id,
            ],[
                'pipeline_step' => $nextStep->id,
                'timeSend' => $this->nextTime,
            ]);
        }




        return false;
    }

    public function setStepCallbackQuery($callback_query,$id,PipelineController $piplelineController){
        $pipeline = Pipeline::query()->where('project_id',$id)->get();
        $piplelineController->pipeline = $pipeline;
        $piplelineController->nowStep = $pipeline->where('callback_data',$callback_query)->first();
    }

    public function getMessageModelByCallbackQuery($id,$callback_query){
        return Message::query()->where('project_id',$id)->where('callback_data',$callback_query)->get()->first();
    }



}
