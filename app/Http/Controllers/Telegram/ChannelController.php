<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Models\Channel;
use App\Models\Telegram\UserBot;
use App\Models\Telegram\UserChannel;
use App\Models\User;


class ChannelController extends Controller
{

    public $chat_id;
    public $chat_title;

    public function addBotToChannel($id,$chat_id,$chat_title,$bot_username){

        Channel::query()->updateOrCreate([
            'project_id' => $id,
            'bot_username' => $bot_username,
        ],[
            'chat_id' => $chat_id,
            'chat_title' => $chat_title,
        ]);

        return 200;
    }


           public function joinRequestTelegram($id,$request,$ChannelController){
            $channel_id = $request['chat_join_request']['chat']['id'];
            $telegram_id = $request['chat_join_request']['from']['id'] ?? '';
            $telegram_username = $request['chat_join_request']['from']['username'] ?? '';
            $invite_link = $request['chat_join_request']['invite_link']['invite_link'] ?? '';
            $firstName = $request['chat_join_request']['from']['first_name'] ?? '';
            $lastName = $request['chat_join_request']['from']['last_name'] ?? '';
            $fullName = $firstName . ' ' . $lastName;
            $link_name = $request['chat_join_request']['invite_link']['name'] ?? '';
            $ChannelController->link = $invite_link;
            $ChannelController->channel_id = $channel_id;


            $channel = Channel::query()->where('project_id',$id)->get()->first();
            $bot_username = $channel->bot_username ?? '';

            UserBot::query()->updateOrCreate([
                'project_id' => $id,
                'telegram_id' => $telegram_id,
            ],[
                'bot_username' => $bot_username,
                'telegram_username' => $telegram_username,
                'fullName' => $fullName,
            ]);



               return UserChannel::query()->updateOrCreate([
               'project_id' => $id,
               'telegram_id' => $telegram_id,
        ],[
            'channel_id' => $channel_id,
            'link_name' => $link_name,
            'link' => $invite_link,
            'telegram_username' => $telegram_username,
            'fullName' => $fullName,
           ]);
    }

        public function getUserParentModel($UserChannel){
            $linkModel = $UserChannel->getLink;
            if (!empty($linkModel)){
                $UserParentModel = $linkModel->getParent;
            }
            if (!empty($UserParentModel)){
                return  $linkModel->getParent;
            }
             return null;
        }

        public function setInvitePeople(User $userParent,$id)
        {
            $myInvite = $userParent->getMyInvite;
            if (!empty($myInvite)) {
            foreach ($myInvite as $key => $user){
                if ($user['project_id'] == $id){
                    $users[$key]['telegram_id'] = $user->telegram_id;
                    $users[$key]['telegram_username'] = $user->telegram_username;
                    $users[$key]['fullName'] = $user->fullName;
                }
            }
                $userParent->myInvite = collect($users);
            }
        }
}
