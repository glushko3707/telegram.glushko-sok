<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Models\ConnectTelegram;
use App\Models\InviteLink;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Klev\TelegramBotApi\Methods\CreateChatInviteLink;
use Klev\TelegramBotApi\Telegram;

class InviteLinkController extends Controller
{
    public function telegramLinkIndex($id)
    {

        return view('TelegramInvite.InviteLink',compact('id'));
    }
    public function linkInviteIndex()
    {
        $id = 1;
        $invites = [];

        $project = Project::query()->get()->first();
        $user = auth()->user();
        $myInvite = $user->getMyInvite;
        $invites ['my'] = $myInvite;

        // получаем ID пользователей, которых пригласили
        $usersFirstLine = User::query()->whereIn('telegram_id',$myInvite->pluck('telegram_id'))->get();
        foreach ($usersFirstLine as $key => $userFirstLine){
            $invites['first'][$userFirstLine->name]['count'] = count($userFirstLine->getMyInvite);
            $invites['first'][$userFirstLine->name]['telegram_id'] = $userFirstLine->getMyInvite->pluck('telegram_id');
        }
        if (count($user->getLink) < 1) {
            if ($project->getChannel == null){
                return 'Добавьте бота в канал или обновите ему роль и обновите страницу';
            }
            $link = $this->createLinktelegram($id);
            $user->link = $link;
        }


        else {
             $user->link = $user->getLink->last()->link;
        }

        if (!isset($user->getCode->code)) {
                do {
                    $code = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 7);
                } while (ConnectTelegram::query()->where('code', $code)->exists ());

                ConnectTelegram::query()->insert([
                    'user_id' => $user->id,
                    'code' => substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 7),
                    'project_id' => $id,
                ]);
            $user->code = $code;
            } else {
            $user->code = $user->getCode->code;
        }



        return view('invite.edit',compact('project','user','invites'));
    }

    public function createLinktelegram($id, $user = null){
        $project_telegram = Project::query()->find($id);
        if ($user == null) {
            $userId = auth()->user()->id;
        } else {
            $userId =  $user->id;
        }

        $channel = $project_telegram->getChannel;
        $chat_id = $channel->chat_id;
        $setting = $project_telegram->getProjectSetting;
        $api_key = $setting->api_key;

        $bot = new Telegram($api_key);
        $CreateChatInviteLink = new CreateChatInviteLink($chat_id);
        $CreateChatInviteLink->creates_join_request = true;
        $CreateChatInviteLink->name = 'users_' .rand(1,10). "_" . $userId;
        $result = $bot->createChatInviteLink($CreateChatInviteLink);
        $link = $result->invite_link;

        InviteLink::query()->insert([
            'created_at' => now(),
            'updated_at' => now(),
            'user_id' => $userId,
            'project_id' => $id,
            'link' => $link,
            'channel_id' => $chat_id,
        ]);
        return $link;

    }


    public function createInviteLink(Project $project_telegram,$link_name,$creates_join_request = false){
        $project_setting = $project_telegram->getProjectSetting;
        if (!empty($project_setting->api_key)){
              $bot = new Telegram($project_setting->api_key);}
            else {return null;}
        $CreateChatInviteLink = new CreateChatInviteLink($project_setting->group_telegram);
        if ($creates_join_request != false){
            $CreateChatInviteLink->creates_join_request = true;
        }
        $CreateChatInviteLink->name = $link_name;
        $result = $bot->createChatInviteLink($CreateChatInviteLink);
        return $result->invite_link;
    }


}
