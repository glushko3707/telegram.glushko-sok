<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Jobs\Telegram\SendMessageJob;
use App\Models\Project;
use App\Models\Telegram\UserBot;
use Emoji\EmojiDetectTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;


class TextHtmlBotController extends Controller
{
    public function setWebhook(){

        $bot_api_key = '6498310871:AAGaYxReYt3VbigwmXXYd9vmZUKwyLrtR_8';
        $hook_url = 'https://api.telegram.org/bot'.$bot_api_key.'/setWebhook?url=https://telegram.glushko-sok.ru/api/telegram/TextHtml/getTelegramChatIdWebhook';
        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->post($hook_url);

    }

    public function getTelegramHook(Request $request){

        if (isset($request['message']['text']) || isset($request['message']['caption'])) {
            $telegram_id = $request['message']['from']['id'] ?? '';

            $messageController = new MessageController();
            $messageController->setHistoryMessage($request['message'],32);
            $first_name = $request['message']['from']['first_name'] ?? '';
            $last_name = $request['message']['from']['last_name'] ?? '';
            UserBot::query()->updateOrCreate([
                'project_id' => 32,
                'bot_username' => 'kupr_format_text_bot',
                'telegram_id' => $telegram_id,
            ],[
                'start_bot' => 1,
                'active' => 1,
                'telegram_username' => $request['message']['from']['username'],
                'fullName' => $first_name  . ' ' . $last_name,
            ]);


            $text = $request->message['text'] ?? $request->message['caption'] ?? '';


            if ($text == '/start'){
               $text = 'Привет! Можешь отправлять текст';
                $messageController = new MessageController();
                $messageController->sendTextTelegram ($text,$telegram_id);
            }

            $entities = $request['message']['entities'] ?? $request['message']['caption_entities'] ?? null;
            if ($entities){
                $text = $this->applyEntities($text, $entities);
            }

            $text = str_replace('&quot;', '"', $text);
            $bot_api_key = '7138835751:AAGJomkynu1XFxjSNuAz5dbIU3bRzQbHh4w';
            $messageController = new MessageController();
            $messageController->sendTextTelegram ($text,$telegram_id,$bot_api_key);
        }


        return 200;
    }

    function applyEntities($text, $entities) {
        $textUtf16 = mb_convert_encoding($text, 'UTF-16', 'UTF-8');
        $chars = unpack('n*', $textUtf16);
        $offsetAdjustment = -1;

        foreach ($entities as $entity) {
            $start = $entity['offset'] + 1 + $offsetAdjustment;
            $end = $start + $entity['length'];

            switch ($entity['type']) {
                case 'bold':
                    $startTag = '<b>';
                    $endTag = '</b>';
                    break;
                case 'italic':
                    $startTag = '<i>';
                    $endTag = '</i>';
                    break;
                case 'underline':
                    $startTag = '<u>';
                    $endTag = '</u>';
                    break;
                case 'strikethrough':
                    $startTag = '<s>';
                    $endTag = '</s>';
                    break;
                case 'code':
                    $startTag = '<code>';
                    $endTag = '</code>';
                    break;
                case 'pre':
                    $startTag = '<pre>';
                    $endTag = '</pre>';
                    break;
                case 'blockquote':
                    $startTag = '<blockquote>';
                    $endTag = '</blockquote>';
                    break;
                case 'text_link':
                    $startTag = '<a href="' . htmlspecialchars($entity['url'], ENT_QUOTES, 'UTF-8') . '">';
                    $endTag = '</a>';
                    break;
                case 'text_mention':
                    $startTag = '<a href="tg://user?id=' . htmlspecialchars($entity['user']['id'], ENT_QUOTES, 'UTF-8') . '">';
                    $endTag = '</a>';
                    break;
                case 'spoiler':
                    $startTag = '<tg-spoiler>';
                    $endTag = '</tg-spoiler>';
                    break;
                default:
                    continue 2;
            }

            $startTagLength = count(unpack('n*', mb_convert_encoding($startTag, 'UTF-16', 'UTF-8')));
            $endTagLength = count(unpack('n*', mb_convert_encoding($endTag, 'UTF-16', 'UTF-8')));

            array_splice($chars, $start, 0, unpack('n*', mb_convert_encoding($startTag, 'UTF-16', 'UTF-8')));
            array_splice($chars, $end + $startTagLength, 0, unpack('n*', mb_convert_encoding($endTag, 'UTF-16', 'UTF-8')));
            $offsetAdjustment += $startTagLength + $endTagLength;
        }

        return mb_convert_encoding(pack('n*', ...$chars), 'UTF-8', 'UTF-16');
    }

    public function changeLink($text, $newLink)
    {
        // Регулярное выражение для поиска ссылок в тегах <a>
        $pattern = '/<a\s+href="([^"]+)"/';

        // Замена всех найденных ссылок на новую ссылку
        $text = preg_replace_callback($pattern, function ($matches) use ($newLink) {
            return str_replace($matches[1], $newLink, $matches[0]);
        }, $text);

        

       // Регулярное выражение для поиска ссылок в тегах <a>
       $pattern = '/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])/';
         // Замена всех найденных ссылок на новую ссылку
       $text = preg_replace_callback($pattern, function ($matches) use ($newLink) {
             return str_replace($matches[0], $newLink, $matches[0]);
       }, $text);
    
    
        
        

        return $text;
    }
}
