<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\Pipeline\PipelineController;
use App\Models\InviteStat;
use App\Models\Project;
use App\Models\Telegram\Pipeline\Button;
use App\Models\Telegram\Pipeline\Pipeline;
use App\Models\Telegram\StoryTelegramMessage;
use App\Models\User;
use App\Traits\TelegramBotTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Klev\TelegramBotApi\Methods\GetChatMember;
use Klev\TelegramBotApi\Methods\GetUserProfilePhotos;
use Klev\TelegramBotApi\Methods\SendMessage;
use Klev\TelegramBotApi\Methods\SendPhoto;
use Klev\TelegramBotApi\Telegram;
use Klev\TelegramBotApi\TelegramException;
use Klev\TelegramBotApi\Types\InlineKeyboardButton;
use Klev\TelegramBotApi\Types\InlineKeyboardMarkup;
use Klev\TelegramBotApi\Types\KeyboardButton;
use Klev\TelegramBotApi\Types\Message;
use Klev\TelegramBotApi\Types\ReplyKeyboardMarkup;

class MessageController extends Controller
{

    use TelegramBotTrait;

    public $text;
    public $buttons;
    public $mediaUrl;
    public $media_id;

    public $mediaType;
    public $messageId;
    public $messageModel;

    public function setMessageParent($messageController,$userParent,$UserChannel){
        $user = '<a href="tg://user?id=' .$UserChannel->telegram_id  . '">' .  "$UserChannel->fullName" . '</a>';
        $countPeopleInvite = count($userParent->myInvite);
        $messageController->text = "Новый участник по твоей ссылке:\n🔥 $user \n\nТы зарабатываешь + 100 рублей\n\nВсего ты пригласил " . $countPeopleInvite . " человека и заработал " . $countPeopleInvite*100 ." рублей💰";

        InviteStat::query()->where('project_id',$UserChannel->project_id)->updateOrCreate([
            'project_id' => $UserChannel->project_id,
            'telegram_id' => $userParent->telegram_id,
        ],[
            'inviteFirst' => $countPeopleInvite,
        ]);

    }


    public function createMessage($id){
        $pipeline = Pipeline::query()->where('project_id',$id)->get();

        foreach ($pipeline as $key => $value){
            if ($value->content_type == "post"){
                $message = $value->getMessage;
                $pipeline[$key]['text'] = $message->text;
                $pipeline[$key]['mediaUrl'] = env('APP_URL') . "/storage/image/".$id.'/' . $message->mediaUrl;
                $pipeline[$key]['pipeline_id'] = $value->pipeline_id;
                // добавляем кнопки
                $getButtons = $message->getButtons;
                $buttons = [];
                foreach ($getButtons as $keys => $getButton) {
                    $buttons[$getButton->type][$keys]['type'] = $getButton->type;
                    $buttons[$getButton->type][$keys]['button_text'] = $getButton->button_text;
                    $buttons[$getButton->type][$keys]['callback_action'] = $getButton->callback_action;
                }
                $pipeline[$key]['buttons'] = $buttons;
            }
        }

        $messages = \App\Models\Telegram\Pipeline\Message::query()->where('project_id',$id)->get();

        foreach ($messages as $key => $message){
            $mess[$key]['text'] = $message->text;
            $mess[$key]['id'] = $message->id;
            $mess[$key]['callback_data'] = $message->callback_data ?? '';
            $mess[$key]['mediaUrl'] =  env('APP_URL') . "/storage/image/".$id.'/' . $message->mediaUrl;
            $mess[$key]['message_id'] = $message->id;
            // добавляем кнопки
            $getButtons = $message->getButtons;
            $buttons = [];
            foreach ($getButtons as $keys => $getButton) {
                $buttons[$getButton->type][$keys]['type'] = $getButton->type;
                $buttons[$getButton->type][$keys]['button_text'] = $getButton->button_text;
                $buttons[$getButton->type][$keys]['callback_action'] = $getButton->callback_action;
            }
            $mess[$key]['buttons'] = $buttons;
        }
        return view('telegram.addMessage',compact('id','pipeline','mess'));
    }
    public function createMessageStore(Request $request,$id){

        DB::transaction(function() use($request,$id) {

            $avatar = $request->avatar ?? '0';

                if ($request->file('photoPost') !== null){
                    $path = "public/image/".$id;
                    $name = $request->file('photoPost')->getClientOriginalName();
                    $request->file('photoPost')->storeAs($path,$name);
                    $mediaType = 'photo';
                } else{
                    $name = null;
                    $mediaType = null;
                }

                $telegramMessege = \App\Models\Telegram\Pipeline\Message::query()->create([
                    'text' => $request->text_post,
                    'mediaUrl' => $name,
                    'project_id' => $id,
                    'media_type' => $mediaType,
                    'avatar' => $avatar,
                ]);

                $callback_data = $id . '_' . $telegramMessege->id;

                $telegramMessege->update(['callback_data' => $callback_data]);

                if ($request->button_type != 'noButton'){

                    $requests = collect($request->all());


                    $filteredButton = $requests->filter(function ($value, $key)
                    { return Str::startsWith($key, 'button_post'); });

                    if ($requests['button_type'] == 'inline_keyboard'){

                        foreach ($filteredButton as $key => $value){
                            $date = [];
                            $idButton = Str::after($key, 'button_post_');

                            $date['button_text'] = $requests['button_post_' . $idButton];
                            $action = $requests['button_type_inline_' . $idButton];
                            if ($action == 'Action'){
                                $date['callback_action'] = $requests['callback_action_' . $idButton];
                            } else {
                                $date['url'] = $requests['url_' . $idButton];
                            }
                            $date['type'] = $requests['button_type'];

                            Button::query()->updateOrCreate([
                                'message_id' => $telegramMessege->id,
                                'project_id' => $id,
                                'button_text' => $date['button_text']
                            ],$date);
                        }
                    }
                }

        });

        return redirect(route('telegramPipeline.index',$id));
    }


    public function setHistoryMessage($messageTelegram,$project_id)
    {

        $message = [
            'text' => $messageTelegram['text'] ?? null,
            'entities' => $messageTelegram['entities'] ?? null,
            'link_preview_options' => $messageTelegram['link_preview_options'] ?? null,
            ];

        $message = json_encode($message);

        StoryTelegramMessage::query()->create([
            'project_id' => $project_id,
            'telegram_id' => $messageTelegram['from']['id'] ?? null,
            'message_id' => $messageTelegram['message_id'] ?? null,
            'type' => 'inside',
            'text' => $message,
        ]);
    }

    public function sendTextTelegram ($text,$telegram_id,$bot_api_key = null){
        $project_telegram = Project::query()->find(32);
        $setting = $project_telegram->getProjectSetting;
        $api_key = $bot_api_key ?: $setting->api_key;

        $botToken = $api_key;
        $chatId = $telegram_id; // ID чата или пользователя, которому вы хотите отправить сообщение

        $response = Http::post("https://api.telegram.org/bot{$botToken}/sendMessage", [
            'chat_id' => $chatId,
            'text' => $text,
        ]);
    }


}
