<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Telegram\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class TelegramSettingController extends Controller
{
    public function telegramSettingIndex($id) {

        $setting = new Collection();
        $telegram_project = Project::query()->find($id);

        $projectSetting = $telegram_project->getProjectSetting;

        return view('telegramSetting.TelegramSetting',compact('id','projectSetting'));
    }

    public function telegramSettingStore(Request $request,$id) {

        if (isset($request->bot_api_key)){
            $setWebhook = $this->setWebhook($request->bot_api_key,$id);

            if ($setWebhook){
                Setting::query()->updateOrCreate([
                    'project_id' => $id
                ],[
                    'api_key' => $request->bot_api_key,
                ]);
            }
        }

        if (isset($request->group_telegram)){
                Setting::query()->updateOrCreate([
                    'project_id' => $id
                ],[
                    'group_telegram' => $request->group_telegram,
                ]);
        }


        return back();
    }

    public function setWebhook($bot_api_key,$id){

        $hook_url1 = 'https://api.telegram.org/bot'.$bot_api_key.'/deleteWebhook';
        $data = ['allowed_updates' => [
                        "update_id",
                        "message",
                        "edited_message",
                        "channel_post",
                        "edited_channel_post",
                        "inline_query",
                        "chosen_inline_result",
                        "callback_query",
                        "shipping_query",
                        "pre_checkout_query",
                        "poll",
                        "poll_answer",
                        "my_chat_member",
                        "chat_member"
                             ],
                          'secret_token' => 'YourSecretToken123'];
        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->post($hook_url1);


        
        $hook_url = 'https://api.telegram.org/bot'.$bot_api_key.'/setWebhook?url=https://telegram.glushko-sok.ru'.route('getWebhook.store',$id,false);
        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->post($hook_url,$data);


        if ($response->json()['result'] == true) {
            return true;
        } else {
            return false;
        }

    }
}
