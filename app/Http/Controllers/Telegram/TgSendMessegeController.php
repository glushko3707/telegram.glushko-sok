<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Http\Controllers\VariableController;
use App\Models\Project;
use App\Models\Telegram\StoryTelegramMessage;
use Klev\TelegramBotApi\Methods\SendMessage;
use Klev\TelegramBotApi\Methods\SendPhoto;
use Klev\TelegramBotApi\Methods\SendVideo;
use Klev\TelegramBotApi\Methods\SendVideoNote;
use Klev\TelegramBotApi\Telegram;
use Klev\TelegramBotApi\Types\InlineKeyboardButton;
use Klev\TelegramBotApi\Types\InlineKeyboardMarkup;
use Klev\TelegramBotApi\Types\KeyboardButton;
use Klev\TelegramBotApi\Types\ReplyKeyboardMarkup;
use Throwable;

class TgSendMessegeController extends Controller
{
    public function sendMessage($id,MessageController $message,$telegram_id){

        $project_telegram = Project::query()->find($id);
        $setting = $project_telegram->getProjectSetting;
        $api_key = $setting->api_key;
        try {
            $bot = new Telegram($api_key);

            (new VariableController)->setVariables($message,$telegram_id,$project_telegram);

            if ((!empty($message->mediaType)) && $message->mediaType == 'photo') {
                if (!empty($message->media_id)) {
                    $photoUrl = $message->media_id;
                } else {
                    $photoUrl = 'http://telegram.glushko-sok.ru/storage/image/' .$id.'/'.$message->mediaUrl;
                }
                $msg = new SendPhoto($telegram_id,$photoUrl);
                $msg->caption = $message->text;
                if (!empty($message->buttons) && isset($message->buttons->first()->type)){
                    $msg->reply_markup = $this->getReplyMarkup($message);
                }
                $message = $bot->sendPhoto($msg);
            } elseif ((!empty($message->mediaType)) && $message->mediaType == 'video') {
                $mediaUrl = $message->mediaUrl;
                $msg =  new SendVideo($telegram_id,$mediaUrl);
                $msg->video = $mediaUrl;
                $msg->caption = $message->text;
                if (isset($message->buttons->first()->type)){
                    $msg->reply_markup = $this->getReplyMarkup($message);
                }
                $message = $bot->sendVideo($msg);

            }
            elseif ((!empty($message->mediaType)) && $message->mediaType == 'videoNote') {
                $mediaUrl = $message->mediaUrl;
                $msg =  new SendVideoNote($telegram_id,$mediaUrl);
                $msg->video_note = $mediaUrl;
                if (isset($message->buttons->first()->type)){
                    $msg->reply_markup = $this->getReplyMarkup($message);
                }
                $message = $bot->sendVideoNote($msg);

            }

            else {
                $msg = new SendMessage($telegram_id, $message->text);
                $msg->parse_mode = 'html';
                if ($message->buttons != null && isset($message->buttons->first()->type)){
                    $msg->reply_markup = $this->getReplyMarkup($message);
                }
                $message = $bot->sendMessage($msg);
            }

            StoryTelegramMessage::query()->create([
                'project_id' => $id,
                'telegram_id' => $telegram_id,
                'message_id' => $message->message_id,
                'type' => 'outside',
                'text' => json_encode($message->text) ?? null,
            ]);

        } catch (Throwable $e) {
            report($e);
        }

    }


    public function getReplyMarkup($message){

        if ($message->buttons->first()->type == 'inline_keyboard') {
            $reply_markup = new InlineKeyboardMarkup();

            foreach ($message->buttons as $key => $button){
                if ($button->type == 'inline_keyboard'){
                    $inlineButton[$key] = new InlineKeyboardButton();
                    $inlineButton[$key]->text = $button->button_text;
                    if (!empty($button->url)){
                        $inlineButton[$key]->url = $button->url;
                    } else if ($button->callback_action) {
                        $inlineButton[$key]->callback_data = $button->callback_action;
                    }
                }
                $reply_markup->inline_keyboard[] = [$inlineButton[$key]];
            }
        }

        if ($message->buttons->first()->type == 'KeyboardButton') {
            $reply_markup = new ReplyKeyboardMarkup();

            foreach ($message->buttons as $key => $button){
                if ($button->type == 'KeyboardButton'){

                    if ($button->contact == true){
                        $request_contact = true;
                    } else {
                        $request_contact = false;
                    }
                    $keyboardButton[$key] = new KeyboardButton($button->button_text,$request_contact);

                }
                $reply_markup->keyboard[] = [$keyboardButton[$key]];
            }

            $reply_markup->resize_keyboard = true;

            if ($message->buttons->first()->contact == true){
                $reply_markup->request_contact = true;
            }
            if ($message->buttons->first()->oneTimeKeyboard == true){
                $reply_markup->one_time_keyboard = true;
            }
        }
        if (!isset($reply_markup)){
            $reply_markup = null;
        }
        return $reply_markup;
    }

}
