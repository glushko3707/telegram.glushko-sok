<?php

namespace App\Http\Controllers\Telegram\Group;

use App\Http\Controllers\Controller;
use App\Models\LefmTelegramMember;
use App\Models\Telegram\UserChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class GroupUserController extends Controller
{
    public function newChatMember($data,$project_id)
    {
        $link_name = $data['link_name'] ?? '';
        $channel_id = $data['channel_id'] ?? '';
        $telegram_user_id = $data['telegram_user_id'] ?? '';
        $fullName = $data['fullName'] ?? '';
        $username = $data['username'] ?? '';
        $invite_link = $data['invite_link'] ?? '';
        $is_bot = $data['bot'] ?? '';

        $this->addUserToDatabases($project_id,$link_name,$channel_id,$is_bot,$telegram_user_id,$fullName,$username,$invite_link);
    }

    function addUserToDatabases($project_id,$link_name,$channel_id,$is_bot,$telegram_user_id,$fullName,$username,$invite_link)
    {
        $user = UserChannel::query()
            ->where('project_id', $project_id)
            ->where('telegram_id', $telegram_user_id)
            ->where('channel_id', $channel_id)
            ->where('link_name', $link_name)
            ->whereDate('created_at', Carbon::today()) // Проверка на записи, созданные сегодня
            ->first();

        if (!$user) {
            UserChannel::create([
                'project_id' => $project_id,
                'telegram_id' => $telegram_user_id,
                'channel_id' => $channel_id,
                'link_name' => $link_name,
                'link' => $invite_link,
                'telegram_username' => $username,
                'fullName' => $fullName,
                'is_bot' => $is_bot,
            ]);
        }
    }



    public function leftChatMember($data, $project_id)
    {
        $channel_id = $data['channel_id'] ?? '';
        $telegram_user_id = $data['telegram_user_id'] ?? '';
        $fullName = $data['fullName'] ?? '';
        $username = $data['username'] ?? '';

        // Проверяем, существует ли запись с такими же данными, созданная сегодня
        $existingRecord = LefmTelegramMember::query()
            ->where('project_id', $project_id)
            ->where('telegram_id', $telegram_user_id)
            ->where('channel_id', $channel_id)
            ->where('telegram_username', $username)
            ->where('fullName', $fullName)
            ->whereDate('created_at', Carbon::today())
            ->first();

        // Если записи нет, добавляем или обновляем запись
        if (!$existingRecord) {
            $left = LefmTelegramMember::create([
                'project_id' => $project_id,
                'telegram_id' => $telegram_user_id,
                'channel_id' => $channel_id,
                'telegram_username' => $username,
                'fullName' => $fullName,
            ]);
        }
    }

}
