<?php

namespace App\Http\Controllers\Smartsender;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Telegram\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function SettingIndex($id){
        $project = Project::query()->find($id);
        $setting = $project->getProjectSetting;

        if (empty($setting)){
            Setting::query()->create(['project_id' => $id ]);
        }

        return view('Smartsender.SmartsenderSetting',[
            'setting' => $setting,'id' => $id,
        ]);

    }

    public function SettingStore(\Illuminate\Http\Request $request,$id){

       $smartSender_token =  $request->smartSender_token ?? '';

       $validator = $request->validate([
            'smartSender_token' => 'required|min:3',
        ],[
            'smartSender_token' => 'Обязательно для заполнения',
        ]);

       Setting::query()->updateOrCreate([
           'project_id' => $id,
       ],[
           'smartSender_token' => $smartSender_token
       ]);

       return back();

    }

    public function setServiceBot(\Illuminate\Http\Request $request,$id){

        $validator = $request->validate([
            'platform_name' => 'required|min:3',
        ],[
            'platform_name' => 'Обязательно для заполнения',
        ]);

        Setting::query()->updateOrCreate([
            'project_id' => $id,
        ],[
            'platform_name' => $request->platform_name
        ]);

        return back();

    }

}
