<?php

namespace App\Http\Controllers\Smartsender;

use App\Http\Controllers\Controller;
use App\Jobs\SmartSender\DeletePictureJob;
use App\Models\Project;
use App\Models\SmartsenderPictur;
use App\Traits\SmartsenderTrait;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PicturController extends Controller
{
    use SmartsenderTrait;

    const IMAGE_PATH = "public/image/";

    public function sendAvatarPicturSmartsender(\Illuminate\Http\Request $request,$id,$banner_name){

        $avatar = $request->photo;
        $chat_id = $request->ssid;
        $fullname = $request->fullName;
        $banner_name_short = Str::before($banner_name, '.');
        $x = $request->x ?? 73;
        $y = $request->y ?? 132;
        $size = $request->size ?? 100;

        // Проверяем, есть ли папка для проекта, иначе создаем ее
        $project_path = self::IMAGE_PATH . $id;
        if (!Storage::exists($project_path)) {
            Storage::makeDirectory($project_path);
        }

        $path = "public/image/".$id . '/saves/';
        $pathSave = "public/image/".$id . '/userPicture/';

        if ($avatar != 'https://messenger.smartsender.com/img/avatar.png') {
            $imageJpg = $this->processAvatar($avatar,$path,$banner_name,$x,$y,$size);
        }
        else {
            $Image = Image::make(file_get_contents(Storage::path($path . $banner_name)));
            $imageJpg = $this->processText($Image,$fullname,$x,$y,$size);
        }

        $imageJpg->save(Storage::path($pathSave . $banner_name_short.$chat_id.".png"));

        $date = [
            'type' => 'picture',
            'media' => asset("storage/image/" . $id . "/userPicture/" . $banner_name_short.$chat_id.".png"),
            'watermark' => 1,
        ];

        if (isset($request->text)){
            $date['content'] = $request->text;
        }

        $this->sendSmartSender($id,$chat_id,$date);

        $name = $pathSave . $banner_name_short . $chat_id . ".png";
//        DeletePictureJob::dispatch($name);

        return 200;

    }
    public function sendAvatarPicturSmartsenderNew(\Illuminate\Http\Request $request,$id){
        $avatar = $request->photo;
        $chat_id = $request->ssid;
        $fullname = $request->fullName;

        $banner_name = $request->picture;
        $banner_name_short = Str::before($banner_name, '.');
        $x = $request->x ?? 73;
        $y = $request->y ?? 132;
        $size = $request->size ?? 100;

        $text_align = $request->text_align ?? 'left';
        $text = $request->text ?? '';
        $text_x = $request->text_x ?? '';
        $text_y  = $request->text_y ?? '';
        $font_size = $request->font_size ?? '';

        $fullname_align = $request->text_align ?? 'left';
        $fullname_x = $request->text_x ?? '';
        $fullname_y  = $request->text_y ?? '';
        $fullname_size = $request->font_size ?? '';

        $path = "public/image/".$id . '/saves/';
        $pathSave = "public/image/".$id . '/userPicture/';

        if (!Storage::exists($pathSave)) {
            Storage::makeDirectory($pathSave);
            Storage::setVisibility($pathSave,'public');
        }

        if ($avatar != 'https://messenger.smartsender.com/img/avatar.png') {
            $imageJpg = Image::make(file_get_contents(Storage::path($path . $banner_name)));
            $imageJpg = $this->processAvatarNew($avatar,$imageJpg,$x,$y,$size);
        }
        else {
            $Image = Image::make(file_get_contents(Storage::path($path . $banner_name)));
            $imageJpg = $this->processTextNew($Image,$fullname,$x,$y,$size);
        }

        if ($text != ''){
            $this->addText($imageJpg,$text,$text_x,$text_y,$font_size,$text_align);

            if ($fullname_x != ''){
                $this->addText($imageJpg,$fullname,$fullname_x,$fullname_y,$fullname_size,$fullname_align);
            }

        }

        $imageJpg->save(Storage::path($pathSave . $banner_name_short."_chat_id".$chat_id.".jpg"));



        $date = [
            'type' => 'picture',
            'media' => asset("storage/image/" . $id . "/userPicture/" . $banner_name_short."_chat_id".$chat_id.".jpg"),
            'watermark' => 1,
        ];

        if (isset($request->text)){
            $date['content'] = $request->text;
        }

        $this->sendSmartSender($id,$chat_id,$date);

        return 200;

    }


    public function sendAvatarPictureSaleBot(\Illuminate\Http\Request $request, $id)
    {
        $avatar = $request->photo !== 'None' ? $request->photo : null;
        $chat_id = $request->platform_id;

        $banner_name = $request->picture;
        $banner_name_short = Str::before($banner_name, '.');

        $x = $request->x ?? 73;
        $y = $request->y ?? 132;
        $size = $request->size ?? 100;

        $base_path = "public/image/$id/";
        $path_save = $base_path . 'userPicture/';

        // Создаем директорию, если она не существует
        $this->createDirectoryIfNotExists($path_save);

        $image_path = Storage::path($base_path . 'saves/' . $banner_name);
        $image_content = file_get_contents($image_path);
        $image = Image::make($image_content);

        if ($avatar) {
            $imageJpg = $this->processAvatarNew($avatar, $image, $x, $y, $size);
        } else {
            $fullname = $request->fullName !== 'None' ? $request->fullName : ' ';
            $imageJpg = $this->processTextNew($image, $fullname, $x, $y, $size);
        }

        $text_align = $request->text_align ?? 'left';
        $text = $request->text ?? '';
        $text_x = $request->text_x ?? '';

        if ($text) {
            $font_size = $request->font_size ?? '';
            $text_y  = $request->text_y ?? '';
            $this->addText($imageJpg, $text, $text_x, $text_y, $font_size, $text_align);
        }

        if ($text_x) {
            $font_size = $request->font_size ?? '';
            $fullname = $request->fullName !== 'None' ? $request->fullName : ' ';
            $fullname_size = $font_size;
            $fullname_y  = $text_y;
            $fullname_align = $text_align;
            $this->addText($imageJpg, $fullname, $text_x, $fullname_y, $fullname_size, $fullname_align);
        }

        $save_path = Storage::path($path_save . "{$banner_name_short}_chat_id{$chat_id}.jpg");
        $imageJpg->save($save_path);

        return [
            'picture' => asset("storage/image/$id/userPicture/{$banner_name_short}_chat_id{$chat_id}.jpg")
        ];
    }

    private function createDirectoryIfNotExists($path)
    {
        if (!Storage::exists($path)) {
            Storage::makeDirectory($path);
            Storage::setVisibility($path, 'public');
        }
    }

    public function deletePictureStore(\Illuminate\Http\Request $request,$id){
        $path = "public/image/" .$id . '/saves/';
        Storage::delete($path.$request->picture_name);

        return back();
    }
    private function processAvatar($avatar,$path,$banner_name,$x,$y,$size)
    {
        // Создаем изображение из URL аватара
        $image = Image::make(file_get_contents($avatar));

        // Меняем размер и формат изображения
        $avatarSize = 160*$size/100;

        $image->resize($avatarSize, $avatarSize);
        $image->encode('png');

        // Получаем ширину и высоту изображения
        $width = $image->getWidth();
        $height = $image->getHeight();

        // Создаем маску в виде белого круга
        $mask = Image::canvas($width, $height);
        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        // Накладываем маску на изображение и увеличиваем его в два раза
        $image->mask($mask, false)->resize($width * 2, $width * 2);

        // Сохраняем изображение в папке проекта с именем avatar_chat_id.png
        // Возвращаем путь к файлу
        $imageJpg = Image::make(file_get_contents(Storage::path($path . $banner_name)));

        $widthBanner = $imageJpg->getWidth();
        $heightBanner = $imageJpg->getHeight();
        $xPosition = $widthBanner*$x/100-$widthBanner;
        $yPosition = $heightBanner*$y/100-$heightBanner;

        $imageJpg->insert($image, 'center',$xPosition,$yPosition);

        return $imageJpg;
    }
    private function sendSmartSender($id,$chat_id,$date){
        // Получаем настройки проекта и токен Smartsender
        $project  = Project::query()->find($id);
        $projectSetting = $project->getProjectSetting;
        $smartSender_token = $projectSetting->smartSender_token;

        $link = 'https://api.smartsender.com/v1/contacts/'.$chat_id.'/send';


        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$smartSender_token
        ])->post($link, $date);
    }

    private function setVariubleSmartSender($id,$chat_id,$date){
        // Получаем настройки проекта и токен Smartsender
        $project  = Project::query()->find($id);
        $projectSetting = $project->getProjectSetting;
        $smartSender_token = $projectSetting->smartSender_token;

        $link = 'https://api.smartsender.com/v1/variables/{variableId}';


        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$smartSender_token
        ])->put($link, $date);
    }


    public function viewPicture($id){

        // Проверяем, есть ли папка для проекта, иначе создаем ее
        $project_path = self::IMAGE_PATH . $id . '/saves/';

        // Получаем массив имен файлов в папке контакта
        $contactDownload = array_map('basename', Storage::files($project_path));
        // Если массив пуст, присваиваем ему пустой массив
        $contactDownload = $contactDownload ?: [];

        $date = [];
        foreach ($contactDownload as $key => $pictur){
                $date[$key]['picture'] = env('APP_URL') . "/storage/image/".$id.'/saves/' . $pictur;
                $date[$key]['picture_name'] = $pictur;
        }

        $const = [
            'ssid' => '{{ userId }}',
            'fullName' => '{{ fullName }}',
            'photo' => '{{ photo }}'
        ];

        return view('Smartsender.smartsenderPictur',compact('id','date','const'));
    }
    public function createPicture($id){
       return view('Smartsender.smartsenderAddPicture', compact('id'));
    }
    public function createPictureStore($id,\Illuminate\Http\Request $request,){

        $path = "public/image/" .$id . '/saves/';


        // Проверяем, есть ли папка для проекта, иначе создаем ее
        $project_path = self::IMAGE_PATH . $id . '/saves/';
        if (!Storage::exists($project_path)) {
            Storage::makeDirectory($project_path);
            Storage::setVisibility($project_path,'public');
        }


        $name = $request->name_file;

        $image = Image::make(file_get_contents($request->file('photoPost')));

        $widthBanner = $image->getWidth();
        $heightBanner = $image->getHeight();

        if ($widthBanner > $heightBanner) {
            $keff = $heightBanner/$widthBanner;
            $height = 1000 * $keff;
            $image->resize(1000, $height);
        } else {
            $keff = $widthBanner/$heightBanner;
            $width = 1000 * $keff;
            $image->resize($width,1000);
        }


        $image->save(Storage::path($path . $name));

        Storage::setVisibility($path . $name,'public');

        return redirect(route('SmartsenderViewPicture.index',$id));
    }

    public function updatePicture($id,$picture_name){

        return view('Smartsender.smartsenderUpdatePicture', compact('id','picture_name'));
    }

}

