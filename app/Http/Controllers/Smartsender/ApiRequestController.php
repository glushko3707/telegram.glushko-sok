<?php

namespace App\Http\Controllers\Smartsender;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiRequestController extends Controller
{
    public function smartsenderFireRequest(\Illuminate\Http\Request $request,$telegram_project_id){

        $trigger = $request->trigger ?? null;
        $ssId = $request->ssId ?? 0;

        $project  = Project::query()->find($telegram_project_id);
        $projectSetting = $project->getProjectSetting;
        $smartSender_token = $projectSetting->smartSender_token;

        $data ['name'] = $trigger;
        Http::withHeaders([
            "content-type" => "application/json",
            "Authorization" =>  "Bearer ".$smartSender_token,
        ])->post("https://api.smartsender.com/v1/contacts/".$ssId."/fire",$data);
        return 200;

    }

    public function smartsenderSetVaruable(\Illuminate\Http\Request $request,$telegram_project_id){

        $variable_name = $request->variable_name ?? null;
        $ssId = $request->ssId ?? 0;
        $value = $request->value ?? null;

        $project  = Project::query()->find($telegram_project_id);
        $projectSetting = $project->getProjectSetting;
        $smartSender_token = $projectSetting->smartSender_token;

        $data ['values'] = [$variable_name => $value];

        Http::withHeaders([
            "content-type" => "application/json",
            "Authorization" =>  "Bearer ".$smartSender_token,
        ])->put("https://api.smartsender.com/v1/contacts/".$ssId,$data);
        return 200;
    }
}
