<?php

namespace App\Http\Controllers\Smartsender;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    public function phoneUser(Request $request){
        return ['phone_user' => '+' . $request->phone_user];

        //     PUT https://api.smartsender.com/v1/contacts/{{ userId }}
        //{ "values":{"phone":"{{ телефон }}"}
        //}
    }
}
