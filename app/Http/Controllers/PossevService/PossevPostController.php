<?php

namespace App\Http\Controllers\PossevService;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\TextHtmlBotController;
use App\Models\PosevPost;
use App\Models\Project;
use App\Models\Telegram\StoryTelegramMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Klev\TelegramBotApi\Telegram;
use function PHPUnit\Framework\fileExists;

class PossevPostController extends Controller
{

    public function deletePost($post_id)
    {
        PosevPost::query()->where('id', $post_id)->delete();
        return back();
    }

    public function possevPostIndex($project_id)
    {
        $id = $project_id;
        $PosevPost = PosevPost::query()->where('project_id', $project_id)->get();
        $PosevPost = $PosevPost->map(function ($item) use ($project_id) {
            if (isset($item->media)){
                $fileUrl = $this->getPicture($item->media,$project_id);
            }
            $item->media_url = $fileUrl;
            $item->text = str_replace("\n", "<br>", $item->text);
            return $item;
        });

        $telegrams_ids = StoryTelegramMessage::query()
            ->where('project_id',$project_id)
            ->get()
            ->unique('telegram_id');

        $telegrams_ids = $telegrams_ids->map(function ($item) {
            $item->full_name = $item->getUserBot->fullName ?? $item->telegram_id;
            return $item;
        });

        return view('possev.possevPost',compact('PosevPost','id','telegrams_ids'));
    }
    public function saveNewPost($message,$project_id)
    {
        if (isset($message['photo'])){
            $lastPhoto = end($message['photo']);
            $data['media'] = $lastPhoto['file_id'] ;
            $data ['media_type'] = 'photo';
        }

        $text = $message['text'] ?? $message['caption'] ?? '';
        $entities = $message['entities'] ?? $message['caption_entities'] ?? false;

        if ($entities){
            $TextHtmlBotController = new TextHtmlBotController();
            $text = $TextHtmlBotController->applyEntities($text, $entities);
        }

        $data['text'] = $text;
        // Извлечение file_id последнего фото

        return $this->addPostToDatabase($project_id,$data);
    }

    function addPostToDatabase($project_id,$data)
    {
        $media = $data ['media'] ?? null;
        $media_type = $data ['media_type'] ?? null;
        $text = $data['text'];
        $PosevPost = PosevPost::query()->updateOrCreate([
            'project_id' => $project_id,
            'text' => $text,
            'media' => $media,
        ],[
            'media_type' => $media_type,
            'active' => 1,
        ]);

        $PosevPost->name = "Пост_" .$PosevPost->id;
        $PosevPost->save();
        return $PosevPost->id;
    }


    function getPicture($file_id, $id)
    {
        // Определение относительного пути к файлу на 'public' диске
        $relativePath = "project/$id/telegram/picture/$file_id.jpg";

        // Проверка существования файла на 'public' диске
        if (!Storage::disk('public')->exists($relativePath)) {
            // Вызов метода для создания изображения, если его нет
            $fileUrl = $this->setPicture($id, $file_id);
        } else {
            // Генерация полного URL файла через 'public' диск
            $fileUrl = Storage::disk('public')->url($relativePath);
        }
        return $fileUrl;
    }
    function setPicture($id, $file_id)
    {

        $project_telegram = Project::query()->find($id);
        $setting = $project_telegram->getProjectSetting;
        $api_key = $setting->api_key;
        $bot = new Telegram($api_key);
        $url = $bot->getFile($file_id);
        $filePath = $url->file_path;
        $fileUrl = "https://api.telegram.org/file/bot{$api_key}/{$filePath}";
        $outputDirectory = "public/project/$id/telegram/picture/";
        if (!Storage::exists($outputDirectory)) {
            Storage::makeDirectory($outputDirectory);
            Storage::setVisibility($outputDirectory,'public');
        }

        $filename = $outputDirectory . $file_id . '.jpg';

        if (!Storage::exists($filename)) {
            $fileContents = file_get_contents($fileUrl);
            Storage::put($filename, $fileContents);
        }

        return Storage::url($filename);
    }
}
