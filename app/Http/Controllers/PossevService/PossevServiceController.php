<?php

namespace App\Http\Controllers\PossevService;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\InviteLinkController;
use App\Http\Controllers\Telegram\MessageController;
use App\Http\Controllers\Telegram\TextHtmlBotController;
use App\Http\Controllers\Telegram\TgSendMessegeController;
use App\Jobs\Telegram\SendMessageJob;
use App\Models\PosevPost;
use App\Models\PossevLink;
use App\Models\Project;
use App\Models\Telegram\StoryTelegramMessage;
use Illuminate\Http\Request;

class PossevServiceController extends Controller
{

    public function possevStatIndex($id)
    {
        return view('possev.possevStat', ['id' => $id]);
    }

    public function sendNewPossevLink(Request $request,$post_id,$project_id)
    {
        $creates_join_request = $request->creates_join_request ?? false;
        $group_name = $request->group_name  ?: null;

        $linkInfo = $this->createPossevLink($group_name,$creates_join_request,$project_id,$post_id);

        $link = $linkInfo['link'];
        $link_name = $linkInfo['link_name'];

        $telegram_id = auth()->user()->telegram_id;
        $this->sendPostToBot($link,$post_id,$link_name,$project_id,$telegram_id);

        return back();
    }

    public function createPossevLink($group_name,$creates_join_request,$project_id,$post_id)
    {
        // добавляем в базу данных новый посев
        $PossevLinkModel = PossevLink::query()->create(['project_id' => $project_id,'posev_post_id' => $post_id]);

        // создаём новую ссылку приглашения в канал и сохраняем
        $project = Project::find($project_id);
        $inviteLinkController = new InviteLinkController();
        $link_name = "possev_$PossevLinkModel->id";
        $link = $inviteLinkController->createInviteLink($project, $link_name,$creates_join_request);

        if (isset($creates_join_request)){
            $PossevLinkModel->creates_join_request = 1;
        }

        $PossevLinkModel->group_name = $group_name  ?: null;
        $PossevLinkModel->link_possev = $link;
        $PossevLinkModel->link_name_possev = $link_name;
        $PossevLinkModel->save();

        return [
            'link' => $link,
            'link_name' => $link_name,
        ];

    }

    public function setPossevLink(PossevLink $PossevLinkModel ,$creates_join_request,$project_id)
    {
        $project = Project::find($project_id);
        $inviteLinkController = new InviteLinkController();
        $link_name = $PossevLinkModel->link_name_possev ?: "link_$PossevLinkModel->id";
        $link = $inviteLinkController->createInviteLink($project, $link_name,$creates_join_request);

        if ($creates_join_request){
            $PossevLinkModel->creates_join_request = 1;
        }

        $PossevLinkModel->link_possev = $link;
        $PossevLinkModel->link_name_possev = $link_name;
        $PossevLinkModel->save();

        return [
            'link' => $link,
            'link_name' => $link_name,
        ];
    }

    public function createPossevNoLink($project_id)
    {
        // добавляем в базу данных новый посев
        $PossevLinkModel = PossevLink::query()->create(['project_id' => $project_id,'posev_post_id' => null]);
        $link_name = "link_$PossevLinkModel->id";
        $PossevLinkModel->link_name_possev = $link_name;
        $PossevLinkModel->save();

        return [
            'link_name' => $link_name,
        ];

    }

    public function subscriptionsUpdateInfo($project_id)
    {
        $possevLinks = PossevLink::query()
            ->where('project_id', $project_id)
            ->withCount([
                'getUser_channels as subscriptionsed',
                'getUser_channels as unsubscribesed' => function ($query) {
                    $query->whereHas('getLeftUser', function ($query) {
                        $query->whereColumn('lefm_telegram_members.telegram_id', 'user_channels.telegram_id')
                              ->whereColumn('lefm_telegram_members.channel_id', 'user_channels.channel_id');
                    });
                }
            ])
            ->get();
        foreach ($possevLinks as $possevLink) {
            $possevLink->subscriptions = $possevLink->subscriptionsed;
            $possevLink->unsubscribes = $possevLink->unsubscribesed;
            $possevLink->save();
        }

        return $possevLinks;
    }

    public function sendPostToBot($link,$post_id,$link_name,$project_id,$platform_id)
    {
        // заменяем ссылку в сообщении на новую
        $PosevPost = PosevPost::query()->find($post_id);
        $text = (new TextHtmlBotController())->changeLink($PosevPost->text,$link);

        // отправить сообщение о том что ниже ссылка на поссев и ID и ссылку на канал где будет размещаться пост
        $message = new MessageController();
        $message->text = "💰Пост для посева <b>$link_name</b> 👇👇👇";
        if (!empty($request->group_name)){
            $message->text = "💰Пост для посева <b>$link_name</b> в канале <b>$request->group_name</b> 👇👇👇";
        }


        $messagePost = new MessageController();
        $messagePost->text = $text;
        if (!empty($PosevPost->media)){
            $messagePost->mediaType = $PosevPost->media_type;
            $messagePost->media_id = $PosevPost->media;
        }
        // Отправляем сообщение с задачей в очередь
        (new TgSendMessegeController())->sendMessage($project_id, $message, $platform_id);
        sleep(1);
        SendMessageJob::dispatch($project_id, $messagePost, $platform_id)->onQueue('top');

    }

    public function setWelcomeStartMessege()
    {
        return 'Этот бот поможет тебе сделать <b>посевы</b> еще более <u>выгодными и удобными</u>:
               
- автоматическая замета ссылок под новый канал или сайт
- аналитика по каналам размещения
- аналитика по вашим креативам (постам)
- статистика по вступившим и отписавшимся
- защита от ботов
              
<b>Готовы начать?</b> Жмите на кнопку и отпр��вьте или перешлите ваш Пост';
    }
}
