<?php

namespace App\Http\Controllers\PossevService;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Support\Facades\Cache;
use Klev\TelegramBotApi\Methods\UpdatingMessages\EditMessageReplyMarkup;
use Klev\TelegramBotApi\Methods\UpdatingMessages\EditMessageText;
use Klev\TelegramBotApi\Telegram;
use Klev\TelegramBotApi\Types\InlineKeyboardButton;
use Klev\TelegramBotApi\Types\InlineKeyboardMarkup;

class CallbackController extends Controller
{
    public function createPostCallback($project_id,$platform_id,$message_id)
    {
        // Найдите проект по ID
        $project_telegram = Project::query()->find($project_id);

// Получите настройки проекта
        $setting = $project_telegram->getProjectSetting;
        $api_key = $setting->api_key;

// Инициализация бота с использованием API ключа
        $bot = new Telegram($api_key);

// Создайте объект для редактирования ReplyMarkup
        $editMessageReplyMarkup = new EditMessageReplyMarkup();
        $editMessageReplyMarkup->chat_id = $platform_id;
        $editMessageReplyMarkup->message_id = $message_id;

// Создайте InlineKeyboardButton
        $InlineKeyboardButton = new InlineKeyboardButton();
        $InlineKeyboardButton->text = 'Назад';
        $InlineKeyboardButton->callback_data = 'Cancel';

// Создайте объект InlineKeyboardMarkup и добавьте кнопку
        $reply_markup = new InlineKeyboardMarkup();
        $reply_markup->inline_keyboard = [[$InlineKeyboardButton]]; // Исправлено на правильное создание массива кнопок
        $editMessageReplyMarkup->reply_markup = $reply_markup;

// Создайте новый текст для сообщения
        $newText = "Отправьте или перешлите Ваш пост";

// Создайте запрос на редактирование сообщения
        $editMessage = new EditMessageText($newText);
        $editMessage->message_id = $message_id;
        $editMessage->chat_id = $platform_id;
        $editMessage->reply_markup = $reply_markup;
// Отправьте запросы на редактирование текста и разметки
        $bot->editMessageText($editMessage);

        $nextStepKey = "telegram_await_project_$project_id" . "_chat_id_$platform_id";
        Cache::put($nextStepKey,'await_post', now()->addHours(12));
    }
}

