<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Telegram\UserBot;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register');
    }

    public function telegram(Request $request)
    {

        $user = User::query()->where('telegram_id',$request->id)->get()->first();

        if (empty($user)){
            $user = User::create([
                'name' => $request->first_name ?: $request->last_name ?: $request->username ?: $request->id,
                'email' => $request->id . '@mail.ru',
                'password' => Hash::make($request->id),
            ]);
        }

        Auth::login($user);
        return redirect(RouteServiceProvider::HOME);
    }


    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }

        public function createUserTelegramStart(Request $request){

            if (isset($request['message']['from']['username'])){
                $fullname = $request['message']['from']['username'];
            } else{
                $firstName = $request['message']['from']['first_name'] ?? '';
                $lastName = $request['message']['from']['last_name'] ?? '';
                if ($firstName == '' && $lastName == '' ){
                    $fullname = 'id_'. $request['message']['from']['id'];
                } else {
                    $fullname = $firstName.' '.$lastName;
                }
            }

            return User::create([
                'name' => $fullname,
                'email' => $request['message']['from']['id'] . '@mail.ru',
                'password' => Hash::make($request['message']['from']['id']),
                'telegram_id' => $request['message']['from']['id'],
            ]);
        }

    public function createUserTelegramVarible($id,$telegram_id){

        $userBot = UserBot::query()->where('project_id',$id)->where('telegram_id',$telegram_id)->get()->first();

        if (!empty($userBot->fullName)){
            $fullname = $userBot->fullName;
        } else if(!empty($userBot->telegram_username)) {
            $fullname = $userBot->telegram_username;
        } else {
            $fullname = 'id_'. $userBot->telegram_id;}


        return User::create([
            'name' => $fullname,
            'email' => $userBot->telegram_id . '@mail.ru',
            'password' => Hash::make($userBot->telegram_id),
            'telegram_id' => $userBot->telegram_id,
        ]);
    }


}
