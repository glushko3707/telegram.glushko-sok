<?php

namespace App\Http\Controllers\SaleBot;

use App\Http\Controllers\Controller;
use App\Models\LefmTelegramMember;
use App\Models\Project;
use App\Models\Telegram\UserChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Response;


class TelegramMember extends Controller
{
    public function leftMember(Request $request,$id){
        $requestObject = json_decode($request->tg_request)->chat_member;
        $channel_id = $requestObject->chat->id;
        $telegram_id = $requestObject->from->id;
        $username = $requestObject->from->username ?? '';
        $fullName = $requestObject->from->first_name ?? '' . '_' . $requestObject->from->last_name ?? '';

        LefmTelegramMember::query()->create([
            'channel_id' => $channel_id,
            'project_id' => $id,
            'telegram_id' => $telegram_id,
            'telegram_username' => $username,
            'fullName' => $fullName,
        ]);

        return 200;
    }


    public function joinMember(Request $request,$id){
        $requestObject = json_decode($request->tg_request)->chat_member;

        $channel_id = $requestObject->chat->id;
        $telegram_id = $requestObject->old_chat_member->user->id;
        $username = $requestObject->old_chat_member->user->username ?? '';
        $invite_link = $requestObject->invite_link->invite_link ?? '';
        $firstName = $requestObject->old_chat_member->user->first_name ?? '';
        $lastName = $requestObject->old_chat_member->user->last_name ?? '';
        $fullName = $firstName . ' ' . $lastName;
        $link_name = $requestObject->invite_link->name ?? '';

        UserChannel::query()->create([
            'project_id' => $id,
            'telegram_id' => $telegram_id,
            'channel_id' => $channel_id,
            'link_name' => $link_name,
            'link' => $invite_link,
            'telegram_username' => $username,
            'fullName' => $fullName,
        ]);

        return 200;
    }


    public function getUserChanal($id){
        $userChannels = UserChannel::query()
            ->where('project_id', $id)
            ->whereNotNull('telegram_username')
            ->get();

        // Определение заголовков для CSV
        $headers = Schema::getColumnListing('user_channels');

        // Создание потока вывода
        $callback = function() use ($userChannels, $headers) {
            $file = fopen('php://output', 'w');
            // Добавление BOM для поддержки UTF-8 в Excel
            fputs($file, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
            // Запись заголовков в CSV
            fputcsv($file, $headers);

            // Запись данных в CSV
            foreach ($userChannels as $userChannel) {
                fputcsv($file, $userChannel->toArray());
            }
            fclose($file);
        };

        // Возвращение ответа с CSV-файлом для скачивания
        return Response::stream($callback, 200, [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="user_channels.csv"',
        ]);
    }

}
