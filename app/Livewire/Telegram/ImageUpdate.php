<?php

namespace App\Livewire\Telegram;

use App\Jobs\SmartSender\DeletePictureJob;
use App\Models\Project;
use App\Traits\SmartsenderTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Livewire\Component;
use Livewire\WithFileUploads;

class ImageUpdate extends Component
{
    const IMAGE_PATH = "public/image/";

    use WithFileUploads;
    use SmartsenderTrait;

    public $avatar_display = '';public $text_display = 'none';
    public $avatar = true;public $id; public $x = 100; public $y = 100; public $size = 100;
    public $imaging = ''; public $picture_name = '';
    public $image = '';public $name_file;
    public $copyLink; public $copyLinksSaleBot;
    public $text_align; public $setting;
    public $text_x = 1;public $text_y = 100;public $font_size = 100;public $text_text = 'Имя фамилия';public $text = false;


    public function mount($id,$picture_name)
    {
        $this->id = $id;
        $this->picture_name = $picture_name;
        $this->imaging = env('APP_URL') . "/storage/image/".$id.'/saves/' . $picture_name;
    }

    public function json($json){
        $copyLink = json_decode($json);

        if (isset($copyLink->x)){
            $this->x = $copyLink->x;
        }
        if (isset($copyLink->y)){
            $this->y = $copyLink->y;
        }
        if (isset($copyLink->size)){
            $this->size = $copyLink->size;
        }

        if (isset($copyLink->text_align)){
            $this->text_align = $copyLink->text_align;
        }
        if (isset($copyLink->text_x)){
            $this->text_x = $copyLink->text_x;
        }
        if (isset($copyLink->text_y)){
            $this->text_y = $copyLink->text_y;
        }
        if (isset($copyLink->font_size)){
            $this->font_size = $copyLink->font_size;
        }
        if (isset($copyLink->text)){
            $this->text = true;
            $this->text_text = $copyLink->text;
        }

    }

    public function render()
    {
        $project = Project::query()->find($this->id);
        $this->setting = $project->getProjectSetting;
        // Проверяем, есть ли папка для проекта, иначе создаем ее
        $project_path = self::IMAGE_PATH . $this->id . '/temp/';
        if (!Storage::exists($project_path)) {
            Storage::makeDirectory($project_path);
            Storage::setVisibility($project_path,'public');
        }

            $type = Str::afterLast($this->picture_name,'.');
            $this->name_file = now()->timestamp . '.' . $type;

            $images = Image::make("https://telegram.glushko-sok.ru/storage/image/".$this->id.'/saves/' . $this->picture_name);
            $widthBanner = $images->getWidth();
            $heightBanner = $images->getHeight();

            if ($widthBanner > $heightBanner) {
                $keff = $heightBanner/$widthBanner;
                $height = 1000 * $keff;
                $images->resize(1000, $height);
            } else {
                $keff = $widthBanner/$heightBanner;
                $width = 1000 * $keff;
                $images->resize($width,1000);
            }


            $banner_name = now()->timestamp . '_test.jpg';
            $path = "public/image/" .$this->id . '/temp/';

            if ($this->avatar != false){
                $this->avatar_display = '';
                $images = $this->avatarTest($images,$this->x,$this->y,$this->size);
            } else {
                $this->avatar_display = 'none';
            }

            if ($this->text != false){
                $this->text_display = '';
                $images = $this->addTextTest($images,$this->text_text,$this->text_x,$this->text_y,$this->font_size,$this->text_align);
            } else {
                $this->text_display = 'none';
            }

            $images->save(Storage::path($path . $banner_name));
            Storage::setVisibility($path . $banner_name,'public');

            $this->imaging = env('APP_URL') . "/storage/image/".$this->id.'/temp/' . $banner_name;
            DeletePictureJob::dispatch($path . $banner_name);

            $this->copyLinks($this->picture_name);


        return view('livewire.telegram.image-update',[
                'imaging' => $this->imaging,
                'copyLink' => $this->copyLink,
            ]);
    }


    function avatarTest($images,$x,$y,$size){
        $avatar = Storage::path('public/image/all/avatar.jpeg');
        $imageJpg = $this->processAvatarNew($avatar,$images,$x,$y,$size);
        return $imageJpg;
    }
    function addTextTest($images,$text,$x_text,$y_text,$font_size,$text_align){
        return $this->addText($images,$text,$x_text,$y_text,$font_size,$text_align);
    }
}
