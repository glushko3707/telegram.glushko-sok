<?php

namespace App\Livewire\Telegram;

use App\Jobs\SmartSender\DeletePictureJob;
use App\Models\Project;
use App\Models\SmartsenderPictur;
use App\Traits\SmartsenderTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\Attributes\On;

class MultipleImageUpload extends Component
{
    const IMAGE_PATH = "public/image/";

    use WithFileUploads;
    use SmartsenderTrait;

    public $paramPicture_display = 'none';public $avatar_display = '';public $text_display = '';
    public $avatar = true;public $id; public $x = 100; public $y = 100; public $size = 100;
    public $imaging = ''; public $image = '';public $name_file;
    public $copyLink; public $copyLinksSaleBot;
    public $setting;
    public $text_align;
    public $text_x = 1;public $text_y = 100;public $font_size = 100;public $text_text = 'Имя фамилия';public $text = false;


    public function mount($id)
    {
        $this->id = $id;
    }

    public function save()
    {

        $images = Image::make($this->image->get());
        $widthBanner = $images->getWidth();
        $heightBanner = $images->getHeight();

        if ($widthBanner > $heightBanner) {
            $keff = $heightBanner/$widthBanner;
            $height = 1000 * $keff;
            $images->resize(1000, $height);
        } else {
            $keff = $widthBanner/$heightBanner;
            $width = 1000 * $keff;
            $images->resize($width,1000);
        }

        $name = $this->name_file;
        $path = "public/image/" .$this->id . '/saves/';

        $images->save(Storage::path($path . $name));
        Storage::setVisibility($path . $name,'public');

        return redirect()->route('SmartsenderViewPicture.index',$this->id);
    }

    public function render()
    {
        $Project = Project::query()->find($this->id);
        $this->setting = $Project->getProjectSetting;

        // Проверяем, есть ли папка для проекта, иначе создаем ее
        $project_path = self::IMAGE_PATH . $this->id . '/temp/';
        if (!Storage::exists($project_path)) {
            Storage::makeDirectory($project_path);
            Storage::setVisibility($project_path,'public');
        }

        if ($this->image != ''){

            $type = Str::afterLast($this->image->getMimeType(),'/');
            $this->name_file = now()->timestamp . '.' . $type;

            $this->paramPicture_display = '';
            $images = Image::make($this->image->get());
            $widthBanner = $images->getWidth();
            $heightBanner = $images->getHeight();

            if ($widthBanner > $heightBanner) {
                $keff = $heightBanner/$widthBanner;
                $height = 1000 * $keff;
                $images->resize(1000, $height);
            } else {
                $keff = $widthBanner/$heightBanner;
                $width = 1000 * $keff;
                $images->resize($width,1000);
            }


            $banner_name = now()->timestamp . '_test.jpg';
            $path = "public/image/" .$this->id . '/temp/';

            if ($this->avatar != false){
                $this->avatar_display = '';
                $images = $this->avatarTest($images,$this->x,$this->y,$this->size);
            } else {
                $this->avatar_display = 'none';
            }

            if ($this->text != false){
                $this->text_display = '';
                $images = $this->addTextTest($images,$this->text_text,$this->text_x,$this->text_y,$this->font_size,$this->text_align);
            } else {
                $this->text_display = 'none';
            }

            $images->save(Storage::path($path . $banner_name));
            Storage::setVisibility($path . $banner_name,'public');

            $this->imaging = env('APP_URL') . "/storage/image/".$this->id.'/temp/' . $banner_name;
            DeletePictureJob::dispatch($path . $banner_name);

            $this->copyLinks($this->name_file);

        }


        return view('livewire.telegram.multiple-image-upload',[
            'imaging' => $this->imaging,
            'copyLink' => $this->copyLink,
        ]);
    }

    function avatarTest($images,$x,$y,$size){
        $avatar = Storage::path('public/image/all/avatar.jpeg');
        $imageJpg = $this->processAvatarNew($avatar,$images,$x,$y,$size);
        return $imageJpg;
    }
    function addTextTest($images,$text,$x_text,$y_text,$font_size,$text_align){
        return $this->addText($images,$text,$x_text,$y_text,$font_size,$text_align);
    }
}
