<?php

namespace App\Livewire\Telegram;

use App\Models\Channel;
use App\Models\LefmTelegramMember;
use App\Models\Telegram\UserChannel;
use Carbon\Carbon;
use Livewire\Component;

class Users extends Component
{
    public $date_to = '';
    public $date_from = '';

    public $users = [];
    public $stat = [];
    public $id;
    public $newLeadWeb = [];
    public $linkName;
    public $links;

    public function mount($id)
    {
        $this->id = $id;
    }

    public function save(){
        $filename = 'file.txt';
        $handle = fopen($filename, 'w');
        foreach ($this->newLeadWeb as $row) {
            fwrite($handle, $row->telegram_id . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);

    }


    public function render()
    {
        $channelQuerys = UserChannel::query()->where('project_id',$this->id)->orderByDesc('id');
        if (!empty($this->date_to)) {$channelQuerys->whereDate('created_at', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$channelQuerys->whereDate('created_at', '>=', $this->date_from);}
        $this->links = $channelQuerys->pluck('link_name')->unique();

        $channelQuery = UserChannel::query()->where('project_id',$this->id)->orderByDesc('id');
        if (!empty($this->date_to)) {$channelQuery->whereDate('created_at', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$channelQuery->whereDate('created_at', '>=', $this->date_from);}

        if ($this->linkName != '' && $this->linkName != 'all') {
            $channelQuery->where('link_name', '=', $this->linkName);
        }

        $channel = $channelQuery->get();

        $lefmTelegramMemberQuery = LefmTelegramMember::query()->where('project_id',$this->id);
        if (!empty($this->date_to)) {$lefmTelegramMemberQuery->whereDate('created_at', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$lefmTelegramMemberQuery->whereDate('created_at', '>=', $this->date_from);}

        if ($this->linkName != '' && $this->linkName != 'all') {
            $lefmTelegramMemberQuery->whereIn('telegram_id', $channel->pluck('telegram_id')->toArray());
        }


        $lefmTelegramMember = $lefmTelegramMemberQuery->get();
        $uniqLeft = $lefmTelegramMember->pluck('telegram_id')->intersect($channel->pluck('telegram_id'));
        $this->stat ['uniqLeft'] = $uniqLeft->count();

        $this->stat ['count'] = $channel->count();
        $this->stat ['left'] = $lefmTelegramMember->count();

        $this->newLeadWeb = $channelQuery->get();
        $leadses = $channelQuery->paginate('30');


        return view('livewire.telegram.users',[
            'leadses' => $leadses,
        ]);
    }
}
