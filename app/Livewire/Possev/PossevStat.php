<?php

namespace App\Livewire\Possev;

use App\Http\Controllers\PossevService\PossevServiceController;
use App\Models\Intensive\IntensiveUser;
use App\Models\PosevPost;
use App\Models\PossevLink;
use Livewire\Component;

class PossevStat extends Component
{
    public $possevLink;
    public $id;

    public $creates_join_request;
    public $target_type;
    public $posts;

    public $date_from;
    public $date_to;

    public function mount($id)
    {
        $this->id = $id;
        $this->load();
        $this->subscriptionsUpdateInfo();
        $this->posts = PosevPost::query()->where('project_id', $id)->get()->toArray();
    }


    public function subscriptionsUpdateInfo(){
        $possevServiceController = new PossevServiceController();
        $posev = $possevServiceController->subscriptionsUpdateInfo($this->id);
    }

    public function load(){
        $possevLink = PossevLink::query()->where('project_id',$this->id)->with('getPost')->get()->reverse();
        $possevLink->map(function ($possevLink) {
            $possevLink->post = $possevLink->getPost;
            return $possevLink;
        });

        $this->possevLink = $possevLink->toArray();
    }

    public function newLinkPossev(){
    $PossevServiceController = new PossevServiceController();
    $PossevServiceController->createPossevNoLink($this->id);
    $this->possevLink = [];    
    sleep(1);
    $this->load();
    }

    public function tgSend($link_id){
        $possevLink = PossevLink::query()->find($link_id);
        $telegram_id = auth()->user()->telegram_id;
        if (empty($possevLink->posev_post_id)) {
            $this->dispatch('alerts', title: 'Выберите пост!');
            return;
        }

        $possevServiceController = new PossevServiceController();

        if (empty($possevLink->link_possev)) {
          
          $possevServiceController->setPossevLink($possevLink,$possevLink->creates_join_request,$this->id);
        }
        $possevServiceController = new PossevServiceController();
        $possevServiceController->sendPostToBot($possevLink->link_possev,$possevLink->posev_post_id,$possevLink->link_name_possev,$this->id,$telegram_id);

    }
    public function delete($id){
        PossevLink::query()->find($id)->delete();
        $this->load();
    }

    public function updated($propertyName, $value)
    {
        if ($propertyName == 'group_name_add' || $propertyName == 'creates_join_request') {
            return; // Пропустить обновление
        }
        

        list($array, $index, $field) = explode('.', $propertyName);

        // Преобразование значения чекбокса в 1 или 0 для базы данных
        if ($field == 'chat_main_active') {
            $value = $value ? 1 : 0;
        }

        // Преобразование значения чекбокса в 1 или 0 для базы данных
        if ($field == 'date' || $field == 'money_price' || $field == 'impressions') {
            $value = $value ?: null;
        }

        $userId = $this->possevLink[$index]['id']; // Получение ID пользователя
        $user = PossevLink::query()->find($userId); // Поиск пользователя по идентификатору
        if ($user) {
            $user->$field = $value; // Присвоение обновлённого значения п��ля
            $user->save(); // Сохранение изменений
        }
        return 200;
    }


    public function render()
    {


        return view('livewire.possev.possev-stat');
    }
}
