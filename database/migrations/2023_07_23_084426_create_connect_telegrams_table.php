<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('connect_telegrams', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('telegram_id')->nullable();
            $table->string('code')->unique();
            $table->string('user_id')->nullable();
            $table->integer('project_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('connect_telegrams');
    }
};
