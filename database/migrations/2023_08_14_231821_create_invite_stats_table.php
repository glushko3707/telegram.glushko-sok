<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('invite_stats', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('telegram_id');
            $table->integer('project_id');
            $table->integer('inviteFirst')->default(0);
            $table->integer('inviteSecond')->default(0);
            $table->integer('inviteThird')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('invite_stats');
    }
};
