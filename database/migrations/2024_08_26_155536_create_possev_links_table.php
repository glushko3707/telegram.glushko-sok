<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('possev_links', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(\App\Models\Project::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(\App\Models\PosevPost::class)->constrained()->onDelete('restrict');
            $table->string('link_possev')->nullable();
            $table->string('link_name_possev')->nullable();
            $table->string('money_price')->nullable();
            $table->dateTime('date')->nullable();
            $table->string('group_name')->nullable();
            $table->string('group_link')->nullable();
            $table->string('admin_link')->nullable();
            $table->string('comment',2024)->nullable();
            $table->string('impressions')->nullable();
            $table->boolean('creates_join_request')->default(0);
            $table->boolean('money_send')->default(0);

            $table->index('project_id');
            $table->index('posev_post_id');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('possev_links');
    }
};
